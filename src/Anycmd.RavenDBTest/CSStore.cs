﻿
namespace Anycmd.RavenDBTest
{
    using Raven.Client;
    using Raven.Client.Document;

    public class CSStore : DocumentStore
    {
        public static readonly IDocumentStore SingleInstance = new CSStore { Url = "http://localhost:8081/" };

        static CSStore()
        {
            SingleInstance.Initialize();
        }
    }
}

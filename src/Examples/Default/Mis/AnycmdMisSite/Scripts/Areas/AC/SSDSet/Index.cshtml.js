﻿/// <reference path="../../../jquery-1.8.3.intellisense.js" />
/// <reference path="../../../miniui/miniui.js" />
/// <reference path="../../../jquery-bbq/jquery.ba-bbq.js" />
(function (window, $) {
    mini.namespace("AC.SSDSet.Index");
    var self = AC.SSDSet.Index;
    self.prifix = "AC_SSDSet_Index_";
    self.search = search;
    self.gridReload = function () {
        grid.reload();
    };
    self.help = { appSystemCode: "Anycmd", areaCode: "AC", resourceCode: "SSDSet", functionCode: "Index" };
    helper.helperSplitterInOne(self);
    mini.namespace("SSDSet.Edit");
    var edit = SSDSet.Edit;
    edit.prifix = "AC_SSDSet_Index_Edit_";
    var faceInitialized = false;

    var tabConfigs = {
        infoTab: {
            url: bootPATH + "../AC/SSDSet/Details",
            params: [{ "pName": 'id', "pValue": "Id" }],
            namespace: "SSDSet.Details"
        },
        roleTab: {
            url: bootPATH + "../AC/SSDSet/Roles",
            params: [{ "pName": 'ssdSetID', "pValue": "Id" }],
            namespace: "SSDSet.Roles"
        },
        operationLogTab: {
            url: bootPATH + "../AC/OperationLog/Index",
            params: [{ "pName": 'targetID', "pValue": "Id" }],
            namespace: "AC.OperationLog.Index"
        }
    };
    self.filters = {
        Name: {
            type: 'string',
            comparison: 'like'
        },
        IsEnabled: {
            type: 'numeric',
            comparison: 'eq'
        }
    };

    mini.parse();

    var win = mini.get(edit.prifix + "win1");
    var form;
    if (win) {
        form = new mini.Form(edit.prifix + "form1");
    }

    var tabs1 = mini.get(self.prifix + "tabs1");
    var grid = mini.get(self.prifix + "datagrid1");
    grid.on("drawcell", helper.ondrawcell(self, "AC.SSDSet.Index"));
    grid.on("load", helper.onGridLoad);
    grid.sortBy("CreateOn", "asc");

    function search() {
        var data = {};
        var filterArray = [];
        for (var k in self.filters) {
            var filter = self.filters[k];
            if (filter.value) {
                filterArray.push({ field: k, type: filter.type, comparison: filter.comparison, value: filter.value });
            }
        }
        data.filters = JSON.stringify(filterArray);
        grid.load(data);
    }

    helper.index.allInOne(
        edit,
        grid,
        bootPATH + "../AC/SSDSet/Edit",
        bootPATH + "../AC/SSDSet/Edit",
        bootPATH + "../AC/SSDSet/Delete",
        self);
    helper.index.tabInOne(grid, tabs1, tabConfigs, self);

    helper.edit.allInOne(
        self,
        win,
        bootPATH + "../AC/SSDSet/Create",
        bootPATH + "../AC/SSDSet/Update",
        bootPATH + "../AC/SSDSet/Get",
        form, edit);
})(window, jQuery);
﻿using Anycmd.Host.EDI.InOuts;
using Anycmd.Model;
using System;

namespace Anycmd.EDI.ViewModels.OntologyViewModels
{
    public class OntologyOrganizationCreateInput : EntityCreateInput, IOntologyOrganizationCreateIO
    {
        public Guid OntologyID { get; set; }

        public Guid OrganizationID { get; set; }
    }
}

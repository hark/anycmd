﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Anycmd.EDI.ViewModels.NodeViewModels
{
    using Host.EDI.InOuts;
    using Model;

    /// <summary>
    /// 
    /// </summary>
    public class NodeElementCareCreateInput : EntityCreateInput, INodeElementCareCreateIO
    {
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public Guid NodeID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public Guid ElementID { get; set; }

        public bool IsInfoIDItem { get; set; }
    }
}

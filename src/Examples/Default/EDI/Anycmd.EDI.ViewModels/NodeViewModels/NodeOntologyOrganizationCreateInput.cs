﻿
namespace Anycmd.EDI.ViewModels.NodeViewModels
{
    using Host.EDI.InOuts;
    using System;

    public class NodeOntologyOrganizationCreateInput : INodeOntologyOrganizationCreateIO
    {
        public Guid NodeID { get; set; }

        public Guid OntologyID { get; set; }

        public Guid OrganizationID { get; set; }

        public Guid? Id { get; set; }
    }
}

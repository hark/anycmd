﻿
namespace Anycmd.EDI.Application {
    using DataContracts;
    using Funq;
    using Host.EDI;
    using MessageServices;
    using ServiceStack;
    using System;
    using Util;

    /// <summary>
    /// Create your ServiceSelfHost web service application with a singleton ServiceSelfHost.
    /// </summary>
    public sealed class ServiceSelfHost : AppHostHttpListenerBase {
        private readonly ProcessDescriptor process;
        private readonly IACDomain acDomain;

        public ServiceSelfHost(Anycmd.IACDomain acDomain, ProcessDescriptor process)
            : base("Self-Host", typeof(MessageService).Assembly)
        {
            if (acDomain == null)
            {
                throw new ArgumentNullException("acDomain");
            }
            if (process == null)
            {
                throw new ArgumentNullException("process");
            }
            this.acDomain = acDomain;
            this.process = process;
            this.ServiceName = process.ProcessType.ToName() + " - " + process.Process.Name;
        }

        public override ServiceStackHost Init() {
            var host = base.Init();
            host.Start(process.WebApiBaseAddress);
            return host;
        }

        public override void Configure(Container container) {
            var adapter = new ServiceContainerAdapter(acDomain);
            container.Adapter = adapter;

            SetConfig(new HostConfig {
                DebugMode = true,
                WsdlServiceNamespace = Consts.Namespace,
                EnableFeatures = Feature.Metadata | Feature.Json | Feature.Jsv | Feature.Html
            });
        }
    }

}

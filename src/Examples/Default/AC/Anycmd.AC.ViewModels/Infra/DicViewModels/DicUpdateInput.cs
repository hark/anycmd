﻿
namespace Anycmd.AC.Infra.ViewModels.DicViewModels
{
    using Host.AC.InOuts;
    using Model;
    using System;
    using System.ComponentModel.DataAnnotations;

    public class DicUpdateInput : IInputModel, IDicUpdateIO
    {
        public Guid Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string Code { get; set; }
        /// <summary>
        /// 
        /// </summary>
        [Required]
        public string Name { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int IsEnabled { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int SortCode { get; set; }
    }
}

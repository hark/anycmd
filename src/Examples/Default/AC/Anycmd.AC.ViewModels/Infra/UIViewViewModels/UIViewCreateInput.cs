﻿
namespace Anycmd.AC.Infra.ViewModels.UIViewViewModels
{
    using Host.AC.InOuts;
    using Model;

    /// <summary>
    /// 
    /// </summary>
    public class UIViewCreateInput : EntityCreateInput, IInputModel, IUIViewCreateIO
    {
        /// <summary>
        /// 
        /// </summary>
        public string Icon { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Tooltip { get; set; }
    }
}

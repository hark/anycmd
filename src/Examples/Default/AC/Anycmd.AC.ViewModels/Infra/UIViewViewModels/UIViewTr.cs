﻿
namespace Anycmd.AC.Infra.ViewModels.UIViewViewModels
{
    using Exceptions;
    using Host;
    using System;

    /// <summary>
    /// 
    /// </summary>
    public class UIViewTr
    {
        private readonly IACDomain host;

        private UIViewTr(IACDomain host)
        {
            this.host = host;
        }

        public static UIViewTr Create(UIViewState view)
        {
            if (view == null)
            {
                return null;
            }
            FunctionState function;
            view.ACDomain.FunctionSet.TryGetFunction(view.Id, out function);
            AppSystemState appSystem;
            view.ACDomain.AppSystemSet.TryGetAppSystem(function.AppSystem.Id, out appSystem);
            return new UIViewTr(view.ACDomain)
            {
                Code = function.Code,
                AppSystemCode = appSystem.Code,
                AppSystemID = appSystem.Id,
                AppSystemName = appSystem.Name,
                ResourceCode = function.Resource.Code,
                CreateOn = view.CreateOn,
                DeveloperID = function.DeveloperID,
                Icon = view.Icon,
                Id = view.Id,
                Description = function.Description
            };
        }

        /// <summary>
        /// 
        /// </summary>
        public virtual Guid Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual Guid AppSystemID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual string AppSystemCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual string AppSystemName { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual string Code { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual string Description { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual string Icon { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual string ResourceCode { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual Guid DeveloperID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual DateTime? CreateOn { get; set; }

        public virtual string DeveloperCode
        {
            get
            {
                AccountState developer;
                if (!host.SysUsers.TryGetDevAccount(this.DeveloperID, out developer))
                {
                    throw new ValidationException("意外的开发人员标识" + this.DeveloperID);
                }
                return developer.LoginName;
            }
        }
    }
}

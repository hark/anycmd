﻿using System;

namespace Anycmd.AC.Infra.ViewModels.UIViewViewModels
{
    using Host.AC.InOuts;
    using Model;

    /// <summary>
    /// 
    /// </summary>
    public class UIViewButtonUpdateInput : IInputModel, IUIViewButtonUpdateIO
    {
        public Guid Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Guid? FunctionID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int IsEnabled { get; set; }
    }
}

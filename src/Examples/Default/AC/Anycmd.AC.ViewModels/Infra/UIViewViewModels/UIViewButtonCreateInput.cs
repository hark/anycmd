﻿using System;

namespace Anycmd.AC.Infra.ViewModels.UIViewViewModels
{
    using Host.AC.InOuts;
    using Model;

    /// <summary>
    /// 
    /// </summary>
    public class UIViewButtonCreateInput : EntityCreateInput, IInputModel, IUIViewButtonCreateIO
    {
        /// <summary>
        /// 
        /// </summary>
        public Guid UIViewID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Guid ButtonID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public Guid? FunctionID { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public int IsEnabled { get; set; }
    }
}

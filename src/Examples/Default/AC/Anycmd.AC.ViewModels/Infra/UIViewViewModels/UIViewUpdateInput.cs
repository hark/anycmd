﻿
namespace Anycmd.AC.Infra.ViewModels.UIViewViewModels
{
    using Host.AC.InOuts;
    using Model;
    using System;

    /// <summary>
    /// 
    /// </summary>
    public class UIViewUpdateInput : IInputModel, IUIViewUpdateIO
    {
        public Guid Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Icon { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string Tooltip { get; set; }
    }
}

﻿
namespace Anycmd.AC.Identity.ViewModels.AccountViewModels
{
    using Host.AC.InOuts;

    public class PasswordChangeInput : IPasswordChangeIO
    {
        public string LoginName { get; set; }

        public string OldPassword { get; set; }

        public string NewPassword { get; set; }
    }
}

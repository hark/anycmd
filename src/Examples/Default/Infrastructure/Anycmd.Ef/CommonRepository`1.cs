﻿
namespace Anycmd.Ef
{
    using Model;
    using Repositories;
    using System;
    using System.Data.Entity;
    using System.Linq;

    public class CommonRepository<TAggregateRoot> : Repository<TAggregateRoot>
        where TAggregateRoot : class, IAggregateRoot
    {
        private readonly string efDbContextName;
        private readonly IACDomain host;

        public CommonRepository(IACDomain host, string efDbContextName)
        {
            this.host = host;
            this.efDbContextName = efDbContextName;
        }

        private EfRepositoryContext EFContext
        {
            get
            {
                var repositoryContext = EfContext.Storage.GetRepositoryContext(this.efDbContextName);
                if (repositoryContext == null)
                {
                    repositoryContext = new EfRepositoryContext(host, this.efDbContextName);
                    EfContext.Storage.SetRepositoryContext(repositoryContext);
                }
                return repositoryContext;
            }
        }

        /// <summary>
        /// 
        /// </summary>
        public override IRepositoryContext Context
        {
            get
            {
                return EFContext;
            }
        }

        public DbContext DbContext
        {
            get
            {
                return EFContext.DbContext;
            }
        }

        #region Protected Methods

        /// <summary>
        /// 查询当前聚合根实体数据源。
        /// </summary>
        /// <returns>对聚合根实体集进行查询计算的接口</returns>
        protected override IQueryable<TAggregateRoot> DoAsQueryable()
        {
            return DbContext.Set<TAggregateRoot>();
        }

        /// <summary>
        /// 根据给定的标识从仓储中获取聚合根实体。
        /// </summary>
        /// <param name="key">聚合根实体的标识</param>
        /// <returns>聚合根实体对象。</returns>
        protected override TAggregateRoot DoGetByKey(ValueType key)
        {
            return DbContext.Set<TAggregateRoot>().Where(p => p.Id == (Guid)key).FirstOrDefault();
        }

        /// <summary>
        /// 添加一个聚合根实体对象到仓储。
        /// </summary>
        /// <param name="aggregateRoot">被添加到聚合根实体仓储的聚合根实体对象。</param>
        protected override void DoAdd(TAggregateRoot aggregateRoot)
        {
            EFContext.RegisterNew(aggregateRoot);
        }

        /// <summary>
        /// 更新当前聚合根实体仓储中的给定的聚合根对象。
        /// </summary>
        /// <param name="aggregateRoot">将被更新的聚合根实体对象。</param>
        protected override void DoUpdate(TAggregateRoot aggregateRoot)
        {
            EFContext.RegisterModified(aggregateRoot);
        }

        /// <summary>
        /// 从仓储中移除给定的聚合根实体。
        /// </summary>
        /// <param name="aggregateRoot">将被移除的聚合根实体。</param>
        protected override void DoRemove(TAggregateRoot aggregateRoot)
        {
            // TODO:区分标记删除
            EFContext.RegisterDeleted(aggregateRoot);
        }
        #endregion
    }
}

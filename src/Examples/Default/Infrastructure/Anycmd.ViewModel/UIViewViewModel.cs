﻿
namespace Anycmd.ViewModel
{
    using Host;
    using System;

    /// <summary>
    /// 界面视图展示层模型。
    /// </summary>
    public class UIViewViewModel
    {
        public static readonly UIViewViewModel Empty = new UIViewViewModel(UIViewState.Empty, "无名页面");

        /// <summary>
        /// 
        /// </summary>
        /// <param name="page"></param>
        /// <param name="title"></param>
        public UIViewViewModel(UIViewState page, string title)
        {
            this.UIView = page;
            this.Id = page.Id;
            this.Tooltip = page.Tooltip;
            this.Icon = page.Icon;
            this.Title = title;
        }

        public UIViewState UIView { get; private set; }

        public Guid Id { get; private set; }

        /// <summary>
        /// 标题
        /// </summary>
        public string Title { get; private set; }

        /// <summary>
        /// 提示型帮助
        /// </summary>
        public string Tooltip { get; private set; }

        public string Icon { get; private set; }
    }
}

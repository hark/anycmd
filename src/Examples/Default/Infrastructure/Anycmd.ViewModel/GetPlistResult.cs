﻿
namespace Anycmd.ViewModel
{
    using Query;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    /// <summary>
    /// 表示分页获取实体集时的输入参数类型。
    /// </summary>
    public class GetPlistResult : PagingInput, IGetPlistResult
    {
        private List<FilterData> _filters;

        /// <summary>
        /// 页索引。零基。如果为空视作0。
        /// </summary>
        [Required]
        public new int? pageIndex
        {
            get
            {
                return base.pageIndex;
            }
            set
            {
                base.pageIndex = !value.HasValue ? 0 : value.Value;
            }
        }

        /// <summary>
        /// 页尺寸。如果为空视作10。
        /// </summary>
        [Required]
        public new int? pageSize
        {
            get
            {
                return base.pageSize;
            }
            set
            {
                base.pageSize = !value.HasValue ? 10 : value.Value;
            }
        }

        /// <summary>
        /// 排序字段。
        /// </summary>
        [Required]
        public new string sortField
        {
            get { return base.sortField; }
            set { base.sortField = value; }
        }

        /// <summary>
        /// 排序方向。
        /// </summary>
        [Required]
        public new string sortOrder
        {
            get { return base.sortOrder; }
            set { base.sortOrder = value; }
        }

        /// <summary>
        /// 设计用于消除一次sql count查询。如果传入的total参数不为0则数据访问层可以避免一次count查询。
        /// 更多信息参见 http://www.cnblogs.com/xuefly/p/3253145.html
        /// </summary>
        public new int? total
        {
            get
            {
                return base.total.HasValue ? (int)base.total.Value : 0;
            }
            set
            {
                if (value.HasValue)
                {
                    base.total = value.Value;
                }
            }
        }

        /// <summary>
        /// 筛选器列表。// TODO:考虑引入daxnet的UQML，更多信息参见 http://www.cnblogs.com/daxnet/p/3925426.html
        /// </summary>
        public List<FilterData> filters
        {
            get
            {
                if (_filters == null)
                {
                    _filters = new List<FilterData>();
                }
                return _filters;
            }
            set
            {
                _filters = value;
            }
        }
    }
}

﻿
namespace Anycmd.Tests
{
    using AC.ViewModels.RoleViewModels;
    using Anycmd.AC;
    using Anycmd.AC.Identity.ViewModels.AccountViewModels;
    using Anycmd.AC.Infra.ViewModels.DicViewModels;
    using Anycmd.AC.Infra.ViewModels.OrganizationViewModels;
    using Anycmd.Host.AC.Identity;
    using Anycmd.Host.AC.Identity.Messages;
    using Anycmd.Host.AC.Infra.Messages;
    using Anycmd.Repositories;
    using Host;
    using Host.AC.Messages;
    using Host.AC.InOuts;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Xunit;

    public class DSDSetTest
    {
        [Fact]
        public void DSDSet()
        {
            var host = TestHelper.GetACDomain();
            Assert.Equal(0, host.DSDSetSet.Count());

            var entityID = Guid.NewGuid();

            DSDSetState dsdSetByID;
            host.Handle(new AddDSDSetCommand(new DSDSetCreateIO
            {
                Id = entityID,
                Name = "测试1",
                Description = "test",
                IsEnabled = 1,
                DSDCard = 2
            }));
            Assert.Equal(1, host.DSDSetSet.Count());
            Assert.True(host.DSDSetSet.TryGetDSDSet(entityID, out dsdSetByID));

            host.Handle(new UpdateDSDSetCommand(new DSDSetUpdateIO
            {
                Id = entityID,
                Name = "test2",
                Description = "test",
                IsEnabled = 1,
                DSDCard = 2
            }));
            Assert.Equal(1, host.DSDSetSet.Count());
            Assert.True(host.DSDSetSet.TryGetDSDSet(entityID, out dsdSetByID));
            Assert.Equal("test2", dsdSetByID.Name);

            host.Handle(new RemoveDSDSetCommand(entityID));
            Assert.False(host.DSDSetSet.TryGetDSDSet(entityID, out dsdSetByID));
            Assert.Equal(0, host.DSDSetSet.Count());
        }

        [Fact]
        public void TestDSDRole()
        {
            var host = TestHelper.GetACDomain();
            Assert.Equal(0, host.DSDSetSet.Count());

            var dsdSetID = Guid.NewGuid();

            DSDSetState dsdSetByID;
            host.Handle(new AddDSDSetCommand(new DSDSetCreateIO
            {
                Id = dsdSetID,
                Name = "测试1",
                Description = "test",
                IsEnabled = 1,
                DSDCard = 2
            }));
            Assert.Equal(1, host.DSDSetSet.Count());
            Assert.True(host.DSDSetSet.TryGetDSDSet(dsdSetID, out dsdSetByID));

            Assert.Equal(0, host.DSDSetSet.GetDSDRoles(dsdSetByID).Count);
            RoleState roleByID;
            var roleID = Guid.NewGuid();
            host.Handle(new AddRoleCommand(new RoleCreateInput
            {
                Id = roleID,
                Name = "测试1",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            }));
            Assert.Equal(1, host.RoleSet.Count());
            Assert.True(host.RoleSet.TryGetRole(roleID, out roleByID));
            var entityID = Guid.NewGuid();
            host.Handle(new AddDSDRoleCommand(new DSDRoleCreateIO
            {
                Id = entityID,
                RoleID = roleID,
                DSDSetID = dsdSetID
            }));
            Assert.Equal(1, host.DSDSetSet.GetDSDRoles(dsdSetByID).Count);
            host.Handle(new RemoveDSDRoleCommand(entityID));
            Assert.Equal(0, host.DSDSetSet.GetDSDRoles(dsdSetByID).Count);
        }

        [Fact]
        public void CheckDSDSetRoles()
        {
            var host = TestHelper.GetACDomain();
            Assert.Equal(0, host.DSDSetSet.Count());

            var ssdSetID = Guid.NewGuid();

            DSDSetState ssdSetByID;
            host.Handle(new AddDSDSetCommand(new DSDSetCreateIO
            {
                Id = ssdSetID,
                Name = "测试1",
                Description = "test",
                IsEnabled = 1,
                DSDCard = 2
            }));
            Assert.Equal(1, host.DSDSetSet.Count());
            Assert.True(host.DSDSetSet.TryGetDSDSet(ssdSetID, out ssdSetByID));

            Assert.Equal(0, host.DSDSetSet.GetDSDRoles(ssdSetByID).Count);
            var entityID = Guid.NewGuid();
            var roleID1 = Guid.NewGuid();
            host.Handle(new AddRoleCommand(new RoleCreateInput
            {
                Id = roleID1,
                Name = "测试1",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            }));
            entityID = Guid.NewGuid();
            host.Handle(new AddDSDRoleCommand(new DSDRoleCreateIO
            {
                Id = entityID,
                RoleID = roleID1,
                DSDSetID = ssdSetID
            }));
            var roleID2 = Guid.NewGuid();
            host.Handle(new AddRoleCommand(new RoleCreateInput
            {
                Id = roleID2,
                Name = "测试2",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            }));
            entityID = Guid.NewGuid();
            host.Handle(new AddDSDRoleCommand(new DSDRoleCreateIO
            {
                Id = entityID,
                RoleID = roleID2,
                DSDSetID = ssdSetID
            }));
            var roleID3 = Guid.NewGuid();
            host.Handle(new AddRoleCommand(new RoleCreateInput
            {
                Id = roleID3,
                Name = "测试3",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            }));
            entityID = Guid.NewGuid();
            host.Handle(new AddDSDRoleCommand(new DSDRoleCreateIO
            {
                Id = entityID,
                RoleID = roleID3,
                DSDSetID = ssdSetID
            }));
            Assert.Equal(3, host.RoleSet.Count());
            Assert.Equal(3, host.DSDSetSet.GetDSDRoles(ssdSetByID).Count);
            var orgID = Guid.NewGuid();

            host.Handle(new AddOrganizationCommand(new OrganizationCreateInput
            {
                Id = orgID,
                Code = "100",
                Name = "测试1",
                Description = "test",
                SortCode = 10,
                Icon = null,
            }));
            Guid dicID = Guid.NewGuid();
            host.Handle(new AddDicCommand(new DicCreateInput
            {
                Id = dicID,
                Code = "auditStatus",
                Name = "auditStatus1"
            }));
            host.Handle(new AddDicItemCommand(new DicItemCreateInput
            {
                Id = dicID,
                IsEnabled = 1,
                DicID = dicID,
                SortCode = 0,
                Description = string.Empty,
                Code = "auditPass",
                Name = "auditPass"
            }));
            Guid accountID = Guid.NewGuid();
            host.Handle(new AddAccountCommand(new AccountCreateInput
            {
                Id = accountID,
                Code = "test",
                Name = "test",
                LoginName = "test",
                Password = "111111",
                OrganizationCode = "100",
                IsEnabled = 1,
                AuditState = "auditPass"
            }));
            Assert.NotNull(host.GetRequiredService<IRepository<Account>>().AsQueryable().FirstOrDefault(a => string.Equals(a.LoginName, "test", StringComparison.OrdinalIgnoreCase)));
            host.SignIn(new Dictionary<string, object>
            {
                {"loginName", "test"},
                {"password", "111111"},
                {"rememberMe", "rememberMe"}
            });
            Assert.True(host.UserSession.Principal.Identity.IsAuthenticated);
            Assert.Equal(0, host.UserSession.AccountPrivilege.Roles.Count);
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = Guid.NewGuid(),
                SubjectInstanceID = accountID,
                SubjectType = ACSubjectType.Account.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = roleID1,
                ObjectType = ACObjectType.Role.ToString()
            }));
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = Guid.NewGuid(),
                SubjectInstanceID = accountID,
                SubjectType = ACSubjectType.Account.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = roleID2,
                ObjectType = ACObjectType.Role.ToString()
            }));
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = Guid.NewGuid(),
                SubjectInstanceID = accountID,
                SubjectType = ACSubjectType.Account.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = roleID3,
                ObjectType = ACObjectType.Role.ToString()
            }));
            bool catched = false;
            host.SignOut();
            try
            {
                host.SignIn(new Dictionary<string, object>
            {
                {"loginName", "test"},
                {"password", "111111"},
                {"rememberMe", "rememberMe"}
            });
                var p = host.UserSession.AccountPrivilege;
            }
            catch
            {
                catched = true;
            }
            Assert.True(catched);
        }
    }
}

﻿
namespace Anycmd.Tests
{
    using AC;
    using AC.Identity.ViewModels.AccountViewModels;
    using AC.Infra.ViewModels.AppSystemViewModels;
    using AC.Infra.ViewModels.DicViewModels;
    using AC.Infra.ViewModels.MenuViewModels;
    using AC.Infra.ViewModels.OrganizationViewModels;
    using AC.ViewModels.GroupViewModels;
    using AC.ViewModels.RoleViewModels;
    using Host;
    using Host.AC;
    using Host.AC.Identity;
    using Host.AC.Identity.Messages;
    using Host.AC.Infra.Messages;
    using Host.AC.Messages;
    using Host.AC.InOuts;
    using Repositories;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Util;
    using Xunit;

    public class UserSessionTest
    {
        #region TestSignInSignOut
        [Fact]
        public void TestSignInSignOut()
        {
            var host = TestHelper.GetACDomain();
            host.SignOut();
            var orgID = Guid.NewGuid();

            host.Handle(new AddOrganizationCommand(new OrganizationCreateInput
            {
                Id = orgID,
                Code = "100",
                Name = "测试1",
                Description = "test",
                SortCode = 10,
                Icon = null,
            }));
            Guid dicID = Guid.NewGuid();
            host.Handle(new AddDicCommand(new DicCreateInput
            {
                Id = dicID,
                Code = "auditStatus",
                Name = "auditStatus1"
            }));
            host.Handle(new AddDicItemCommand(new DicItemCreateInput
            {
                Id = dicID,
                IsEnabled = 1,
                DicID = dicID,
                SortCode = 0,
                Description = string.Empty,
                Code = "auditPass",
                Name = "auditPass"
            }));
            Guid accountID = Guid.NewGuid();
            host.Handle(new AddAccountCommand(new AccountCreateInput
            {
                Id = accountID,
                Code = "test",
                Name = "test",
                LoginName = "test",
                Password = "111111",
                OrganizationCode = "100",
                IsEnabled = 1,
                AuditState = "auditPass"
            }));
            Assert.NotNull(host.GetRequiredService<IRepository<Account>>().AsQueryable().FirstOrDefault(a => string.Equals(a.LoginName, "test", StringComparison.OrdinalIgnoreCase)));
            host.SignIn(new Dictionary<string, object>
            {
                {"loginName", "test"},
                {"password", "111111"},
                {"rememberMe", "rememberMe"}
            });
            Assert.True(host.UserSession.Principal.Identity.IsAuthenticated);
            Assert.Equal("test", host.UserSession.Principal.Identity.Name);
            host.SignOut();
            Assert.False(host.UserSession.Principal.Identity.IsAuthenticated);
        }
        #endregion

        #region TestUserRoles
        [Fact]
        public void TestUserRoles()
        {
            var host = TestHelper.GetACDomain();
            host.SignOut();
            var orgID = Guid.NewGuid();

            host.Handle(new AddOrganizationCommand(new OrganizationCreateInput
            {
                Id = orgID,
                Code = "100",
                Name = "测试1",
                Description = "test",
                SortCode = 10,
                Icon = null,
            }));
            Guid dicID = Guid.NewGuid();
            host.Handle(new AddDicCommand(new DicCreateInput
            {
                Id = dicID,
                Code = "auditStatus",
                Name = "auditStatus1"
            }));
            host.Handle(new AddDicItemCommand(new DicItemCreateInput
            {
                Id = dicID,
                IsEnabled = 1,
                DicID = dicID,
                SortCode = 0,
                Description = string.Empty,
                Code = "auditPass",
                Name = "auditPass"
            }));
            Guid accountID = Guid.NewGuid();
            host.Handle(new AddAccountCommand(new AccountCreateInput
            {
                Id = accountID,
                Code = "test",
                Name = "test",
                LoginName = "test",
                Password = "111111",
                OrganizationCode = "100",
                IsEnabled = 1,
                AuditState = "auditPass"
            }));
            Assert.NotNull(host.GetRequiredService<IRepository<Account>>().AsQueryable().FirstOrDefault(a => string.Equals(a.LoginName, "test", StringComparison.OrdinalIgnoreCase)));
            host.SignIn(new Dictionary<string, object>
            {
                {"loginName", "test"},
                {"password", "111111"},
                {"rememberMe", "rememberMe"}
            });
            Assert.True(host.UserSession.Principal.Identity.IsAuthenticated);
            Assert.Equal(0, host.UserSession.AccountPrivilege.Roles.Count);
            Guid roleID = Guid.NewGuid();
            host.Handle(new AddRoleCommand(new RoleCreateInput
            {
                Id = roleID,
                Name = "测试1",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            }));
            Guid entityID = Guid.NewGuid();
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = accountID,
                SubjectType = ACSubjectType.Account.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = roleID,
                ObjectType = ACObjectType.Role.ToString()
            }));
            Assert.Equal(0, host.PrivilegeSet.Count()); // 主体为账户的权限记录不驻留在内存中所以为0
            RoleState role;
            Assert.True(host.RoleSet.TryGetRole(roleID, out role));
            host.UserSession.AccountPrivilege.AddActiveRole(role);
            Assert.Equal(1, host.UserSession.AccountPrivilege.Roles.Count);
            var privilegeBigram = host.GetRequiredService<IRepository<PrivilegeBigram>>().AsQueryable().FirstOrDefault(a => a.Id == entityID);
            Assert.NotNull(privilegeBigram);
            Assert.Equal(accountID, privilegeBigram.SubjectInstanceID);
            Assert.Equal(roleID, privilegeBigram.ObjectInstanceID);
            Assert.Equal(ACSubjectType.Account.ToName(), privilegeBigram.SubjectType);
            Assert.Equal(ACObjectType.Role.ToName(), privilegeBigram.ObjectType);
            host.SignOut();
            host.SignIn(new Dictionary<string, object>
            {
                {"loginName", "test"},
                {"password", "111111"},
                {"rememberMe", "rememberMe"}
            });
            Assert.Equal(1, host.UserSession.AccountPrivilege.Roles.Count);
            host.Handle(new RemovePrivilegeBigramCommand(entityID));
            Assert.Equal(1, host.UserSession.AccountPrivilege.Roles.Count);
            host.UserSession.AccountPrivilege.DropActiveRole(role);
            Assert.Equal(0, host.UserSession.AccountPrivilege.Roles.Count);
            host.SignOut();
            host.SignIn(new Dictionary<string, object>
            {
                {"loginName", "test"},
                {"password", "111111"},
                {"rememberMe", "rememberMe"}
            });
            Assert.Equal(0, host.UserSession.AccountPrivilege.Roles.Count);
        }
        #endregion

        #region TestUserFunctions
        [Fact]
        public void TestUserFunctions()
        {
            var host = TestHelper.GetACDomain();
            host.SignOut();
            var orgID = Guid.NewGuid();

            host.Handle(new AddOrganizationCommand(new OrganizationCreateInput
            {
                Id = orgID,
                Code = "100",
                Name = "测试1",
                Description = "test",
                SortCode = 10,
                Icon = null,
            }));
            Guid dicID = Guid.NewGuid();
            host.Handle(new AddDicCommand(new DicCreateInput
            {
                Id = dicID,
                Code = "auditStatus",
                Name = "auditStatus1"
            }));
            host.Handle(new AddDicItemCommand(new DicItemCreateInput
            {
                Id = dicID,
                IsEnabled = 1,
                DicID = dicID,
                SortCode = 0,
                Description = string.Empty,
                Code = "auditPass",
                Name = "auditPass"
            }));
            Guid accountID = Guid.NewGuid();
            host.Handle(new AddAccountCommand(new AccountCreateInput
            {
                Id = accountID,
                Code = "test",
                Name = "test",
                LoginName = "test",
                Password = "111111",
                OrganizationCode = "100",
                IsEnabled = 1,
                AuditState = "auditPass"
            }));
            Assert.NotNull(host.GetRequiredService<IRepository<Account>>().AsQueryable().FirstOrDefault(a => string.Equals(a.LoginName, "test", StringComparison.OrdinalIgnoreCase)));
            host.SignIn(new Dictionary<string, object>
            {
                {"loginName", "test"},
                {"password", "111111"},
                {"rememberMe", "rememberMe"}
            });
            Assert.True(host.UserSession.Principal.Identity.IsAuthenticated);
            Assert.Equal(0, host.UserSession.AccountPrivilege.Roles.Count);
            Guid functionID = Guid.NewGuid();
            host.Handle(new AddFunctionCommand(new FunctionCreateInput
            {
                Id = functionID,
                Code = "fun1",
                Description = string.Empty,
                DeveloperID = host.SysUsers.GetDevAccounts().First().Id,
                IsEnabled = 1,
                IsManaged = true,
                ResourceTypeID = host.ResourceTypeSet.First().Id,
                SortCode = 10
            }));
            Guid entityID = Guid.NewGuid();
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = accountID,
                SubjectType = ACSubjectType.Account.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = functionID,
                ObjectType = ACObjectType.Function.ToString()
            }));
            Assert.Equal(0, host.PrivilegeSet.Count()); // 主体为账户的权限记录不驻留在内存中所以为0
            Assert.Equal(0, host.UserSession.AccountPrivilege.Functions.Count);// 需要重新登录才能激活新添加的用户功能授权所以为0
            var privilegeBigram = host.GetRequiredService<IRepository<PrivilegeBigram>>().AsQueryable().FirstOrDefault(a => a.Id == entityID);
            Assert.NotNull(privilegeBigram);
            Assert.Equal(accountID, privilegeBigram.SubjectInstanceID);
            Assert.Equal(functionID, privilegeBigram.ObjectInstanceID);
            Assert.Equal(ACSubjectType.Account.ToName(), privilegeBigram.SubjectType);
            Assert.Equal(ACObjectType.Function.ToName(), privilegeBigram.ObjectType);
            host.SignOut();
            host.SignIn(new Dictionary<string, object>
            {
                {"loginName", "test"},
                {"password", "111111"},
                {"rememberMe", "rememberMe"}
            });
            Assert.Equal(1, host.UserSession.AccountPrivilege.Functions.Count);
            host.Handle(new RemovePrivilegeBigramCommand(entityID));
            Assert.Equal(1, host.UserSession.AccountPrivilege.Functions.Count);
            host.SignOut();
            host.SignIn(new Dictionary<string, object>
            {
                {"loginName", "test"},
                {"password", "111111"},
                {"rememberMe", "rememberMe"}
            });
            Assert.Equal(0, host.UserSession.AccountPrivilege.Functions.Count);
        }
        #endregion

        #region TestUserOrganizations
        [Fact]
        public void TestUserOrganizations()
        {
            var host = TestHelper.GetACDomain();
            host.SignOut();
            var orgID = Guid.NewGuid();

            host.Handle(new AddOrganizationCommand(new OrganizationCreateInput
            {
                Id = orgID,
                Code = "100",
                Name = "测试1",
                Description = "test",
                SortCode = 10,
                Icon = null,
            }));
            Guid dicID = Guid.NewGuid();
            host.Handle(new AddDicCommand(new DicCreateInput
            {
                Id = dicID,
                Code = "auditStatus",
                Name = "auditStatus1"
            }));
            host.Handle(new AddDicItemCommand(new DicItemCreateInput
            {
                Id = dicID,
                IsEnabled = 1,
                DicID = dicID,
                SortCode = 0,
                Description = string.Empty,
                Code = "auditPass",
                Name = "auditPass"
            }));
            Guid accountID = Guid.NewGuid();
            host.Handle(new AddAccountCommand(new AccountCreateInput
            {
                Id = accountID,
                Code = "test",
                Name = "test",
                LoginName = "test",
                Password = "111111",
                OrganizationCode = "100",
                IsEnabled = 1,
                AuditState = "auditPass"
            }));
            Assert.NotNull(host.GetRequiredService<IRepository<Account>>().AsQueryable().FirstOrDefault(a => string.Equals(a.LoginName, "test", StringComparison.OrdinalIgnoreCase)));
            host.SignIn(new Dictionary<string, object>
            {
                {"loginName", "test"},
                {"password", "111111"},
                {"rememberMe", "rememberMe"}
            });
            Assert.True(host.UserSession.Principal.Identity.IsAuthenticated);
            Assert.Equal(0, host.UserSession.AccountPrivilege.Roles.Count);
            Guid organizationID = Guid.NewGuid();
            host.Handle(new AddOrganizationCommand(new OrganizationCreateInput
            {
                Id = organizationID,
                Code = "110",
                Name = "测试110",
                Description = "test",
                SortCode = 10,
                Icon = null,
            }));
            Guid entityID = Guid.NewGuid();
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = accountID,
                SubjectType = ACSubjectType.Account.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = organizationID,
                ObjectType = ACObjectType.Organization.ToString()
            }));
            Assert.Equal(0, host.PrivilegeSet.Count()); // 主体为账户的权限记录不驻留在内存中所以为0
            Assert.Equal(0, host.UserSession.AccountPrivilege.Organizations.Count);// 需要重新登录才能激活新添加的用户功能授权所以为0
            var privilegeBigram = host.GetRequiredService<IRepository<PrivilegeBigram>>().AsQueryable().FirstOrDefault(a => a.Id == entityID);
            Assert.NotNull(privilegeBigram);
            Assert.Equal(accountID, privilegeBigram.SubjectInstanceID);
            Assert.Equal(organizationID, privilegeBigram.ObjectInstanceID);
            Assert.Equal(ACSubjectType.Account.ToName(), privilegeBigram.SubjectType);
            Assert.Equal(ACObjectType.Organization.ToName(), privilegeBigram.ObjectType);
            host.SignOut();
            host.SignIn(new Dictionary<string, object>
            {
                {"loginName", "test"},
                {"password", "111111"},
                {"rememberMe", "rememberMe"}
            });
            Assert.Equal(1, host.UserSession.AccountPrivilege.Organizations.Count);
            host.Handle(new RemovePrivilegeBigramCommand(entityID));
            Assert.Equal(1, host.UserSession.AccountPrivilege.Organizations.Count);
            host.SignOut();
            host.SignIn(new Dictionary<string, object>
            {
                {"loginName", "test"},
                {"password", "111111"},
                {"rememberMe", "rememberMe"}
            });
            Assert.Equal(0, host.UserSession.AccountPrivilege.Organizations.Count);
        }
        #endregion

        #region TestUserGroups
        [Fact]
        public void TestUserGroups()
        {
            var host = TestHelper.GetACDomain();
            host.SignOut();
            var orgID = Guid.NewGuid();

            host.Handle(new AddOrganizationCommand(new OrganizationCreateInput
            {
                Id = orgID,
                Code = "100",
                Name = "测试1",
                Description = "test",
                SortCode = 10,
                Icon = null,
            }));
            Guid dicID = Guid.NewGuid();
            host.Handle(new AddDicCommand(new DicCreateInput
            {
                Id = dicID,
                Code = "auditStatus",
                Name = "auditStatus1"
            }));
            host.Handle(new AddDicItemCommand(new DicItemCreateInput
            {
                Id = dicID,
                IsEnabled = 1,
                DicID = dicID,
                SortCode = 0,
                Description = string.Empty,
                Code = "auditPass",
                Name = "auditPass"
            }));
            Guid accountID = Guid.NewGuid();
            host.Handle(new AddAccountCommand(new AccountCreateInput
            {
                Id = accountID,
                Code = "test",
                Name = "test",
                LoginName = "test",
                Password = "111111",
                OrganizationCode = "100",
                IsEnabled = 1,
                AuditState = "auditPass"
            }));
            Assert.NotNull(host.GetRequiredService<IRepository<Account>>().AsQueryable().FirstOrDefault(a => string.Equals(a.LoginName, "test", StringComparison.OrdinalIgnoreCase)));
            host.SignIn(new Dictionary<string, object>
            {
                {"loginName", "test"},
                {"password", "111111"},
                {"rememberMe", "rememberMe"}
            });
            Assert.True(host.UserSession.Principal.Identity.IsAuthenticated);
            Assert.Equal(0, host.UserSession.AccountPrivilege.Roles.Count);
            Guid groupID = Guid.NewGuid();
            host.Handle(new AddGroupCommand(new GroupCreateInput
            {
                Id = groupID,
                Name = "测试1",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                ShortName = "",
                SortCode = 10,
                TypeCode = "AC"
            }));
            Guid entityID = Guid.NewGuid();
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = accountID,
                SubjectType = ACSubjectType.Account.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = groupID,
                ObjectType = ACObjectType.Group.ToString()
            }));
            Assert.Equal(0, host.PrivilegeSet.Count()); // 主体为账户的权限记录不驻留在内存中所以为0
            Assert.Equal(0, host.UserSession.AccountPrivilege.Groups.Count);// 需要重新登录才能激活新添加的用户功能授权所以为0
            var privilegeBigram = host.GetRequiredService<IRepository<PrivilegeBigram>>().AsQueryable().FirstOrDefault(a => a.Id == entityID);
            Assert.NotNull(privilegeBigram);
            Assert.Equal(accountID, privilegeBigram.SubjectInstanceID);
            Assert.Equal(groupID, privilegeBigram.ObjectInstanceID);
            Assert.Equal(ACSubjectType.Account.ToName(), privilegeBigram.SubjectType);
            Assert.Equal(ACObjectType.Group.ToName(), privilegeBigram.ObjectType);
            host.SignOut();
            host.SignIn(new Dictionary<string, object>
            {
                {"loginName", "test"},
                {"password", "111111"},
                {"rememberMe", "rememberMe"}
            });
            Assert.Equal(1, host.UserSession.AccountPrivilege.Groups.Count);
            host.Handle(new RemovePrivilegeBigramCommand(entityID));
            Assert.Equal(1, host.UserSession.AccountPrivilege.Groups.Count);
            host.SignOut();
            host.SignIn(new Dictionary<string, object>
            {
                {"loginName", "test"},
                {"password", "111111"},
                {"rememberMe", "rememberMe"}
            });
            Assert.Equal(0, host.UserSession.AccountPrivilege.Groups.Count);
        }
        #endregion

        #region TestUserMenus
        [Fact]
        public void TestUserMenus()
        {
            var host = TestHelper.GetACDomain();
            host.SignOut();
            var orgID = Guid.NewGuid();

            host.Handle(new AddOrganizationCommand(new OrganizationCreateInput
            {
                Id = orgID,
                Code = "100",
                Name = "测试1",
                Description = "test",
                SortCode = 10,
                Icon = null,
            }));
            Guid dicID = Guid.NewGuid();
            host.Handle(new AddDicCommand(new DicCreateInput
            {
                Id = dicID,
                Code = "auditStatus",
                Name = "auditStatus1"
            }));
            host.Handle(new AddDicItemCommand(new DicItemCreateInput
            {
                Id = dicID,
                IsEnabled = 1,
                DicID = dicID,
                SortCode = 0,
                Description = string.Empty,
                Code = "auditPass",
                Name = "auditPass"
            }));
            Guid accountID = Guid.NewGuid();
            host.Handle(new AddAccountCommand(new AccountCreateInput
            {
                Id = accountID,
                Code = "test",
                Name = "test",
                LoginName = "test",
                Password = "111111",
                OrganizationCode = "100",
                IsEnabled = 1,
                AuditState = "auditPass"
            }));
            Assert.NotNull(host.GetRequiredService<IRepository<Account>>().AsQueryable().FirstOrDefault(a => string.Equals(a.LoginName, "test", StringComparison.OrdinalIgnoreCase)));
            host.SignIn(new Dictionary<string, object>
            {
                {"loginName", "test"},
                {"password", "111111"},
                {"rememberMe", "rememberMe"}
            });
            Assert.True(host.UserSession.Principal.Identity.IsAuthenticated);
            Assert.Equal(0, host.UserSession.AccountPrivilege.Roles.Count);
            Guid menuID = Guid.NewGuid();
            host.Handle(new AddMenuCommand(new MenuCreateInput
            {
                Id = menuID,
                Name = "测试1",
                Description = "test",
                SortCode = 10,
                AppSystemID = host.AppSystemSet.First().Id,
                Icon = null,
                ParentID = null,
                Url = string.Empty
            }));
            Guid entityID = Guid.NewGuid();
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = accountID,
                SubjectType = ACSubjectType.Account.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = menuID,
                ObjectType = ACObjectType.Menu.ToString()
            }));
            Assert.Equal(0, host.PrivilegeSet.Count()); // 主体为账户的权限记录不驻留在内存中所以为0
            Assert.Equal(0, host.UserSession.AccountPrivilege.Menus.Count);// 需要重新登录才能激活新添加的用户功能授权所以为0
            var privilegeBigram = host.GetRequiredService<IRepository<PrivilegeBigram>>().AsQueryable().FirstOrDefault(a => a.Id == entityID);
            Assert.NotNull(privilegeBigram);
            Assert.Equal(accountID, privilegeBigram.SubjectInstanceID);
            Assert.Equal(menuID, privilegeBigram.ObjectInstanceID);
            Assert.Equal(ACSubjectType.Account.ToName(), privilegeBigram.SubjectType);
            Assert.Equal(ACObjectType.Menu.ToName(), privilegeBigram.ObjectType);
            host.SignOut();
            host.SignIn(new Dictionary<string, object>
            {
                {"loginName", "test"},
                {"password", "111111"},
                {"rememberMe", "rememberMe"}
            });
            Assert.Equal(1, host.UserSession.AccountPrivilege.Menus.Count);
            host.Handle(new RemovePrivilegeBigramCommand(entityID));
            Assert.Equal(1, host.UserSession.AccountPrivilege.Menus.Count);
            host.SignOut();
            host.SignIn(new Dictionary<string, object>
            {
                {"loginName", "test"},
                {"password", "111111"},
                {"rememberMe", "rememberMe"}
            });
            Assert.Equal(0, host.UserSession.AccountPrivilege.Menus.Count);
        }
        #endregion

        #region TestUserAppSystems
        [Fact]
        public void TestUserAppSystems()
        {
            var host = TestHelper.GetACDomain();
            host.SignOut();
            var orgID = Guid.NewGuid();

            host.Handle(new AddOrganizationCommand(new OrganizationCreateInput
            {
                Id = orgID,
                Code = "100",
                Name = "测试1",
                Description = "test",
                SortCode = 10,
                Icon = null,
            }));
            Guid dicID = Guid.NewGuid();
            host.Handle(new AddDicCommand(new DicCreateInput
            {
                Id = dicID,
                Code = "auditStatus",
                Name = "auditStatus1"
            }));
            host.Handle(new AddDicItemCommand(new DicItemCreateInput
            {
                Id = dicID,
                IsEnabled = 1,
                DicID = dicID,
                SortCode = 0,
                Description = string.Empty,
                Code = "auditPass",
                Name = "auditPass"
            }));
            Guid accountID = Guid.NewGuid();
            host.Handle(new AddAccountCommand(new AccountCreateInput
            {
                Id = accountID,
                Code = "test",
                Name = "test",
                LoginName = "test",
                Password = "111111",
                OrganizationCode = "100",
                IsEnabled = 1,
                AuditState = "auditPass"
            }));
            Assert.NotNull(host.GetRequiredService<IRepository<Account>>().AsQueryable().FirstOrDefault(a => string.Equals(a.LoginName, "test", StringComparison.OrdinalIgnoreCase)));
            host.SignIn(new Dictionary<string, object>
            {
                {"loginName", "test"},
                {"password", "111111"},
                {"rememberMe", "rememberMe"}
            });
            Assert.True(host.UserSession.Principal.Identity.IsAuthenticated);
            Assert.Equal(0, host.UserSession.AccountPrivilege.Roles.Count);
            Guid appSystemID = Guid.NewGuid();
            host.Handle(new AddAppSystemCommand(new AppSystemCreateInput
            {
                Id = appSystemID,
                Code = "app1",
                Name = "测试1",
                PrincipalID = host.SysUsers.GetDevAccounts().First().Id
            }));
            Guid entityID = Guid.NewGuid();
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = accountID,
                SubjectType = ACSubjectType.Account.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = appSystemID,
                ObjectType = ACObjectType.AppSystem.ToString()
            }));
            Assert.Equal(0, host.PrivilegeSet.Count()); // 主体为账户的权限记录不驻留在内存中所以为0
            Assert.Equal(0, host.UserSession.AccountPrivilege.AppSystems.Count);// 需要重新登录才能激活新添加的用户功能授权所以为0
            var privilegeBigram = host.GetRequiredService<IRepository<PrivilegeBigram>>().AsQueryable().FirstOrDefault(a => a.Id == entityID);
            Assert.NotNull(privilegeBigram);
            Assert.Equal(accountID, privilegeBigram.SubjectInstanceID);
            Assert.Equal(appSystemID, privilegeBigram.ObjectInstanceID);
            Assert.Equal(ACSubjectType.Account.ToName(), privilegeBigram.SubjectType);
            Assert.Equal(ACObjectType.AppSystem.ToName(), privilegeBigram.ObjectType);
            host.SignOut();
            host.SignIn(new Dictionary<string, object>
            {
                {"loginName", "test"},
                {"password", "111111"},
                {"rememberMe", "rememberMe"}
            });
            Assert.Equal(1, host.UserSession.AccountPrivilege.AppSystems.Count);
            host.Handle(new RemovePrivilegeBigramCommand(entityID));
            Assert.Equal(1, host.UserSession.AccountPrivilege.AppSystems.Count);
            host.SignOut();
            host.SignIn(new Dictionary<string, object>
            {
                {"loginName", "test"},
                {"password", "111111"},
                {"rememberMe", "rememberMe"}
            });
            Assert.Equal(0, host.UserSession.AccountPrivilege.AppSystems.Count);
        }
        #endregion

        #region TestUserRolePrivilege
        [Fact]
        public void TestUserRolePrivilege()
        {
            var host = TestHelper.GetACDomain();
            host.SignOut();
            var orgID = Guid.NewGuid();

            host.Handle(new AddOrganizationCommand(new OrganizationCreateInput
            {
                Id = orgID,
                Code = "100",
                Name = "测试1",
                Description = "test",
                SortCode = 10,
                Icon = null,
            }));
            Guid dicID = Guid.NewGuid();
            host.Handle(new AddDicCommand(new DicCreateInput
            {
                Id = dicID,
                Code = "auditStatus",
                Name = "auditStatus1"
            }));
            host.Handle(new AddDicItemCommand(new DicItemCreateInput
            {
                Id = dicID,
                IsEnabled = 1,
                DicID = dicID,
                SortCode = 0,
                Description = string.Empty,
                Code = "auditPass",
                Name = "auditPass"
            }));
            Guid accountID = Guid.NewGuid();
            host.Handle(new AddAccountCommand(new AccountCreateInput
            {
                Id = accountID,
                Code = "test",
                Name = "test",
                LoginName = "test",
                Password = "111111",
                OrganizationCode = "100",
                IsEnabled = 1,
                AuditState = "auditPass"
            }));
            Assert.NotNull(host.GetRequiredService<IRepository<Account>>().AsQueryable().FirstOrDefault(a => string.Equals(a.LoginName, "test", StringComparison.OrdinalIgnoreCase)));
            host.SignIn(new Dictionary<string, object>
            {
                {"loginName", "test"},
                {"password", "111111"},
                {"rememberMe", "rememberMe"}
            });
            Assert.True(host.UserSession.Principal.Identity.IsAuthenticated);
            Assert.Equal(0, host.UserSession.AccountPrivilege.Roles.Count);
            Guid roleID = Guid.NewGuid();
            host.Handle(new AddRoleCommand(new RoleCreateInput
            {
                Id = roleID,
                Name = "测试1",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            }));
            Guid entityID = Guid.NewGuid();
            // 授予账户角色
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = accountID,
                SubjectType = ACSubjectType.Account.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = roleID,
                ObjectType = ACObjectType.Role.ToString()
            }));
            Guid organizationID = Guid.NewGuid();
            host.Handle(new AddOrganizationCommand(new OrganizationCreateInput
            {
                Id = organizationID,
                Code = "110",
                Name = "测试110",
                Description = "test",
                SortCode = 10,
                Icon = null,
            }));
            entityID = Guid.NewGuid();
            // 授予账户组织结构
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = accountID,
                SubjectType = ACSubjectType.Account.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = organizationID,
                ObjectType = ACObjectType.Organization.ToString()
            }));
            // 授予组织结构角色
            entityID = Guid.NewGuid();
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = organizationID,
                SubjectType = ACSubjectType.Organization.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = roleID,
                ObjectType = ACObjectType.Role.ToString()
            }));
            Guid groupID = Guid.NewGuid();
            host.Handle(new AddGroupCommand(new GroupCreateInput
            {
                Id = groupID,
                Name = "测试1",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                ShortName = "",
                SortCode = 10,
                TypeCode = "AC"
            }));
            entityID = Guid.NewGuid();
            // 授予账户工作组
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = accountID,
                SubjectType = ACSubjectType.Account.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = groupID,
                ObjectType = ACObjectType.Group.ToString()
            }));
            // 授予工作组角色
            entityID = Guid.NewGuid();
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = roleID,
                SubjectType = ACSubjectType.Role.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = groupID,
                ObjectType = ACObjectType.Group.ToString()
            }));
            host.SignOut();
            host.SignIn(new Dictionary<string, object>
            {
                {"loginName", "test"},
                {"password", "111111"},
                {"rememberMe", "rememberMe"}
            });
            Assert.Equal(1, host.UserSession.AccountPrivilege.Roles.Count);
            Assert.Equal(1, host.UserSession.AccountPrivilege.AuthorizedRoles.Count);
            roleID = Guid.NewGuid();
            // 添加一个新角色并将该角色授予上面创建的组织结构
            host.Handle(new AddRoleCommand(new RoleCreateInput
            {
                Id = roleID,
                Name = "测试2",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            }));
            entityID = Guid.NewGuid();
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = organizationID,
                SubjectType = ACSubjectType.Organization.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = roleID,
                ObjectType = ACObjectType.Role.ToString()
            }));
            host.SignOut();
            host.SignIn(new Dictionary<string, object>
            {
                {"loginName", "test"},
                {"password", "111111"},
                {"rememberMe", "rememberMe"}
            });
            Assert.Equal(1, host.UserSession.AccountPrivilege.Roles.Count);
            Assert.Equal(2, host.UserSession.AccountPrivilege.AuthorizedRoles.Count);
            entityID = Guid.NewGuid();
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = roleID,
                SubjectType = ACSubjectType.Role.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = groupID,
                ObjectType = ACObjectType.Group.ToString()
            }));
            host.SignOut();
            host.SignIn(new Dictionary<string, object>
            {
                {"loginName", "test"},
                {"password", "111111"},
                {"rememberMe", "rememberMe"}
            });
            Assert.Equal(1, host.UserSession.AccountPrivilege.Roles.Count);
            Assert.Equal(2, host.UserSession.AccountPrivilege.AuthorizedRoles.Count);
            roleID = Guid.NewGuid();
            // 添加一个新角色并将该角色授予上面创建的工作组
            host.Handle(new AddRoleCommand(new RoleCreateInput
            {
                Id = roleID,
                Name = "测试3",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            }));
            entityID = Guid.NewGuid();
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = roleID,
                SubjectType = ACSubjectType.Role.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = groupID,
                ObjectType = ACObjectType.Group.ToString()
            }));

            host.SignOut();
            host.SignIn(new Dictionary<string, object>
            {
                {"loginName", "test"},
                {"password", "111111"},
                {"rememberMe", "rememberMe"}
            });
            Assert.Equal(1, host.UserSession.AccountPrivilege.Roles.Count);
            Assert.Equal(3, host.UserSession.AccountPrivilege.AuthorizedRoles.Count);// 用户的全部角色来自直接角色、组织结构角色、工作组角色三者的并集所以是三个角色。
        }
        #endregion

        #region TestUserFunctionPrivilege
        [Fact]
        public void TestUserFunctionPrivilege()
        {
            var host = TestHelper.GetACDomain();
            host.SignOut();
            var orgID = Guid.NewGuid();

            host.Handle(new AddOrganizationCommand(new OrganizationCreateInput
            {
                Id = orgID,
                Code = "100",
                Name = "测试1",
                Description = "test",
                SortCode = 10,
                Icon = null,
            }));
            Guid dicID = Guid.NewGuid();
            host.Handle(new AddDicCommand(new DicCreateInput
            {
                Id = dicID,
                Code = "auditStatus",
                Name = "auditStatus1"
            }));
            host.Handle(new AddDicItemCommand(new DicItemCreateInput
            {
                Id = dicID,
                IsEnabled = 1,
                DicID = dicID,
                SortCode = 0,
                Description = string.Empty,
                Code = "auditPass",
                Name = "auditPass"
            }));
            Guid accountID = Guid.NewGuid();
            host.Handle(new AddAccountCommand(new AccountCreateInput
            {
                Id = accountID,
                Code = "test",
                Name = "test",
                LoginName = "test",
                Password = "111111",
                OrganizationCode = "100",
                IsEnabled = 1,
                AuditState = "auditPass"
            }));
            Assert.NotNull(host.GetRequiredService<IRepository<Account>>().AsQueryable().FirstOrDefault(a => string.Equals(a.LoginName, "test", StringComparison.OrdinalIgnoreCase)));
            Guid roleID = Guid.NewGuid();
            host.Handle(new AddRoleCommand(new RoleCreateInput
            {
                Id = roleID,
                Name = "测试1",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            }));
            var functionID = Guid.NewGuid();
            host.Handle(new AddFunctionCommand(new FunctionCreateInput
            {
                Id = functionID,
                Code = "fun1",
                Description = string.Empty,
                DeveloperID = host.SysUsers.GetDevAccounts().First().Id,
                IsEnabled = 1,
                IsManaged = true,
                ResourceTypeID = host.ResourceTypeSet.First().Id,
                SortCode = 10
            }));
            Guid entityID = Guid.NewGuid();
            // 授予角色功能
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = roleID,
                SubjectType = ACSubjectType.Role.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = functionID,
                ObjectType = ACObjectType.Function.ToString()
            }));
            // 授予账户角色
            entityID = Guid.NewGuid();
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = accountID,
                SubjectType = ACSubjectType.Account.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = roleID,
                ObjectType = ACObjectType.Role.ToString()
            }));
            entityID = Guid.NewGuid();
            functionID = Guid.NewGuid();
            host.Handle(new AddFunctionCommand(new FunctionCreateInput
            {
                Id = functionID,
                Code = "fun2",
                Description = string.Empty,
                DeveloperID = host.SysUsers.GetDevAccounts().First().Id,
                IsEnabled = 1,
                IsManaged = true,
                ResourceTypeID = host.ResourceTypeSet.First().Id,
                SortCode = 10
            }));
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = accountID,
                SubjectType = ACSubjectType.Account.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = functionID,
                ObjectType = ACObjectType.Function.ToString()
            }));
            host.SignIn(new Dictionary<string, object>
            {
                {"loginName", "test"},
                {"password", "111111"},
                {"rememberMe", "rememberMe"}
            });
            Assert.Equal(1, host.UserSession.AccountPrivilege.Functions.Count);
            Assert.Equal(2, host.UserSession.AccountPrivilege.AuthorizedFunctions.Count);
        }
        #endregion

        #region TestUserMenuPrivileges
        [Fact]
        public void TestUserMenuPrivileges()
        {
            var host = TestHelper.GetACDomain();
            host.SignOut();
            var orgID = Guid.NewGuid();

            host.Handle(new AddOrganizationCommand(new OrganizationCreateInput
            {
                Id = orgID,
                Code = "100",
                Name = "测试1",
                Description = "test",
                SortCode = 10,
                Icon = null,
            }));
            Guid dicID = Guid.NewGuid();
            host.Handle(new AddDicCommand(new DicCreateInput
            {
                Id = dicID,
                Code = "auditStatus",
                Name = "auditStatus1"
            }));
            host.Handle(new AddDicItemCommand(new DicItemCreateInput
            {
                Id = dicID,
                IsEnabled = 1,
                DicID = dicID,
                SortCode = 0,
                Description = string.Empty,
                Code = "auditPass",
                Name = "auditPass"
            }));
            Guid accountID = Guid.NewGuid();
            host.Handle(new AddAccountCommand(new AccountCreateInput
            {
                Id = accountID,
                Code = "test",
                Name = "test",
                LoginName = "test",
                Password = "111111",
                OrganizationCode = "100",
                IsEnabled = 1,
                AuditState = "auditPass"
            }));
            Assert.NotNull(host.GetRequiredService<IRepository<Account>>().AsQueryable().FirstOrDefault(a => string.Equals(a.LoginName, "test", StringComparison.OrdinalIgnoreCase)));
            Guid roleID = Guid.NewGuid();
            host.Handle(new AddRoleCommand(new RoleCreateInput
            {
                Id = roleID,
                Name = "测试1",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            }));
            var menuID = Guid.NewGuid();
            host.Handle(new AddMenuCommand(new MenuCreateInput
            {
                Id = menuID,
                Name = "测试1",
                Description = "test",
                SortCode = 10,
                AppSystemID = host.AppSystemSet.First().Id,
                Icon = null,
                ParentID = null,
                Url = string.Empty
            }));
            Guid entityID = Guid.NewGuid();
            // 授予角色菜单
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = roleID,
                SubjectType = ACSubjectType.Role.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = menuID,
                ObjectType = ACObjectType.Menu.ToString()
            }));
            // 授予账户角色
            entityID = Guid.NewGuid();
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = accountID,
                SubjectType = ACSubjectType.Account.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = roleID,
                ObjectType = ACObjectType.Role.ToString()
            }));
            entityID = Guid.NewGuid();
            menuID = Guid.NewGuid();
            host.Handle(new AddMenuCommand(new MenuCreateInput
            {
                Id = menuID,
                Name = "测试2",
                Description = "test",
                SortCode = 10,
                AppSystemID = host.AppSystemSet.First().Id,
                Icon = null,
                ParentID = null,
                Url = string.Empty
            }));
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = accountID,
                SubjectType = ACSubjectType.Account.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = menuID,
                ObjectType = ACObjectType.Menu.ToString()
            }));
            host.SignIn(new Dictionary<string, object>
            {
                {"loginName", "test"},
                {"password", "111111"},
                {"rememberMe", "rememberMe"}
            });
            Assert.Equal(1, host.UserSession.AccountPrivilege.Menus.Count);
            Assert.Equal(2, host.UserSession.AccountPrivilege.AuthorizedMenus.Count);
        }
        #endregion
    }
}

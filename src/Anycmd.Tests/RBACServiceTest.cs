﻿
namespace Anycmd.Tests
{
    using AC;
    using AC.Identity.ViewModels.AccountViewModels;
    using AC.Infra.ViewModels.AppSystemViewModels;
    using AC.Infra.ViewModels.DicViewModels;
    using AC.Infra.ViewModels.OrganizationViewModels;
    using AC.ViewModels.GroupViewModels;
    using AC.ViewModels.RoleViewModels;
    using Host;
    using Host.AC;
    using Host.AC.Identity;
    using Host.AC.Identity.Messages;
    using Host.AC.Infra.Messages;
    using Host.AC.Messages;
    using Host.AC.InOuts;
    using Repositories;
    using System;
    using System.Linq;
    using Xunit;

    public class RBACServiceTest
    {
        #region TestAddUser
        [Fact]
        public void TestAddUser()
        {
            var host = TestHelper.GetACDomain();
            var rbacService = host.GetRequiredService<IRBACService>();
            var accountRepository = host.GetRequiredService<IRepository<Account>>();
            host.Handle(new AddOrganizationCommand(new OrganizationCreateInput
            {
                Id = Guid.NewGuid(),
                Code = "100",
                Name = "测试1",
                Description = "test",
                SortCode = 10,
                Icon = null,
            }));
            var accountID = Guid.NewGuid();
            rbacService.AddUser(new AccountCreateInput
            {
                Id = accountID,
                LoginName = "test",
                Code = "test",
                Name = "test",
                Password = "111111",
                OrganizationCode = "100"
            });
            var entity = accountRepository.GetByKey(accountID);
            Assert.NotNull(entity);
        }
        #endregion

        #region TestDeleteUser
        [Fact]
        public void TestDeleteUser()
        {
            var host = TestHelper.GetACDomain();
            var rbacService = host.GetRequiredService<IRBACService>();
            var accountRepository = host.GetRequiredService<IRepository<Account>>();
            host.Handle(new AddOrganizationCommand(new OrganizationCreateInput
            {
                Id = Guid.NewGuid(),
                Code = "100",
                Name = "测试1",
                Description = "test",
                SortCode = 10,
                Icon = null,
            }));
            var accountID = Guid.NewGuid();
            rbacService.AddUser(new AccountCreateInput
            {
                Id = accountID,
                LoginName = "test",
                Code = "test",
                Name = "test",
                Password = "111111",
                OrganizationCode = "100"
            });
            rbacService.DeleteUser(accountID);
            var entity = accountRepository.GetByKey(accountID);
            Assert.Null(entity);
        }
        #endregion

        #region TestAddRole
        [Fact]
        public void TestAddRole()
        {
            var host = TestHelper.GetACDomain();
            var rbacService = host.GetRequiredService<IRBACService>();
            var entityID = Guid.NewGuid();

            RoleState roleByID;
            rbacService.AddRole(new RoleCreateInput
            {
                Id = entityID,
                Name = "测试1",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            });
            Assert.Equal(1, host.RoleSet.Count());
            Assert.True(host.RoleSet.TryGetRole(entityID, out roleByID));
        }
        #endregion

        #region TestDeleteRole
        [Fact]
        public void TestDeleteRole()
        {
            var host = TestHelper.GetACDomain();
            var rbacService = host.GetRequiredService<IRBACService>();
            var entityID = Guid.NewGuid();

            RoleState roleByID;
            rbacService.AddRole(new RoleCreateInput
            {
                Id = entityID,
                Name = "测试1",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            });
            Assert.Equal(1, host.RoleSet.Count());
            Assert.True(host.RoleSet.TryGetRole(entityID, out roleByID));
            rbacService.DeleteRole(entityID);
            Assert.Equal(0, host.RoleSet.Count());
            Assert.False(host.RoleSet.TryGetRole(entityID, out roleByID));
        }
        #endregion

        #region TestAssignUser
        [Fact]
        public void TestAssignUser()
        {
            var host = TestHelper.GetACDomain();
            var rbacService = host.GetRequiredService<IRBACService>();
            var privilegeBigramRepository = host.GetRequiredService<IRepository<PrivilegeBigram>>();
            var roleID = Guid.NewGuid();
            rbacService.AddRole(new RoleCreateInput
            {
                Id = roleID,
                Name = "测试1",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            });
            host.Handle(new AddOrganizationCommand(new OrganizationCreateInput
            {
                Id = Guid.NewGuid(),
                Code = "100",
                Name = "测试1",
                Description = "test",
                SortCode = 10,
                Icon = null,
            }));
            var accountID = Guid.NewGuid();
            rbacService.AddUser(new AccountCreateInput
            {
                Id = accountID,
                LoginName = "test",
                Code = "test",
                Name = "test",
                Password = "111111",
                OrganizationCode = "100"
            });
            rbacService.AssignUser(accountID, roleID);
            var entity = privilegeBigramRepository.AsQueryable().FirstOrDefault(a => a.SubjectInstanceID == accountID && a.ObjectInstanceID == roleID);
            Assert.NotNull(entity);
        }
        #endregion

        #region TestDeassignUser
        [Fact]
        public void TestDeassignUser()
        {
            var host = TestHelper.GetACDomain();
            var rbacService = host.GetRequiredService<IRBACService>();
            var privilegeBigramRepository = host.GetRequiredService<IRepository<PrivilegeBigram>>();
            var roleID = Guid.NewGuid();
            rbacService.AddRole(new RoleCreateInput
            {
                Id = roleID,
                Name = "测试1",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            });
            host.Handle(new AddOrganizationCommand(new OrganizationCreateInput
            {
                Id = Guid.NewGuid(),
                Code = "100",
                Name = "测试1",
                Description = "test",
                SortCode = 10,
                Icon = null,
            }));
            var accountID = Guid.NewGuid();
            rbacService.AddUser(new AccountCreateInput
            {
                Id = accountID,
                LoginName = "test",
                Code = "test",
                Name = "test",
                Password = "111111",
                OrganizationCode = "100"
            });
            rbacService.AssignUser(accountID, roleID);
            var entity = privilegeBigramRepository.AsQueryable().FirstOrDefault(a => a.SubjectInstanceID == accountID && a.ObjectInstanceID == roleID);
            Assert.NotNull(entity);
            rbacService.DeassignUser(accountID, roleID);
            entity = privilegeBigramRepository.AsQueryable().FirstOrDefault(a => a.SubjectInstanceID == accountID && a.ObjectInstanceID == roleID);
            Assert.Null(entity);
        }
        #endregion

        #region TestGrantPermission
        [Fact]
        public void TestGrantPermission()
        {
            var host = TestHelper.GetACDomain();
            var rbacService = host.GetRequiredService<IRBACService>();
            var privilegeBigramRepository = host.GetRequiredService<IRepository<PrivilegeBigram>>();
            var roleID = Guid.NewGuid();
            rbacService.AddRole(new RoleCreateInput
            {
                Id = roleID,
                Name = "测试1",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            });
            var functionID = Guid.NewGuid();
            host.Handle(new AddFunctionCommand(new FunctionCreateInput
            {
                Id = functionID,
                Code = "fun1",
                Description = string.Empty,
                DeveloperID = host.SysUsers.GetDevAccounts().First().Id,
                IsEnabled = 1,
                IsManaged = true,
                ResourceTypeID = host.ResourceTypeSet.First().Id,
                SortCode = 10
            }));
            rbacService.GrantPermission(functionID, roleID);
            var entity = privilegeBigramRepository.AsQueryable().FirstOrDefault(a => a.SubjectInstanceID == roleID && a.ObjectInstanceID == functionID);
            Assert.NotNull(entity);
            Assert.NotNull(host.PrivilegeSet.FirstOrDefault(a => a.SubjectInstanceID == roleID && a.ObjectInstanceID == functionID));
        }
        #endregion

        #region TestRevokePermission
        [Fact]
        public void TestRevokePermission()
        {
            var host = TestHelper.GetACDomain();
            var rbacService = host.GetRequiredService<IRBACService>();
            var privilegeBigramRepository = host.GetRequiredService<IRepository<PrivilegeBigram>>();
            var roleID = Guid.NewGuid();
            rbacService.AddRole(new RoleCreateInput
            {
                Id = roleID,
                Name = "测试1",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            });
            var functionID = Guid.NewGuid();
            host.Handle(new AddFunctionCommand(new FunctionCreateInput
            {
                Id = functionID,
                Code = "fun1",
                Description = string.Empty,
                DeveloperID = host.SysUsers.GetDevAccounts().First().Id,
                IsEnabled = 1,
                IsManaged = true,
                ResourceTypeID = host.ResourceTypeSet.First().Id,
                SortCode = 10
            }));
            rbacService.GrantPermission(functionID, roleID);
            var entity = privilegeBigramRepository.AsQueryable().FirstOrDefault(a => a.SubjectInstanceID == roleID && a.ObjectInstanceID == functionID);
            Assert.NotNull(entity);
            Assert.NotNull(host.PrivilegeSet.FirstOrDefault(a => a.SubjectInstanceID == roleID && a.ObjectInstanceID == functionID));
            rbacService.RevokePermission(functionID, roleID);
            entity = privilegeBigramRepository.AsQueryable().FirstOrDefault(a => a.SubjectInstanceID == roleID && a.ObjectInstanceID == functionID);
            Assert.Null(entity);
            Assert.Null(host.PrivilegeSet.FirstOrDefault(a => a.SubjectInstanceID == roleID && a.ObjectInstanceID == functionID));
        }
        #endregion

        #region TestCreateSession
        [Fact]
        public void TestCreateSession()
        {
            var host = TestHelper.GetACDomain();
            var rbacService = host.GetRequiredService<IRBACService>();
            var accountRepository = host.GetRequiredService<IRepository<Account>>();
            var sessionRepository = host.GetRequiredService<IRepository<UserSession>>();
            host.Handle(new AddOrganizationCommand(new OrganizationCreateInput
            {
                Id = Guid.NewGuid(),
                Code = "100",
                Name = "测试1",
                Description = "test",
                SortCode = 10,
                Icon = null,
            }));
            var accountID = Guid.NewGuid();
            rbacService.AddUser(new AccountCreateInput
            {
                Id = accountID,
                LoginName = "test",
                Code = "test",
                Name = "test",
                Password = "111111",
                OrganizationCode = "100"
            });
            var account = accountRepository.GetByKey(accountID);
            var sessionID = Guid.NewGuid();
            var userSession = rbacService.CreateSession(sessionID, AccountState.Create(account));
            Assert.NotNull(userSession);
            var sessionEntity = sessionRepository.GetByKey(sessionID);
            Assert.NotNull(sessionEntity);
        }
        #endregion

        #region TestDeleteSession
        [Fact]
        public void TestDeleteSession()
        {
            var host = TestHelper.GetACDomain();
            var rbacService = host.GetRequiredService<IRBACService>();
            var accountRepository = host.GetRequiredService<IRepository<Account>>();
            var sessionRepository = host.GetRequiredService<IRepository<UserSession>>();
            host.Handle(new AddOrganizationCommand(new OrganizationCreateInput
            {
                Id = Guid.NewGuid(),
                Code = "100",
                Name = "测试1",
                Description = "test",
                SortCode = 10,
                Icon = null,
            }));
            var accountID = Guid.NewGuid();
            rbacService.AddUser(new AccountCreateInput
            {
                Id = accountID,
                LoginName = "test",
                Code = "test",
                Name = "test",
                Password = "111111",
                OrganizationCode = "100"
            });
            var account = accountRepository.GetByKey(accountID);
            var sessionID = Guid.NewGuid();
            var userSession = rbacService.CreateSession(sessionID, AccountState.Create(account));
            Assert.NotNull(userSession);
            var sessionEntity = sessionRepository.GetByKey(sessionID);
            Assert.NotNull(sessionEntity);
            rbacService.DeleteSession(sessionID);
            sessionEntity = sessionRepository.GetByKey(sessionID);
            Assert.Null(sessionEntity);
        }
        #endregion

        #region TestSessionRoles
        [Fact]
        public void TestSessionRoles()
        {
            var host = TestHelper.GetACDomain();
            var rbacService = host.GetRequiredService<IRBACService>();
            var accountRepository = host.GetRequiredService<IRepository<Account>>();
            var sessionRepository = host.GetRequiredService<IRepository<UserSession>>();
            var orgID = Guid.NewGuid();

            host.Handle(new AddOrganizationCommand(new OrganizationCreateInput
            {
                Id = orgID,
                Code = "100",
                Name = "测试1",
                Description = "test",
                SortCode = 10,
                Icon = null,
            }));
            Guid dicID = Guid.NewGuid();
            host.Handle(new AddDicCommand(new DicCreateInput
            {
                Id = dicID,
                Code = "auditStatus",
                Name = "auditStatus1"
            }));
            host.Handle(new AddDicItemCommand(new DicItemCreateInput
            {
                Id = dicID,
                IsEnabled = 1,
                DicID = dicID,
                SortCode = 0,
                Description = string.Empty,
                Code = "auditPass",
                Name = "auditPass"
            }));
            Guid accountID = Guid.NewGuid();
            host.Handle(new AddAccountCommand(new AccountCreateInput
            {
                Id = accountID,
                Code = "test",
                Name = "test",
                LoginName = "test",
                Password = "111111",
                OrganizationCode = "100",
                IsEnabled = 1,
                AuditState = "auditPass"
            }));
            Guid roleID = Guid.NewGuid();
            host.Handle(new AddRoleCommand(new RoleCreateInput
            {
                Id = roleID,
                Name = "测试1",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            }));
            Guid entityID = Guid.NewGuid();
            // 授予账户角色
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = accountID,
                SubjectType = ACSubjectType.Account.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = roleID,
                ObjectType = ACObjectType.Role.ToString()
            }));
            Guid organizationID = Guid.NewGuid();
            host.Handle(new AddOrganizationCommand(new OrganizationCreateInput
            {
                Id = organizationID,
                Code = "110",
                Name = "测试110",
                Description = "test",
                SortCode = 10,
                Icon = null,
            }));
            entityID = Guid.NewGuid();
            // 授予账户组织结构
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = accountID,
                SubjectType = ACSubjectType.Account.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = organizationID,
                ObjectType = ACObjectType.Organization.ToString()
            }));
            // 授予组织结构角色
            entityID = Guid.NewGuid();
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = organizationID,
                SubjectType = ACSubjectType.Organization.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = roleID,
                ObjectType = ACObjectType.Role.ToString()
            }));
            Guid groupID = Guid.NewGuid();
            host.Handle(new AddGroupCommand(new GroupCreateInput
            {
                Id = groupID,
                Name = "测试1",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                ShortName = "",
                SortCode = 10,
                TypeCode = "AC"
            }));
            entityID = Guid.NewGuid();
            // 授予账户工作组
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = accountID,
                SubjectType = ACSubjectType.Account.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = groupID,
                ObjectType = ACObjectType.Group.ToString()
            }));
            // 授予工作组角色
            entityID = Guid.NewGuid();
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = roleID,
                SubjectType = ACSubjectType.Role.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = groupID,
                ObjectType = ACObjectType.Group.ToString()
            }));
            roleID = Guid.NewGuid();
            // 添加一个新角色并将该角色授予上面创建的组织结构
            host.Handle(new AddRoleCommand(new RoleCreateInput
            {
                Id = roleID,
                Name = "测试2",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            }));
            entityID = Guid.NewGuid();
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = organizationID,
                SubjectType = ACSubjectType.Organization.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = roleID,
                ObjectType = ACObjectType.Role.ToString()
            }));
            entityID = Guid.NewGuid();
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = roleID,
                SubjectType = ACSubjectType.Role.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = groupID,
                ObjectType = ACObjectType.Group.ToString()
            }));
            roleID = Guid.NewGuid();
            // 添加一个新角色并将该角色授予上面创建的工作组
            host.Handle(new AddRoleCommand(new RoleCreateInput
            {
                Id = roleID,
                Name = "测试3",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            }));
            entityID = Guid.NewGuid();
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = roleID,
                SubjectType = ACSubjectType.Role.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = groupID,
                ObjectType = ACObjectType.Group.ToString()
            }));
            var account = accountRepository.GetByKey(accountID);
            var sessionID = Guid.NewGuid();
            var userSession = rbacService.CreateSession(sessionID, AccountState.Create(account));
            Assert.NotNull(userSession);
            var sessionEntity = sessionRepository.GetByKey(sessionID);
            Assert.NotNull(sessionEntity);
            Assert.Equal(1, userSession.AccountPrivilege.Roles.Count);
            Assert.Equal(3, userSession.AccountPrivilege.AuthorizedRoles.Count);// 用户的全部角色来自直接角色、组织结构角色、工作组角色三者的并集所以是三个角色。
        }
        #endregion

        #region TestSessionPermissions
        [Fact]
        public void TestSessionPermissions()
        {
            var host = TestHelper.GetACDomain();
            var rbacService = host.GetRequiredService<IRBACService>();
            var accountRepository = host.GetRequiredService<IRepository<Account>>();
            var sessionRepository = host.GetRequiredService<IRepository<UserSession>>();
            var orgID = Guid.NewGuid();

            host.Handle(new AddOrganizationCommand(new OrganizationCreateInput
            {
                Id = orgID,
                Code = "100",
                Name = "测试1",
                Description = "test",
                SortCode = 10,
                Icon = null,
            }));
            Guid dicID = Guid.NewGuid();
            host.Handle(new AddDicCommand(new DicCreateInput
            {
                Id = dicID,
                Code = "auditStatus",
                Name = "auditStatus1"
            }));
            host.Handle(new AddDicItemCommand(new DicItemCreateInput
            {
                Id = dicID,
                IsEnabled = 1,
                DicID = dicID,
                SortCode = 0,
                Description = string.Empty,
                Code = "auditPass",
                Name = "auditPass"
            }));
            Guid accountID = Guid.NewGuid();
            host.Handle(new AddAccountCommand(new AccountCreateInput
            {
                Id = accountID,
                Code = "test",
                Name = "test",
                LoginName = "test",
                Password = "111111",
                OrganizationCode = "100",
                IsEnabled = 1,
                AuditState = "auditPass"
            }));
            Assert.NotNull(host.GetRequiredService<IRepository<Account>>().AsQueryable().FirstOrDefault(a => string.Equals(a.LoginName, "test", StringComparison.OrdinalIgnoreCase)));
            Guid roleID = Guid.NewGuid();
            host.Handle(new AddRoleCommand(new RoleCreateInput
            {
                Id = roleID,
                Name = "测试1",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            }));
            var functionID = Guid.NewGuid();
            host.Handle(new AddFunctionCommand(new FunctionCreateInput
            {
                Id = functionID,
                Code = "fun1",
                Description = string.Empty,
                DeveloperID = host.SysUsers.GetDevAccounts().First().Id,
                IsEnabled = 1,
                IsManaged = true,
                ResourceTypeID = host.ResourceTypeSet.First().Id,
                SortCode = 10
            }));
            Guid entityID = Guid.NewGuid();
            // 授予角色功能
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = roleID,
                SubjectType = ACSubjectType.Role.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = functionID,
                ObjectType = ACObjectType.Function.ToString()
            }));
            // 授予账户角色
            entityID = Guid.NewGuid();
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = accountID,
                SubjectType = ACSubjectType.Account.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = roleID,
                ObjectType = ACObjectType.Role.ToString()
            }));
            entityID = Guid.NewGuid();
            functionID = Guid.NewGuid();
            host.Handle(new AddFunctionCommand(new FunctionCreateInput
            {
                Id = functionID,
                Code = "fun2",
                Description = string.Empty,
                DeveloperID = host.SysUsers.GetDevAccounts().First().Id,
                IsEnabled = 1,
                IsManaged = true,
                ResourceTypeID = host.ResourceTypeSet.First().Id,
                SortCode = 10
            }));
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = accountID,
                SubjectType = ACSubjectType.Account.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = functionID,
                ObjectType = ACObjectType.Function.ToString()
            }));
            var account = accountRepository.GetByKey(accountID);
            var sessionID = Guid.NewGuid();
            var userSession = rbacService.CreateSession(sessionID, AccountState.Create(account));
            Assert.NotNull(userSession);
            var sessionEntity = sessionRepository.GetByKey(sessionID);
            Assert.NotNull(sessionEntity);
            Assert.Equal(1, userSession.AccountPrivilege.Functions.Count);
            Assert.Equal(2, userSession.AccountPrivilege.AuthorizedFunctions.Count);
            Assert.Equal(2, rbacService.UserPermissions(accountID).Count);
        }
        #endregion

        #region TestAddActiveRole
        [Fact]
        public void TestAddActiveRole()
        {
            // TODO:
        }
        #endregion

        #region TestDropActiveRole
        [Fact]
        public void TestDropActiveRole()
        {
            // TODO:
        }
        #endregion

        #region TestAssignedUsers
        [Fact]
        public void TestAssignedUsers()
        {
            // TODO:
        }
        #endregion

        #region TestAssignedRoles
        [Fact]
        public void TestAssignedRoles()
        {
            var host = TestHelper.GetACDomain();
            var rbacService = host.GetRequiredService<IRBACService>();
            var accountRepository = host.GetRequiredService<IRepository<Account>>();
            var sessionRepository = host.GetRequiredService<IRepository<UserSession>>();
            var orgID = Guid.NewGuid();

            host.Handle(new AddOrganizationCommand(new OrganizationCreateInput
            {
                Id = orgID,
                Code = "100",
                Name = "测试1",
                Description = "test",
                SortCode = 10,
                Icon = null,
            }));
            Guid dicID = Guid.NewGuid();
            host.Handle(new AddDicCommand(new DicCreateInput
            {
                Id = dicID,
                Code = "auditStatus",
                Name = "auditStatus1"
            }));
            host.Handle(new AddDicItemCommand(new DicItemCreateInput
            {
                Id = dicID,
                IsEnabled = 1,
                DicID = dicID,
                SortCode = 0,
                Description = string.Empty,
                Code = "auditPass",
                Name = "auditPass"
            }));
            Guid accountID = Guid.NewGuid();
            host.Handle(new AddAccountCommand(new AccountCreateInput
            {
                Id = accountID,
                Code = "test",
                Name = "test",
                LoginName = "test",
                Password = "111111",
                OrganizationCode = "100",
                IsEnabled = 1,
                AuditState = "auditPass"
            }));
            Guid roleID = Guid.NewGuid();
            host.Handle(new AddRoleCommand(new RoleCreateInput
            {
                Id = roleID,
                Name = "测试1",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            }));
            Guid entityID = Guid.NewGuid();
            // 授予账户角色
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = accountID,
                SubjectType = ACSubjectType.Account.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = roleID,
                ObjectType = ACObjectType.Role.ToString()
            }));
            Guid organizationID = Guid.NewGuid();
            host.Handle(new AddOrganizationCommand(new OrganizationCreateInput
            {
                Id = organizationID,
                Code = "110",
                Name = "测试110",
                Description = "test",
                SortCode = 10,
                Icon = null,
            }));
            entityID = Guid.NewGuid();
            // 授予账户组织结构
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = accountID,
                SubjectType = ACSubjectType.Account.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = organizationID,
                ObjectType = ACObjectType.Organization.ToString()
            }));
            // 授予组织结构角色
            entityID = Guid.NewGuid();
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = organizationID,
                SubjectType = ACSubjectType.Organization.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = roleID,
                ObjectType = ACObjectType.Role.ToString()
            }));
            Guid groupID = Guid.NewGuid();
            host.Handle(new AddGroupCommand(new GroupCreateInput
            {
                Id = groupID,
                Name = "测试1",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                ShortName = "",
                SortCode = 10,
                TypeCode = "AC"
            }));
            entityID = Guid.NewGuid();
            // 授予账户工作组
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = accountID,
                SubjectType = ACSubjectType.Account.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = groupID,
                ObjectType = ACObjectType.Group.ToString()
            }));
            // 授予工作组角色
            entityID = Guid.NewGuid();
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = roleID,
                SubjectType = ACSubjectType.Role.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = groupID,
                ObjectType = ACObjectType.Group.ToString()
            }));
            roleID = Guid.NewGuid();
            // 添加一个新角色并将该角色授予上面创建的组织结构
            host.Handle(new AddRoleCommand(new RoleCreateInput
            {
                Id = roleID,
                Name = "测试2",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            }));
            entityID = Guid.NewGuid();
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = organizationID,
                SubjectType = ACSubjectType.Organization.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = roleID,
                ObjectType = ACObjectType.Role.ToString()
            }));
            entityID = Guid.NewGuid();
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = roleID,
                SubjectType = ACSubjectType.Role.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = groupID,
                ObjectType = ACObjectType.Group.ToString()
            }));
            roleID = Guid.NewGuid();
            // 添加一个新角色并将该角色授予上面创建的工作组
            host.Handle(new AddRoleCommand(new RoleCreateInput
            {
                Id = roleID,
                Name = "测试3",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            }));
            entityID = Guid.NewGuid();
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = entityID,
                SubjectInstanceID = roleID,
                SubjectType = ACSubjectType.Role.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = groupID,
                ObjectType = ACObjectType.Group.ToString()
            }));

            var roles = rbacService.AssignedRoles(accountID);
            Assert.Equal(1, roles.Count);
            roles = rbacService.AuthorizedRoles(accountID);
            Assert.Equal(3, roles.Count);// 用户的全部角色来自直接角色、组织结构角色、工作组角色三者的并集所以是三个角色。
        }
        #endregion

        #region TestAuthorizedUsers
        [Fact]
        public void TestAuthorizedUsers()
        {
            // TODO:
        }
        #endregion

        #region TestRolePermissions
        [Fact]
        public void TestRolePermissions()
        {
            var host = TestHelper.GetACDomain();
            var rbacService = host.GetRequiredService<IRBACService>();
            var privilegeBigramRepository = host.GetRequiredService<IRepository<PrivilegeBigram>>();
            var roleID = Guid.NewGuid();
            rbacService.AddRole(new RoleCreateInput
            {
                Id = roleID,
                Name = "测试1",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            });
            var functionID = Guid.NewGuid();
            host.Handle(new AddFunctionCommand(new FunctionCreateInput
            {
                Id = functionID,
                Code = "fun1",
                Description = string.Empty,
                DeveloperID = host.SysUsers.GetDevAccounts().First().Id,
                IsEnabled = 1,
                IsManaged = true,
                ResourceTypeID = host.ResourceTypeSet.First().Id,
                SortCode = 10
            }));
            rbacService.GrantPermission(functionID, roleID);
            var entity = privilegeBigramRepository.AsQueryable().FirstOrDefault(a => a.SubjectInstanceID == roleID && a.ObjectInstanceID == functionID);
            Assert.NotNull(entity);
            Assert.NotNull(host.PrivilegeSet.FirstOrDefault(a => a.SubjectInstanceID == roleID && a.ObjectInstanceID == functionID));
            Assert.Equal(1, rbacService.RolePermissions(roleID).Count);
        }
        #endregion

        #region TestAddInheritance
        [Fact]
        public void TestAddInheritance()
        {
            var host = TestHelper.GetACDomain();
            var rbacService = host.GetRequiredService<IRBACService>();
            Assert.Equal(0, host.RoleSet.Count());

            var roleID1 = Guid.NewGuid();
            host.Handle(new AddRoleCommand(new RoleCreateInput
            {
                Id = roleID1,
                Name = "role1",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            }));

            var roleID2 = Guid.NewGuid();
            host.Handle(new AddRoleCommand(new RoleCreateInput
            {
                Id = roleID2,
                Name = "role2",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            }));
            rbacService.AddInheritance(roleID1, roleID2);
            Assert.Equal(2, host.RoleSet.Count());
            RoleState role1;
            RoleState role2;
            Assert.True(host.RoleSet.TryGetRole(roleID1, out role1));
            Assert.True(host.RoleSet.TryGetRole(roleID2, out role2));
            Assert.Equal(1, host.RoleSet.GetDescendantRoles(role1).Count);
            Assert.Equal(0, host.RoleSet.GetDescendantRoles(role2).Count);
            rbacService.DeleteInheritance(roleID1, roleID2);
            Assert.Equal(0, host.RoleSet.GetDescendantRoles(role1).Count);
        }
        #endregion

        #region TestAddAscendant
        [Fact]
        public void TestAddAscendant()
        {
            var host = TestHelper.GetACDomain();
            var rbacService = host.GetRequiredService<IRBACService>();
            Assert.Equal(0, host.RoleSet.Count());

            var roleID1 = Guid.NewGuid();
            host.Handle(new AddRoleCommand(new RoleCreateInput
            {
                Id = roleID1,
                Name = "role1",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            }));

            var roleID2 = Guid.NewGuid();
            rbacService.AddAscendant(roleID1, new RoleCreateInput
            {
                Id = roleID2,
                Name = "role2",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            });
            Assert.Equal(2, host.RoleSet.Count());
            RoleState role1;
            RoleState role2;
            Assert.True(host.RoleSet.TryGetRole(roleID1, out role1));
            Assert.True(host.RoleSet.TryGetRole(roleID2, out role2));
            Assert.Equal(1, host.RoleSet.GetDescendantRoles(role1).Count);
            Assert.Equal(0, host.RoleSet.GetDescendantRoles(role2).Count);
            rbacService.DeleteInheritance(roleID1, roleID2);
            Assert.Equal(0, host.RoleSet.GetDescendantRoles(role1).Count);
        }
        #endregion

        #region TestAddDescendant
        [Fact]
        public void TestAddDescendant()
        {
            var host = TestHelper.GetACDomain();
            var rbacService = host.GetRequiredService<IRBACService>();
            Assert.Equal(0, host.RoleSet.Count());

            var roleID1 = Guid.NewGuid();
            var roleID2 = Guid.NewGuid();
            host.Handle(new AddRoleCommand(new RoleCreateInput
            {
                Id = roleID2,
                Name = "role2",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            }));
            rbacService.AddDescendant(roleID2, new RoleCreateInput
            {
                Id = roleID1,
                Name = "role1",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            });
            Assert.Equal(2, host.RoleSet.Count());
            RoleState role1;
            RoleState role2;
            Assert.True(host.RoleSet.TryGetRole(roleID1, out role1));
            Assert.True(host.RoleSet.TryGetRole(roleID2, out role2));
            Assert.Equal(1, host.RoleSet.GetDescendantRoles(role1).Count);
            Assert.Equal(0, host.RoleSet.GetDescendantRoles(role2).Count);
            rbacService.DeleteInheritance(roleID1, roleID2);
            Assert.Equal(0, host.RoleSet.GetDescendantRoles(role1).Count);
        }
        #endregion

        #region TestCreateSSDSet
        [Fact]
        public void TestCreateSSDSet()
        {
            var host = TestHelper.GetACDomain();
            var rbacService = host.GetRequiredService<IRBACService>();
            Assert.Equal(0, host.SSDSetSet.Count());

            var entityID = Guid.NewGuid();

            SSDSetState ssdSetByID;
            rbacService.CreateSSDSet(new SSDSetCreateIO
            {
                Id = entityID,
                Name = "测试1",
                Description = "test",
                IsEnabled = 1,
                SSDCard = 2
            });
            Assert.Equal(1, host.SSDSetSet.Count());
            Assert.True(host.SSDSetSet.TryGetSSDSet(entityID, out ssdSetByID));
        }
        #endregion

        #region TestDeleteSSDSet
        [Fact]
        public void TestDeleteSSDSet()
        {
            var host = TestHelper.GetACDomain();
            var rbacService = host.GetRequiredService<IRBACService>();
            Assert.Equal(0, host.SSDSetSet.Count());

            var entityID = Guid.NewGuid();

            SSDSetState ssdSetByID;
            rbacService.CreateSSDSet(new SSDSetCreateIO
            {
                Id = entityID,
                Name = "测试1",
                Description = "test",
                IsEnabled = 1,
                SSDCard = 2
            });
            Assert.Equal(1, host.SSDSetSet.Count());
            Assert.True(host.SSDSetSet.TryGetSSDSet(entityID, out ssdSetByID));
            rbacService.DeleteSSDSet(entityID);
            Assert.Equal(0, host.SSDSetSet.Count());
            Assert.False(host.SSDSetSet.TryGetSSDSet(entityID, out ssdSetByID));
        }
        #endregion

        #region TestAddSSDRoleMember
        [Fact]
        public void TestAddSSDRoleMember()
        {
            var host = TestHelper.GetACDomain();
            var rbacService = host.GetRequiredService<IRBACService>();
            Assert.Equal(0, host.SSDSetSet.Count());

            var ssdSetID = Guid.NewGuid();

            SSDSetState ssdSetByID;
            host.Handle(new AddSSDSetCommand(new SSDSetCreateIO
            {
                Id = ssdSetID,
                Name = "测试1",
                Description = "test",
                IsEnabled = 1,
                SSDCard = 2
            }));
            Assert.Equal(1, host.SSDSetSet.Count());
            Assert.True(host.SSDSetSet.TryGetSSDSet(ssdSetID, out ssdSetByID));

            Assert.Equal(0, host.SSDSetSet.GetSSDRoles(ssdSetByID).Count);
            RoleState roleByID;
            var roleID = Guid.NewGuid();
            host.Handle(new AddRoleCommand(new RoleCreateInput
            {
                Id = roleID,
                Name = "测试1",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            }));
            Assert.Equal(1, host.RoleSet.Count());
            Assert.True(host.RoleSet.TryGetRole(roleID, out roleByID));
            rbacService.AddSSDRoleMember(ssdSetID, roleID);
            Assert.Equal(1, host.SSDSetSet.GetSSDRoles(ssdSetByID).Count);
        }
        #endregion

        #region TestDeleteSSDRoleMember
        [Fact]
        public void TestDeleteSSDRoleMember()
        {
            var host = TestHelper.GetACDomain();
            var rbacService = host.GetRequiredService<IRBACService>();
            Assert.Equal(0, host.SSDSetSet.Count());

            var ssdSetID = Guid.NewGuid();

            SSDSetState ssdSetByID;
            host.Handle(new AddSSDSetCommand(new SSDSetCreateIO
            {
                Id = ssdSetID,
                Name = "测试1",
                Description = "test",
                IsEnabled = 1,
                SSDCard = 2
            }));
            Assert.Equal(1, host.SSDSetSet.Count());
            Assert.True(host.SSDSetSet.TryGetSSDSet(ssdSetID, out ssdSetByID));

            Assert.Equal(0, host.SSDSetSet.GetSSDRoles(ssdSetByID).Count);
            RoleState roleByID;
            var roleID = Guid.NewGuid();
            host.Handle(new AddRoleCommand(new RoleCreateInput
            {
                Id = roleID,
                Name = "测试1",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            }));
            Assert.Equal(1, host.RoleSet.Count());
            Assert.True(host.RoleSet.TryGetRole(roleID, out roleByID));
            rbacService.AddSSDRoleMember(ssdSetID, roleID);
            Assert.Equal(1, host.SSDSetSet.GetSSDRoles(ssdSetByID).Count);
            rbacService.DeleteSSDRoleMember(ssdSetID, roleID);
            Assert.Equal(0, host.SSDSetSet.GetSSDRoles(ssdSetByID).Count);
        }
        #endregion

        #region TestSetSSDCardinality
        [Fact]
        public void TestSetSSDCardinality()
        {
            var host = TestHelper.GetACDomain();
            var rbacService = host.GetRequiredService<IRBACService>();
            Assert.Equal(0, host.SSDSetSet.Count());

            var ssdSetID = Guid.NewGuid();

            SSDSetState ssdSetByID;
            host.Handle(new AddSSDSetCommand(new SSDSetCreateIO
            {
                Id = ssdSetID,
                Name = "测试1",
                Description = "test",
                IsEnabled = 1,
                SSDCard = 2
            }));
            Assert.Equal(1, host.SSDSetSet.Count());
            Assert.True(host.SSDSetSet.TryGetSSDSet(ssdSetID, out ssdSetByID));
            Assert.Equal(2, ssdSetByID.SSDCard);
            rbacService.SetSSDCardinality(ssdSetID, 3);
            Assert.True(host.SSDSetSet.TryGetSSDSet(ssdSetID, out ssdSetByID));
            Assert.Equal(3, ssdSetByID.SSDCard);
        }
        #endregion

        #region TestCreateDSDSet
        [Fact]
        public void TestCreateDSDSet()
        {
            var host = TestHelper.GetACDomain();
            var rbacService = host.GetRequiredService<IRBACService>();
            Assert.Equal(0, host.DSDSetSet.Count());

            var entityID = Guid.NewGuid();

            DSDSetState dsdSetByID;
            rbacService.CreateDSDSet(new DSDSetCreateIO
            {
                Id = entityID,
                Name = "测试1",
                Description = "test",
                IsEnabled = 1,
                DSDCard = 2
            });
            Assert.Equal(1, host.DSDSetSet.Count());
            Assert.True(host.DSDSetSet.TryGetDSDSet(entityID, out dsdSetByID));
        }
        #endregion

        #region TestDeleteDSDSet
        [Fact]
        public void TestDeleteDSDSet()
        {
            var host = TestHelper.GetACDomain();
            var rbacService = host.GetRequiredService<IRBACService>();
            Assert.Equal(0, host.DSDSetSet.Count());

            var entityID = Guid.NewGuid();

            DSDSetState dsdSetByID;
            rbacService.CreateDSDSet(new DSDSetCreateIO
            {
                Id = entityID,
                Name = "测试1",
                Description = "test",
                IsEnabled = 1,
                DSDCard = 2
            });
            Assert.Equal(1, host.DSDSetSet.Count());
            Assert.True(host.DSDSetSet.TryGetDSDSet(entityID, out dsdSetByID));
            rbacService.DeleteDSDSet(entityID);
            Assert.Equal(0, host.DSDSetSet.Count());
            Assert.False(host.DSDSetSet.TryGetDSDSet(entityID, out dsdSetByID));
        }
        #endregion

        #region TestAddDSDRoleMember
        [Fact]
        public void TestAddDSDRoleMember()
        {
            var host = TestHelper.GetACDomain();
            var rbacService = host.GetRequiredService<IRBACService>();
            Assert.Equal(0, host.DSDSetSet.Count());

            var dsdSetID = Guid.NewGuid();

            DSDSetState dsdSetByID;
            host.Handle(new AddDSDSetCommand(new DSDSetCreateIO
            {
                Id = dsdSetID,
                Name = "测试1",
                Description = "test",
                IsEnabled = 1,
                DSDCard = 2
            }));
            Assert.Equal(1, host.DSDSetSet.Count());
            Assert.True(host.DSDSetSet.TryGetDSDSet(dsdSetID, out dsdSetByID));

            Assert.Equal(0, host.DSDSetSet.GetDSDRoles(dsdSetByID).Count);
            RoleState roleByID;
            var roleID = Guid.NewGuid();
            host.Handle(new AddRoleCommand(new RoleCreateInput
            {
                Id = roleID,
                Name = "测试1",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            }));
            Assert.Equal(1, host.RoleSet.Count());
            Assert.True(host.RoleSet.TryGetRole(roleID, out roleByID));
            rbacService.AddDSDRoleMember(dsdSetID, roleID);
            Assert.Equal(1, host.DSDSetSet.GetDSDRoles(dsdSetByID).Count);
        }
        #endregion

        #region TestDeleteDSDRoleMember
        [Fact]
        public void TestDeleteDSDRoleMember()
        {
            var host = TestHelper.GetACDomain();
            var rbacService = host.GetRequiredService<IRBACService>();
            Assert.Equal(0, host.DSDSetSet.Count());

            var dsdSetID = Guid.NewGuid();

            DSDSetState dsdSetByID;
            host.Handle(new AddDSDSetCommand(new DSDSetCreateIO
            {
                Id = dsdSetID,
                Name = "测试1",
                Description = "test",
                IsEnabled = 1,
                DSDCard = 2
            }));
            Assert.Equal(1, host.DSDSetSet.Count());
            Assert.True(host.DSDSetSet.TryGetDSDSet(dsdSetID, out dsdSetByID));

            Assert.Equal(0, host.DSDSetSet.GetDSDRoles(dsdSetByID).Count);
            RoleState roleByID;
            var roleID = Guid.NewGuid();
            host.Handle(new AddRoleCommand(new RoleCreateInput
            {
                Id = roleID,
                Name = "测试1",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            }));
            Assert.Equal(1, host.RoleSet.Count());
            Assert.True(host.RoleSet.TryGetRole(roleID, out roleByID));
            rbacService.AddDSDRoleMember(dsdSetID, roleID);
            Assert.Equal(1, host.DSDSetSet.GetDSDRoles(dsdSetByID).Count);
            rbacService.DeleteDSDRoleMember(dsdSetID, roleID);
            Assert.Equal(0, host.DSDSetSet.GetDSDRoles(dsdSetByID).Count);
        }
        #endregion

        #region TestSetDSDCardinality
        [Fact]
        public void TestSetDSDCardinality()
        {
            var host = TestHelper.GetACDomain();
            var rbacService = host.GetRequiredService<IRBACService>();
            Assert.Equal(0, host.DSDSetSet.Count());

            var ssdSetID = Guid.NewGuid();

            DSDSetState ssdSetByID;
            host.Handle(new AddDSDSetCommand(new DSDSetCreateIO
            {
                Id = ssdSetID,
                Name = "测试1",
                Description = "test",
                IsEnabled = 1,
                DSDCard = 2
            }));
            Assert.Equal(1, host.DSDSetSet.Count());
            Assert.True(host.DSDSetSet.TryGetDSDSet(ssdSetID, out ssdSetByID));
            Assert.Equal(2, ssdSetByID.DSDCard);
            rbacService.SetDSDCardinality(ssdSetID, 3);
            Assert.True(host.DSDSetSet.TryGetDSDSet(ssdSetID, out ssdSetByID));
            Assert.Equal(3, ssdSetByID.DSDCard);
        }
        #endregion
    }
}

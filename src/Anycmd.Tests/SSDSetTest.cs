﻿
namespace Anycmd.Tests
{
    using AC;
    using AC.Identity.ViewModels.AccountViewModels;
    using AC.Infra.ViewModels.DicViewModels;
    using AC.Infra.ViewModels.OrganizationViewModels;
    using AC.ViewModels.RoleViewModels;
    using Host;
    using Host.AC.Identity;
    using Host.AC.Identity.Messages;
    using Host.AC.Infra.Messages;
    using Host.AC.Messages;
    using Host.AC.InOuts;
    using Repositories;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using Xunit;

    public class SSDSetTest
    {
        [Fact]
        public void SSDSet()
        {
            var host = TestHelper.GetACDomain();
            Assert.Equal(0, host.SSDSetSet.Count());

            var entityID = Guid.NewGuid();

            SSDSetState ssdSEtByID;
            host.Handle(new AddSSDSetCommand(new SSDSetCreateIO
            {
                Id = entityID,
                Name = "测试1",
                Description = "test",
                IsEnabled = 1,
                SSDCard = 2
            }));
            Assert.Equal(1, host.SSDSetSet.Count());
            Assert.True(host.SSDSetSet.TryGetSSDSet(entityID, out ssdSEtByID));

            host.Handle(new UpdateSSDSetCommand(new SSDSetUpdateIO
            {
                Id = entityID,
                Name = "test2",
                Description = "test",
                IsEnabled = 1,
                SSDCard = 2
            }));
            Assert.Equal(1, host.SSDSetSet.Count());
            Assert.True(host.SSDSetSet.TryGetSSDSet(entityID, out ssdSEtByID));
            Assert.Equal("test2", ssdSEtByID.Name);

            host.Handle(new RemoveSSDSetCommand(entityID));
            Assert.False(host.SSDSetSet.TryGetSSDSet(entityID, out ssdSEtByID));
            Assert.Equal(0, host.SSDSetSet.Count());
        }

        [Fact]
        public void TestSSDRole()
        {
            var host = TestHelper.GetACDomain();
            Assert.Equal(0, host.SSDSetSet.Count());

            var ssdSetID = Guid.NewGuid();

            SSDSetState ssdSetByID;
            host.Handle(new AddSSDSetCommand(new SSDSetCreateIO
            {
                Id = ssdSetID,
                Name = "测试1",
                Description = "test",
                IsEnabled = 1,
                SSDCard = 2
            }));
            Assert.Equal(1, host.SSDSetSet.Count());
            Assert.True(host.SSDSetSet.TryGetSSDSet(ssdSetID, out ssdSetByID));

            Assert.Equal(0, host.SSDSetSet.GetSSDRoles(ssdSetByID).Count);
            RoleState roleByID;
            var roleID = Guid.NewGuid();
            host.Handle(new AddRoleCommand(new RoleCreateInput
            {
                Id = roleID,
                Name = "测试1",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            }));
            Assert.Equal(1, host.RoleSet.Count());
            Assert.True(host.RoleSet.TryGetRole(roleID, out roleByID));
            var entityID = Guid.NewGuid();
            host.Handle(new AddSSDRoleCommand(new SSDRoleCreateIO
            {
                Id = entityID,
                RoleID = roleID,
                SSDSetID = ssdSetID
            }));
            Assert.Equal(1, host.SSDSetSet.GetSSDRoles(ssdSetByID).Count);
            host.Handle(new RemoveSSDRoleCommand(entityID));
            Assert.Equal(0, host.SSDSetSet.GetSSDRoles(ssdSetByID).Count);
        }

        [Fact]
        public void CheckSSDSetRoles()
        {
            var host = TestHelper.GetACDomain();
            Assert.Equal(0, host.SSDSetSet.Count());

            var ssdSetID = Guid.NewGuid();

            SSDSetState ssdSetByID;
            host.Handle(new AddSSDSetCommand(new SSDSetCreateIO
            {
                Id = ssdSetID,
                Name = "测试1",
                Description = "test",
                IsEnabled = 1,
                SSDCard = 2
            }));
            Assert.Equal(1, host.SSDSetSet.Count());
            Assert.True(host.SSDSetSet.TryGetSSDSet(ssdSetID, out ssdSetByID));

            Assert.Equal(0, host.SSDSetSet.GetSSDRoles(ssdSetByID).Count);
            var entityID = Guid.NewGuid();
            var roleID1 = Guid.NewGuid();
            host.Handle(new AddRoleCommand(new RoleCreateInput
            {
                Id = roleID1,
                Name = "测试1",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            }));
            entityID = Guid.NewGuid();
            host.Handle(new AddSSDRoleCommand(new SSDRoleCreateIO
            {
                Id = entityID,
                RoleID = roleID1,
                SSDSetID = ssdSetID
            }));
            var roleID2 = Guid.NewGuid();
            host.Handle(new AddRoleCommand(new RoleCreateInput
            {
                Id = roleID2,
                Name = "测试2",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            }));
            entityID = Guid.NewGuid();
            host.Handle(new AddSSDRoleCommand(new SSDRoleCreateIO
            {
                Id = entityID,
                RoleID = roleID2,
                SSDSetID = ssdSetID
            }));
            var roleID3 = Guid.NewGuid();
            host.Handle(new AddRoleCommand(new RoleCreateInput
            {
                Id = roleID3,
                Name = "测试3",
                CategoryCode = "test",
                Description = "test",
                IsEnabled = 1,
                SortCode = 10,
                Icon = null
            }));
            entityID = Guid.NewGuid();
            host.Handle(new AddSSDRoleCommand(new SSDRoleCreateIO
            {
                Id = entityID,
                RoleID = roleID3,
                SSDSetID = ssdSetID
            }));
            Assert.Equal(3, host.RoleSet.Count());
            Assert.Equal(3, host.SSDSetSet.GetSSDRoles(ssdSetByID).Count);
            var orgID = Guid.NewGuid();

            host.Handle(new AddOrganizationCommand(new OrganizationCreateInput
            {
                Id = orgID,
                Code = "100",
                Name = "测试1",
                Description = "test",
                SortCode = 10,
                Icon = null,
            }));
            Guid dicID = Guid.NewGuid();
            host.Handle(new AddDicCommand(new DicCreateInput
            {
                Id = dicID,
                Code = "auditStatus",
                Name = "auditStatus1"
            }));
            host.Handle(new AddDicItemCommand(new DicItemCreateInput
            {
                Id = dicID,
                IsEnabled = 1,
                DicID = dicID,
                SortCode = 0,
                Description = string.Empty,
                Code = "auditPass",
                Name = "auditPass"
            }));
            Guid accountID = Guid.NewGuid();
            host.Handle(new AddAccountCommand(new AccountCreateInput
            {
                Id = accountID,
                Code = "test",
                Name = "test",
                LoginName = "test",
                Password = "111111",
                OrganizationCode = "100",
                IsEnabled = 1,
                AuditState = "auditPass"
            }));
            Assert.NotNull(host.GetRequiredService<IRepository<Account>>().AsQueryable().FirstOrDefault(a => string.Equals(a.LoginName, "test", StringComparison.OrdinalIgnoreCase)));
            host.SignIn(new Dictionary<string, object>
            {
                {"loginName", "test"},
                {"password", "111111"},
                {"rememberMe", "rememberMe"}
            });
            Assert.True(host.UserSession.Principal.Identity.IsAuthenticated);
            Assert.Equal(0, host.UserSession.AccountPrivilege.Roles.Count);
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = Guid.NewGuid(),
                SubjectInstanceID = accountID,
                SubjectType = ACSubjectType.Account.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = roleID1,
                ObjectType = ACObjectType.Role.ToString()
            }));
            host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
            {
                Id = Guid.NewGuid(),
                SubjectInstanceID = accountID,
                SubjectType = ACSubjectType.Account.ToString(),// 主体是账户
                PrivilegeConstraint = null,
                PrivilegeOrientation = 1,
                ObjectInstanceID = roleID2,
                ObjectType = ACObjectType.Role.ToString()
            }));
            bool catched = false;
            try
            {
                host.Handle(new AddPrivilegeBigramCommand(new PrivilegeBigramCreateIO
                {
                    Id = Guid.NewGuid(),
                    SubjectInstanceID = accountID,
                    SubjectType = ACSubjectType.Account.ToString(),// 主体是账户
                    PrivilegeConstraint = null,
                    PrivilegeOrientation = 1,
                    ObjectInstanceID = roleID3,
                    ObjectType = ACObjectType.Role.ToString()
                }));
            }
            catch
            {
                catched = true;
            }
            Assert.True(catched);
        }
    }
}

﻿
namespace Anycmd.Tests
{
    using Host.AC;
    using Host.AC.Identity;
    using Host.Impl;
    using Repositories;
    using System;
    using System.Linq;

    public class MoqACDomain : DefaultACDomain
    {
        public override void Configure()
        {
            base.Configure();
        }

        protected override void OnSignOuted(Guid sessionID)
        {
            var repository = GetRequiredService<IRepository<UserSession>>();
            var entity = repository.GetByKey(sessionID);
            if (entity != null)
            {
                entity.IsAuthenticated = false;
                repository.Update(entity);
            }
        }

        protected override Account GetAccountByID(Guid accountID)
        {
            var repository = GetRequiredService<IRepository<Account>>();
            return repository.GetByKey(accountID);
        }

        protected override Account GetAccountByLoginName(string loginName)
        {
            var repository = GetRequiredService<IRepository<Account>>();
            return repository.AsQueryable().FirstOrDefault(a => string.Equals(a.LoginName, loginName, StringComparison.OrdinalIgnoreCase));
        }
    }
}

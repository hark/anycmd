﻿
namespace Anycmd.Tests
{
    using Host;
    using Host.AC;
    using Host.AC.Identity;
    using Host.AC.Infra;
    using Host.Impl;
    using Logging;
    using Model;
    using Moq;
    using Rdb;
    using Repositories;
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Reflection;

    public static class TestHelper
    {
        public static IACDomain GetACDomain()
        {
            var host = new MoqACDomain();
            host.RegisterRepository(typeof(ACDomain).Assembly);
            host.AddService(typeof(ILoggingService), new log4netLoggingService(host));
            host.AddService(typeof(IUserSessionStorage), new SimpleUserSessionStorage());
            Guid accountID = Guid.NewGuid();
            host.GetRequiredService<IRepository<Account>>().Add(new Account
            {
                Id = accountID,
                LoginName = "LoginName1",
                Password = "111111",
                AuditState = string.Empty,
                BackColor = string.Empty,
                AllowEndTime = null,
                AllowStartTime = null,
                AnswerQuestion = string.Empty,
                Description = string.Empty,
                FirstLoginOn = null,
                DeletionStateCode = 0,
                IPAddress = string.Empty,
                Lang = string.Empty,
                IsEnabled = 1,
                LastPasswordChangeOn = null,
                LockEndTime = null,
                LockStartTime = null,
                LoginCount = null,
                MacAddress = string.Empty,
                OpenID = string.Empty,
                PreviousLoginOn = null,
                NumberID = 10,
                Question = string.Empty,
                Theme = string.Empty,
                Wallpaper = string.Empty,
                SecurityLevel = 0,
                Code = "user1",
                CommunicationPassword = string.Empty,
                Email = string.Empty,
                Mobile = string.Empty,
                PublicKey = string.Empty,
                QQ = string.Empty,
                Name = "user1",
                QuickQuery = string.Empty,
                QuickQuery1 = string.Empty,
                QuickQuery2 = string.Empty,
                SignedPassword = string.Empty,
                Telephone = string.Empty,
                OrganizationCode = string.Empty
            });
            host.GetRequiredService<IRepository<Account>>().Context.Commit();
            Guid appSystemID = Guid.NewGuid();
            host.GetRequiredService<IRepository<AppSystem>>().Add(new AppSystem
            {
                Id = appSystemID,
                Name = "test",
                Code = "test",
                PrincipalID = host.GetRequiredService<IRepository<Account>>().AsQueryable().First().Id
            });
            host.GetRequiredService<IRepository<AppSystem>>().Context.Commit();
            host.GetRequiredService<IRepository<ResourceType>>().Add(new ResourceType
                {
                    AllowDelete = 0,
                    AllowEdit = 0,
                    Code = "Resource1",
                    Id = Guid.NewGuid(),
                    Icon = string.Empty,
                    Description = string.Empty,
                    Name = "测试1",
                    SortCode = 10,
                    AppSystemID = appSystemID
                });
            host.GetRequiredService<IRepository<ResourceType>>().Context.Commit();
            host.RemoveService(typeof(IOriginalHostStateReader));
            var moACDomainBootstrap = new Mock<IOriginalHostStateReader>();
            moACDomainBootstrap.Setup<IList<RDatabase>>(a => a.GetAllRDatabases()).Returns(new List<RDatabase>
            {
                new RDatabase
                {
                    Id=Guid.NewGuid(),
                    CatalogName="test",
                    DataSource=".",
                    Description="test",
                    IsTemplate=false,
                    Password=string.Empty,
                    Profile=string.Empty,
                    UserID=string.Empty,
                    RdbmsType="SqlServer",
                    ProviderName=string.Empty
                }
            });
            moACDomainBootstrap.Setup<IList<DbTableColumn>>(a => a.GetTableColumns(It.IsAny<RdbDescriptor>())).Returns(new List<DbTableColumn>());
            moACDomainBootstrap.Setup<IList<DbTable>>(a => a.GetDbTables(It.IsAny<RdbDescriptor>())).Returns(new List<DbTable>());
            moACDomainBootstrap.Setup<IList<DbViewColumn>>(a => a.GetViewColumns(It.IsAny<RdbDescriptor>())).Returns(new List<DbViewColumn>());
            moACDomainBootstrap.Setup<IList<DbView>>(a => a.GetDbViews(It.IsAny<RdbDescriptor>())).Returns(new List<DbView>());
            moACDomainBootstrap.Setup<IList<Organization>>(a => a.GetOrganizations()).Returns(host.GetRequiredService<IRepository<Organization>>().AsQueryable().ToList());
            moACDomainBootstrap.Setup<IList<AppSystem>>(a => a.GetAllAppSystems()).Returns(host.GetRequiredService<IRepository<AppSystem>>().AsQueryable().ToList());
            moACDomainBootstrap.Setup<IList<Button>>(a => a.GetAllButtons()).Returns(host.GetRequiredService<IRepository<Button>>().AsQueryable().ToList());
            moACDomainBootstrap.Setup<IList<Dic>>(a => a.GetAllDics()).Returns(host.GetRequiredService<IRepository<Dic>>().AsQueryable().ToList());
            moACDomainBootstrap.Setup<IList<DicItem>>(a => a.GetAllDicItems()).Returns(host.GetRequiredService<IRepository<DicItem>>().AsQueryable().ToList());
            moACDomainBootstrap.Setup<IList<EntityType>>(a => a.GetAllEntityTypes()).Returns(host.GetRequiredService<IRepository<EntityType>>().AsQueryable().ToList());
            moACDomainBootstrap.Setup<IList<Property>>(a => a.GetAllProperties()).Returns(host.GetRequiredService<IRepository<Property>>().AsQueryable().ToList());
            moACDomainBootstrap.Setup<IList<Function>>(a => a.GetAllFunctions()).Returns(host.GetRequiredService<IRepository<Function>>().AsQueryable().ToList());
            moACDomainBootstrap.Setup<IList<Group>>(a => a.GetAllGroups()).Returns(host.GetRequiredService<IRepository<Group>>().AsQueryable().ToList());
            moACDomainBootstrap.Setup<IList<Menu>>(a => a.GetAllMenus()).Returns(host.GetRequiredService<IRepository<Menu>>().AsQueryable().ToList());
            moACDomainBootstrap.Setup<IList<UIView>>(a => a.GetAllUIViews()).Returns(host.GetRequiredService<IRepository<UIView>>().AsQueryable().ToList());
            moACDomainBootstrap.Setup<IList<UIViewButton>>(a => a.GetAllUIViewButtons()).Returns(host.GetRequiredService<IRepository<UIViewButton>>().AsQueryable().ToList());
            moACDomainBootstrap.Setup<IList<PrivilegeBigram>>(a => a.GetPrivilegeBigrams()).Returns(host.GetRequiredService<IRepository<PrivilegeBigram>>().AsQueryable().ToList());
            moACDomainBootstrap.Setup<IList<ResourceType>>(a => a.GetAllResources()).Returns(host.GetRequiredService<IRepository<ResourceType>>().AsQueryable().ToList());
            moACDomainBootstrap.Setup<IList<Role>>(a => a.GetAllRoles()).Returns(host.GetRequiredService<IRepository<Role>>().AsQueryable().ToList());
            moACDomainBootstrap.Setup<IList<SSDSet>>(a => a.GetAllSSDSets()).Returns(host.GetRequiredService<IRepository<SSDSet>>().AsQueryable().ToList());
            moACDomainBootstrap.Setup<IList<DSDSet>>(a => a.GetAllDSDSets()).Returns(host.GetRequiredService<IRepository<DSDSet>>().AsQueryable().ToList());
            moACDomainBootstrap.Setup<IList<SSDRole>>(a => a.GetAllSSDRoles()).Returns(host.GetRequiredService<IRepository<SSDRole>>().AsQueryable().ToList());
            moACDomainBootstrap.Setup<IList<DSDRole>>(a => a.GetAllDSDRoles()).Returns(host.GetRequiredService<IRepository<DSDRole>>().AsQueryable().ToList());
            moACDomainBootstrap.Setup<IList<Account>>(a => a.GetAllDevAccounts()).Returns(host.GetRequiredService<IRepository<Account>>().AsQueryable().ToList());
            host.AddService(typeof(IOriginalHostStateReader), moACDomainBootstrap.Object);
            host.Init();

            return host;
        }

        public static Mock<TRepository> GetMoqRepository<TEntity, TRepository>(this IACDomain host)
            where TEntity : class, IAggregateRoot
            where TRepository : class, IRepository<TEntity>
        {

            var moRepository = new Mock<TRepository>();
            var context = new MoqRepositoryContext(host);
            moRepository.Setup<IRepositoryContext>(a => a.Context).Returns(context);
            moRepository.Setup(a => a.Add(It.IsAny<TEntity>()));
            moRepository.Setup(a => a.Remove(It.IsAny<TEntity>()));
            moRepository.Setup(a => a.Update(It.IsAny<TEntity>()));
            moRepository.Setup<TEntity>(a => a.GetByKey(It.IsAny<Guid>())).Returns((TEntity)null);
            moRepository.Setup<IQueryable<TEntity>>(a => a.AsQueryable()).Returns(new List<TEntity>().AsQueryable());

            return moRepository;
        }

        public static void RegisterRepository(this IACDomain host, params Assembly[] assemblies)
        {
            foreach (var assembly in assemblies)
            {
                foreach (var type in assembly.GetTypes())
                {
                    if (type.IsClass && !type.IsAbstract && typeof(IAggregateRoot).IsAssignableFrom(type))
                    {
                        var repositoryType = typeof(MoqCommonRepository<>);
                        var genericInterface = typeof(IRepository<>);
                        repositoryType = repositoryType.MakeGenericType(type);
                        genericInterface = genericInterface.MakeGenericType(type);
                        var repository = Activator.CreateInstance(repositoryType, host);
                        host.AddService(genericInterface, repository);
                    }
                }
            }
        }
    }
}

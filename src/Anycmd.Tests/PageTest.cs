﻿
namespace Anycmd.Tests
{
    using AC.Infra.ViewModels.AppSystemViewModels;
    using AC.Infra.ViewModels.UIViewViewModels;
    using Host;
    using Host.AC.Infra;
    using Host.AC.Infra.Messages;
    using Moq;
    using Repositories;
    using System;
    using System.Linq;
    using Xunit;

    public class PageTest
    {
        #region PageSet
        [Fact]
        public void PageSet()
        {
            var host = TestHelper.GetACDomain();
            Assert.Equal(0, host.UIViewSet.Count());

            var entityID = Guid.NewGuid();

            host.Handle(new AddFunctionCommand(new FunctionCreateInput
            {
                Id = entityID,
                Code = "fun1",
                Description = string.Empty,
                DeveloperID = host.SysUsers.GetDevAccounts().First().Id,
                IsEnabled = 1,
                IsManaged = true,
                ResourceTypeID = host.ResourceTypeSet.First().Id,
                SortCode = 10
            }));
            FunctionState functionByID;
            Assert.Equal(1, host.FunctionSet.Count());
            Assert.True(host.FunctionSet.TryGetFunction(entityID, out functionByID));
            UIViewState pageByID;
            host.Handle(new AddUIViewCommand(new UIViewCreateInput
            {
                Id = entityID,
                Icon = null,
                Tooltip = null
            }));
            Assert.Equal(1, host.UIViewSet.Count());
            Assert.True(host.UIViewSet.TryGetUIView(entityID, out pageByID));
            bool catched = false;
            try
            {
                host.Handle(new AddUIViewCommand(new UIViewCreateInput
                {
                    Id = Guid.NewGuid(),
                    Icon = null,
                    Tooltip = null
                }));
            }
            catch (Exception)
            {
                catched = true;
            }
            finally
            {
                Assert.True(catched);
            }
            host.Handle(new UpdateUIViewCommand(new UIViewUpdateInput
            {
                Id = entityID,
                Icon = null,
                Tooltip = null
            }));
            Assert.Equal(1, host.UIViewSet.Count());
            Assert.True(host.UIViewSet.TryGetUIView(entityID, out pageByID));

            host.Handle(new RemoveUIViewCommand(entityID));
            Assert.False(host.UIViewSet.TryGetUIView(entityID, out pageByID));
            Assert.Equal(0, host.UIViewSet.Count());
        }
        #endregion

        #region PageSetShouldRollbackedWhenPersistFailed
        [Fact]
        public void PageSetShouldRollbackedWhenPersistFailed()
        {
            var host = TestHelper.GetACDomain();
            Assert.Equal(0, host.UIViewSet.Count());

            host.RemoveService(typeof(IRepository<UIView>));
            var moPageRepository = host.GetMoqRepository<UIView, IRepository<UIView>>();
            var entityID1 = Guid.NewGuid();
            var entityID2 = Guid.NewGuid();
            moPageRepository.Setup(a => a.Add(It.Is<UIView>(b => b.Id == entityID1))).Throws(new DbException(entityID1.ToString()));
            moPageRepository.Setup(a => a.Update(It.Is<UIView>(b => b.Id == entityID2))).Throws(new DbException(entityID2.ToString()));
            moPageRepository.Setup(a => a.Remove(It.Is<UIView>(b => b.Id == entityID2))).Throws(new DbException(entityID2.ToString()));
            moPageRepository.Setup<UIView>(a => a.GetByKey(entityID1)).Returns(new UIView { Id = entityID1 });
            moPageRepository.Setup<UIView>(a => a.GetByKey(entityID2)).Returns(new UIView { Id = entityID2 });
            host.AddService(typeof(IRepository<UIView>), moPageRepository.Object);

            host.Handle(new AddFunctionCommand(new FunctionCreateInput
            {
                Id = entityID1,
                Code = "fun1",
                Description = string.Empty,
                DeveloperID = host.SysUsers.GetDevAccounts().First().Id,
                IsEnabled = 1,
                IsManaged = true,
                ResourceTypeID = host.ResourceTypeSet.First().Id,
                SortCode = 10
            }));
            host.Handle(new AddFunctionCommand(new FunctionCreateInput
            {
                Id = entityID2,
                Code = "fun2",
                Description = string.Empty,
                DeveloperID = host.SysUsers.GetDevAccounts().First().Id,
                IsEnabled = 1,
                IsManaged = true,
                ResourceTypeID = host.ResourceTypeSet.First().Id,
                SortCode = 10
            }));
            FunctionState functionByID;
            Assert.Equal(2, host.FunctionSet.Count());
            Assert.True(host.FunctionSet.TryGetFunction(entityID1, out functionByID));
            Assert.True(host.FunctionSet.TryGetFunction(entityID2, out functionByID));

            bool catched = false;
            try
            {
                host.Handle(new AddUIViewCommand(new UIViewCreateInput
                {
                    Id = entityID1
                }));
            }
            catch (Exception e)
            {
                Assert.Equal(e.GetType(), typeof(DbException));
                catched = true;
                Assert.Equal(entityID1.ToString(), e.Message);
            }
            finally
            {
                Assert.True(catched);
                Assert.Equal(0, host.UIViewSet.Count());
            }

            host.Handle(new AddUIViewCommand(new UIViewCreateInput
            {
                Id = entityID2
            }));
            Assert.Equal(1, host.UIViewSet.Count());

            catched = false;
            try
            {
                host.Handle(new UpdateUIViewCommand(new UIViewUpdateInput
                {
                    Id = entityID2
                }));
            }
            catch (Exception e)
            {
                Assert.Equal(e.GetType(), typeof(DbException));
                catched = true;
                Assert.Equal(entityID2.ToString(), e.Message);
            }
            finally
            {
                Assert.True(catched);
                Assert.Equal(1, host.UIViewSet.Count());
                UIViewState Page;
                Assert.True(host.UIViewSet.TryGetUIView(entityID2, out Page));
            }

            catched = false;
            try
            {
                host.Handle(new RemoveUIViewCommand(entityID2));
            }
            catch (Exception e)
            {
                Assert.Equal(e.GetType(), typeof(DbException));
                catched = true;
                Assert.Equal(entityID2.ToString(), e.Message);
            }
            finally
            {
                Assert.True(catched);
                UIViewState Page;
                Assert.True(host.UIViewSet.TryGetUIView(entityID2, out Page));
                Assert.Equal(1, host.UIViewSet.Count());
            }
        }
        #endregion
    }
}

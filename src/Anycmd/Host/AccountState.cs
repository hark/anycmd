﻿
namespace Anycmd.Host
{
    using Anycmd.AC.Identity;
    using Model;
    using System;
    using Util;

    /// <summary>
    /// 表示账户业务实体类型。
    /// </summary>
    public sealed class AccountState : StateObject<AccountState>, IAccount, IStateObject
    {
        private string _name;
        private int _numberID;
        private DateTime? _createOn;
        private string _loginName;
        private string _code;
        private string _email;
        private string _mobile;
        private string _qq;

        /// <summary>
        /// 空账户
        /// </summary>
        public static readonly AccountState Empty = new AccountState
        {
            _numberID = int.MinValue,
            _createOn = SystemTime.MinDate,
            Id = Guid.Empty,
            _loginName = string.Empty,
            _code = string.Empty,
            _email = string.Empty,
            _mobile = string.Empty,
            _name = string.Empty,
            _qq = string.Empty
        };

        private AccountState() { }

        public static AccountState Create(AccountBase account)
        {
            if (account == null)
            {
                return Empty;
            }
            return new AccountState()
            {
                _numberID = account.NumberID,
                Id = account.Id,
                _loginName = account.LoginName,
                _createOn = account.CreateOn,
                _code = account.Code,
                _email = account.Email,
                _mobile = account.Mobile,
                _name = account.Name,
                _qq = account.QQ
            };
        }

        public int NumberID
        {
            get { return _numberID; }
        }

        public string LoginName
        {
            get { return _loginName; }
        }

        public DateTime? CreateOn
        {
            get { return _createOn; }
        }

        public string Name
        {
            get { return _name; }
        }

        public string Code
        {
            get { return _code; }
        }

        public string Email
        {
            get { return _email; }
        }

        public string QQ
        {
            get { return _qq; }
        }

        public string Mobile
        {
            get { return _mobile; }
        }

        protected override bool DoEquals(AccountState other)
        {
            return Id == other.Id &&
                LoginName == other.LoginName &&
                NumberID == other.NumberID &&
                Name == other.Name &&
                Code == other.Code &&
                Email == other.Email &&
                QQ == other.QQ &&
                Mobile == other.Mobile;
        }
    }
}

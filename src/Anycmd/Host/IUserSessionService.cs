﻿
namespace Anycmd.Host
{
    using System;

    /// <summary>
    /// 表示该接口的实现类是用户会话服务。
    /// </summary>
    public interface IUserSessionService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sessionID"></param>
        /// <param name="account"></param>
        /// <returns></returns>
        IUserSession CreateSession(IACDomain host, Guid sessionID, AccountState account);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sessionID"></param>
        void DeleteSession(IACDomain host, Guid sessionID);
    }
}

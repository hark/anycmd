﻿using System;

namespace Anycmd.Host.Impl
{
    using AC;
    using AC.Identity;
    using AC.Infra;
    using AC.Infra.Messages;
    using AC.MemorySets.Impl;
    using AC.MessageHandlers;
    using AC.InOuts;
    using Anycmd.Rdb;
    using Bus;
    using Bus.DirectBus;
    using EDI;
    using EDI.Entities;
    using EDI.Handlers;
    using EDI.MessageHandlers;
    using EDI.Messages;
    using Extensions;
    using Logging;
    using Query;
    using Rdb;

    /// <summary>
    /// 系统实体宿主。
    /// </summary>
    public class DefaultACDomain : ACDomain
    {
        public DefaultACDomain()
        {
            base.MessageDispatcher = new MessageDispatcher();
            base.CommandBus = new DirectCommandBus(this.MessageDispatcher);
            this.EventBus = new DirectEventBus(this.MessageDispatcher);

            base.Rdbs = new Rdbs(this);
            base.DbTables = new DbTables(this);
            base.DbViews = new DbViews(this);
            base.DbTableColumns = new DbTableColumns(this);
            base.DbViewColumns = new DbViewColumns(this);
            base.AppSystemSet = new AppSystemSet(this);
            base.ButtonSet = new ButtonSet(this);
            base.SysUsers = new SysUserSet(this);
            base.DicSet = new DicSet(this);
            base.EntityTypeSet = new EntityTypeSet(this);
            base.FunctionSet = new FunctionSet(this);
            base.OrganizationSet = new OrganizationSet(this);
            base.UIViewSet = new UIViewSet(this);
            base.ResourceTypeSet = new ResourceTypeSet(this);
            base.PrivilegeSet = new PrivilegeSet(this);
            base.MenuSet = new MenuSet(this);
            base.RoleSet = new RoleSet(this);
            base.SSDSetSet = new SSDSetSet(this);
            base.DSDSetSet = new DSDSetSet(this);
            base.GroupSet = new GroupSet(this);
            this.NodeHost = new DefaultNodeHost(this);
        }

        public override void Configure()
        {
            this.AddDefaultService<IOriginalHostStateReader>(new RdbOriginalHostStateReader(this));
            this.AddDefaultService<IRdbMetaDataService>(new SQLServerMetaDataService(this));
            this.AddDefaultService<ISqlFilterStringBuilder>(new SqlFilterStringBuilder());
            this.AddDefaultService<ISecurityService>(new DefaultSecurityService());
            this.AddDefaultService<IPasswordEncryptionService>(new PasswordEncryptionService(this));
            this.AddDefaultService<IUserSessionService>(new DefaultUserSessionService());
            this.AddDefaultService<IRBACService>(new DefaultRBACService(this));

            new OntologyMessageHandler(this).Register();

            base.MessageDispatcher.Register(new AccountLoginedEventHandler(this));
            base.MessageDispatcher.Register(new AccountLogoutedEventHandler(this));
            base.MessageDispatcher.Register(new AddVisitingLogCommandHandler(this));
            base.MessageDispatcher.Register(new AddAccountCommandHandler(this));
            base.MessageDispatcher.Register(new UpdateAccountCommandHandler(this));
            base.MessageDispatcher.Register(new RemoveAccountCommandHandler(this));
            base.MessageDispatcher.Register(new AddPasswordCommandHandler(this));
            base.MessageDispatcher.Register(new ChangePasswordCommandHandler(this));
            base.MessageDispatcher.Register(new SaveHelpCommandHandler(this));

            this.MessageDispatcher.Register(new OperatedEventHandler(this));

            this.AddService(typeof(INodeHostBootstrap), new FastNodeHostBootstrap(this));
            this.MessageDispatcher.Register(new AddBatchCommandHandler(this));
            this.MessageDispatcher.Register(new UpdateBatchCommandHandler(this));
            this.MessageDispatcher.Register(new RemoveBatchCommandHandler(this));

            this.Map(EntityTypeMap.Create<Action>("EDI"));
            this.Map(EntityTypeMap.Create<Archive>("EDI"));
            this.Map(EntityTypeMap.Create<Batch>("EDI"));
            this.Map(EntityTypeMap.Create<Element>("EDI"));
            this.Map(EntityTypeMap.Create<InfoDic>("EDI"));
            this.Map(EntityTypeMap.Create<InfoDicItem>("EDI"));
            this.Map(EntityTypeMap.Create<InfoGroup>("EDI"));
            this.Map(EntityTypeMap.Create<InfoRule>("EDI"));
            this.Map(EntityTypeMap.Create<Node>("EDI"));
            this.Map(EntityTypeMap.Create<NodeElementAction>("EDI"));
            this.Map(EntityTypeMap.Create<NodeElementCare>("EDI"));
            this.Map(EntityTypeMap.Create<NodeTopic>("EDI"));
            this.Map(EntityTypeMap.Create<NodeOntologyCare>("EDI"));
            this.Map(EntityTypeMap.Create<NodeOntologyOrganization>("EDI"));
            this.Map(EntityTypeMap.Create<Ontology>("EDI"));
            this.Map(EntityTypeMap.Create<OntologyOrganization>("EDI"));
            this.Map(EntityTypeMap.Create<Plugin>("EDI"));
            this.Map(EntityTypeMap.Create<Process>("EDI"));
            this.Map(EntityTypeMap.Create<StateCode>("EDI"));
            this.Map(EntityTypeMap.Create<Topic>("EDI"));
            this.Map(EntityTypeMap.Create<MessageEntity>("EDI", "Command"));

            // TODO:实现一个良好的插件架构
            // TODO:参考InfoRule模块完成命令插件模块的配置
            //var plugins = Resolver.Resolve<IPluginImporter>().GetPlugins();
            //if (plugins != null) {
            //    foreach (var item in plugins) {
            //        this.Plugins.Add(item);
            //    }
            //}

            this.Map(EntityTypeMap.Create<RDatabase>("AC"));
            this.Map(EntityTypeMap.Create<DbTable>("AC"));
            this.Map(EntityTypeMap.Create<DbView>("AC"));
            this.Map(EntityTypeMap.Create<DbTableColumn>("AC"));
            this.Map(EntityTypeMap.Create<DbViewColumn>("AC"));
            this.Map(EntityTypeMap.Create<DbTableSpace>("AC", "TableSpace"));
            this.Map(EntityTypeMap.Create<ExceptionLog>("AC"));
            this.Map(EntityTypeMap.Create<OperationLog>("AC"));
            this.Map(EntityTypeMap.Create<OperationHelp>("AC", "Help"));
            this.Map(EntityTypeMap.Create<AnyLog>("AC"));

            this.Map(EntityTypeMap.Create<AppSystem>("AC"));
            this.Map(EntityTypeMap.Create<Button>("AC"));
            this.Map(EntityTypeMap.Create<Dic>("AC"));
            this.Map(EntityTypeMap.Create<DicItem>("AC"));
            this.Map(EntityTypeMap.Create<EntityType>("AC"));
            this.Map(EntityTypeMap.Create<Function>("AC"));
            this.Map(EntityTypeMap.Create<Menu>("AC"));
            this.Map(EntityTypeMap.Create<OperationHelp>("AC"));
            this.Map(EntityTypeMap.Create<Organization>("AC"));
            this.Map(EntityTypeMap.Create<UIView>("AC"));
            this.Map(EntityTypeMap.Create<UIViewButton>("AC"));
            this.Map(EntityTypeMap.Create<Property>("AC"));
            this.Map(EntityTypeMap.Create<ResourceType>("AC"));

            this.Map(EntityTypeMap.Create<Account>("AC"));
            this.Map(EntityTypeMap.Create<DeveloperID>("AC"));
            this.Map(EntityTypeMap.Create<VisitingLog>("AC"));

            this.Map(EntityTypeMap.Create<Group>("AC"));
            this.Map(EntityTypeMap.Create<Role>("AC"));
            this.Map(EntityTypeMap.Create<SSDSet>("AC"));
            this.Map(EntityTypeMap.Create<DSDSet>("AC"));
            this.Map(EntityTypeMap.Create<PrivilegeBigram>("AC"));
        }

        private void AddDefaultService<T>(object service)
        {
            if (this.GetService(typeof(T)) == null)
            {
                this.AddService(typeof(T), service);
            }
        }

        private class OntologyMessageHandler :
            IHandler<OntologyAddedEvent>,
            IHandler<OntologyUpdatedEvent>,
            IHandler<OntologyRemovedEvent>
        {
            private readonly IACDomain host;

            public OntologyMessageHandler(IACDomain host)
            {
                this.host = host;
            }

            public void Register()
            {
                var messageDispatcher = host.MessageDispatcher;
                if (messageDispatcher == null)
                {
                    throw new ArgumentNullException("messageDispatcher has not be set of host:{0}".Fmt(host.Name));
                }
                messageDispatcher.Register((IHandler<OntologyAddedEvent>)this);
                messageDispatcher.Register((IHandler<OntologyUpdatedEvent>)this);
                messageDispatcher.Register((IHandler<OntologyRemovedEvent>)this);
            }

            public void Handle(OntologyAddedEvent message)
            {
                var entityMenu = new MenuCreateInput
                {
                    Id = new Guid("9BE27152-808E-427D-B8C6-D7ABC600A963"),
                    ParentID = null,
                    Name = "实体管理",
                    Url = string.Empty,
                    Icon = string.Empty,
                    AppSystemID = host.AppSystemSet.SelfAppSystem.Id,
                    Description = string.Empty,
                    SortCode = 0
                };
                MenuState parentMenu;
                if (!host.MenuSet.TryGetMenu(entityMenu.Id.Value, out parentMenu))
                {
                    host.Handle(new AddMenuCommand(entityMenu));
                }
                OntologyDescriptor ontology;
                if (host.NodeHost.Ontologies.TryGetOntology(message.Source.Id, out ontology))
                {
                    host.Handle(new AddMenuCommand(new MenuCreateInput
                    {
                        Id = ontology.Ontology.Id,// 约定
                        ParentID = entityMenu.Id,
                        Name = ontology.Ontology.Name + "管理",
                        Url = string.Format("EDI/Entity/Index?ontologyCode={0}&ontologyID={1}", ontology.Ontology.Code, ontology.Ontology.Id),
                        Icon = ontology.Ontology.Icon,
                        AppSystemID = host.AppSystemSet.SelfAppSystem.Id,
                        Description = string.Empty,
                        SortCode = ontology.Ontology.SortCode
                    }));
                }
            }

            public void Handle(OntologyUpdatedEvent message)
            {
                OntologyDescriptor ontology;
                if (host.NodeHost.Ontologies.TryGetOntology(message.Source.Id, out ontology))
                {
                    MenuState menu;
                    if (host.MenuSet.TryGetMenu(ontology.Ontology.Id, out menu))
                    {
                        host.Handle(new UpdateMenuCommand(new MenuUpdateInput
                        {
                            Id = ontology.Ontology.Id,
                            AppSystemID = menu.AppSystemID,
                            Description = menu.Description,
                            Icon = menu.Icon,
                            Name = ontology.Ontology.Name + "管理",
                            SortCode = menu.SortCode,
                            Url = string.Format("EDI/Entity/Index?ontologyCode={0}&ontologyID={1}", ontology.Ontology.Code, ontology.Ontology.Id)
                        }));
                    }
                }
            }

            public void Handle(OntologyRemovedEvent message)
            {
                OntologyDescriptor ontology;
                if (host.NodeHost.Ontologies.TryGetOntology(message.Source.Id, out ontology))
                {
                    MenuState menu;
                    if (host.MenuSet.TryGetMenu(ontology.Ontology.Id, out menu))
                    {
                        host.Handle(new RemoveMenuCommand(ontology.Ontology.Id));
                    }
                }
            }

            private class MenuCreateInput : IMenuCreateIO
            {
                public Guid? Id { get; set; }

                public Guid AppSystemID { get; set; }

                public string Description { get; set; }

                public string Icon { get; set; }

                public string Name { get; set; }

                public Guid? ParentID { get; set; }

                public int SortCode { get; set; }

                public string Url { get; set; }
            }

            private class MenuUpdateInput : IMenuUpdateIO
            {
                public Guid Id { get; set; }

                public Guid AppSystemID { get; set; }

                public string Description { get; set; }

                public string Icon { get; set; }

                public string Name { get; set; }

                public int SortCode { get; set; }

                public string Url { get; set; }
            }
        }
    }
}

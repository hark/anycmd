﻿
namespace Anycmd.Host.Impl
{
    using Host.AC;
    using Repositories;
    using System;

    public class DefaultUserSessionService : IUserSessionService
    {
        #region CreateSession
        /// <summary>
        /// 创建AC会话
        /// </summary>
        /// <param name="account"></param>
        /// <param name="sessionID">会话标识。会话级的权限依赖于会话的持久跟踪</param>
        /// <returns></returns>
        public IUserSession CreateSession(IACDomain host, Guid sessionID, AccountState account)
        {
            var userSessionRepository = host.GetRequiredService<IRepository<UserSession>>();
            var identity = new AnycmdIdentity("Anycmd", true, account.LoginName);
            var principal = new AnycmdPrincipal(host, identity);
            IUserSession user = new UserSessionState(host, sessionID, principal, account);
            var userSessionEntity = new UserSession
            {
                Id = sessionID,
                AccountID = account.Id,
                AuthenticationType = identity.AuthenticationType,
                Description = null,
                IsAuthenticated = identity.IsAuthenticated,
                IsEnabled = 1,
                LoginName = account.LoginName
            };
            userSessionRepository.Add(userSessionEntity);
            userSessionRepository.Context.Commit();
            return user;
        }
        #endregion

        #region DeleteSession
        /// <summary>
        /// 删除会话
        /// <remarks>
        /// 会话不应该经常删除，会话级的权限依赖于会话的持久跟踪。用户退出系统只需要清空该用户的内存会话记录和更新数据库中的会话记录为IsAuthenticated为false而不需要删除持久的UserSession。
        /// </remarks>
        /// </summary>
        /// <param name="sessionID"></param>
        public void DeleteSession(IACDomain host, Guid sessionID)
        {
            var userSessionRepository = host.GetRequiredService<IRepository<UserSession>>();
            var userSessionEntity = userSessionRepository.GetByKey(sessionID);
            if (userSessionEntity != null)
            {
                userSessionRepository.Remove(userSessionEntity);
                userSessionRepository.Context.Commit();
            }
        }
        #endregion
    }
}

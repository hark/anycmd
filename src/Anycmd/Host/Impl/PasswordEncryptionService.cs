﻿
namespace Anycmd.Host.Impl
{
    using Util;

    public class PasswordEncryptionService : IPasswordEncryptionService
    {
        private readonly IACDomain host;

        public PasswordEncryptionService(IACDomain host)
        {
            this.host = host;
        }

        public string Encrypt(string rawPwd)
        {
            return EncryptionHelper.Hash(rawPwd);
        }
    }
}

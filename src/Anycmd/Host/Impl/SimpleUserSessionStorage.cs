﻿
namespace Anycmd.Host.Impl
{
    using System;
    using System.Collections.Concurrent;

    public sealed class SimpleUserSessionStorage : IUserSessionStorage
    {
        private readonly ConcurrentDictionary<string, object> dic = new ConcurrentDictionary<string, object>(StringComparer.OrdinalIgnoreCase);

        public void SetData(string key, object data)
        {
            object value;
            if (dic.TryGetValue(key, out value))
            {
                value = data;
            }
            else
            {
                dic.GetOrAdd(key, data);   
            }
        }

        public object GetData(string key)
        {
            object value;
            if (dic.TryGetValue(key, out value))
            {
                return value;
            }
            return null;
        }

        public void Clear()
        {
            dic.Clear();
        }
    }
}

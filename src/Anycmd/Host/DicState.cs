﻿
namespace Anycmd.Host
{
    using Anycmd.AC.Infra;
    using Model;
    using System;
    using Util;

    /// <summary>
    /// 表示系统字典业务实体。
    /// </summary>
    public sealed class DicState : StateObject<DicState>, IDic, IStateObject
    {
        public static readonly DicState Empty = new DicState
        {
            Code = string.Empty,
            CreateOn = SystemTime.MinDate,
            Id = Guid.Empty,
            IsEnabled = 0,
            Name = string.Empty,
            SortCode = 0
        };

        private DicState() { }

        public static DicState Create(DicBase dic)
        {
            if (dic == null)
            {
                throw new ArgumentNullException("dic");
            }
            return new DicState
            {
                Id = dic.Id,
                Code = dic.Code,
                Name = dic.Name,
                IsEnabled = dic.IsEnabled,
                CreateOn = dic.CreateOn,
                SortCode = dic.SortCode
            };
        }

        public string Code { get; private set; }

        public string Name { get; private set; }

        public int IsEnabled { get; private set; }

        public int SortCode { get; private set; }

        public DateTime? CreateOn { get; private set; }

        protected override bool DoEquals(DicState other)
        {
            return Id == other.Id &&
                Code == other.Code &&
                Name == other.Name &&
                IsEnabled == other.IsEnabled &&
                SortCode == other.SortCode;
        }
    }
}

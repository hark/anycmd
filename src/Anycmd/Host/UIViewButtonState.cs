﻿
namespace Anycmd.Host
{
    using Anycmd.AC.Infra;
    using Exceptions;
    using Model;
    using System;

    public sealed class UIViewButtonState : StateObject<UIViewButtonState>, IUIViewButton, IStateObject
    {
        private Guid _viewID;
        private Guid _buttonID;
        private Guid? _functionID;
        private IACDomain ACDomain { get; set; }

        private UIViewButtonState() { }

        public static UIViewButtonState Create(IACDomain host, UIViewButtonBase viewButton)
        {
            if (viewButton == null)
            {
                throw new ArgumentNullException("viewButton");
            }
            UIViewState view;
            if (!host.UIViewSet.TryGetUIView(viewButton.UIViewID, out view))
            {
                throw new CoreException("意外的界面视图" + viewButton.UIViewID);
            }
            ButtonState button;
            if (!host.ButtonSet.TryGetButton(viewButton.ButtonID, out button))
            {
                throw new CoreException("意外的按钮" + viewButton.ButtonID);
            }
            return new UIViewButtonState
            {
                ACDomain = host,
                Id = viewButton.Id,
                UIViewID = viewButton.UIViewID,
                FunctionID = viewButton.FunctionID,
                ButtonID = viewButton.ButtonID,
                IsEnabled = viewButton.IsEnabled,
                CreateOn = viewButton.CreateOn
            };
        }

        /// <summary>
        /// 
        /// </summary>
        public Guid UIViewID
        {
            get { return _viewID; }
            private set
            {
                UIViewState view;
                if (!ACDomain.UIViewSet.TryGetUIView(value, out view))
                {
                    throw new ValidationException("以外的界面视图标识" + value);
                }
                _viewID = value;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public Guid ButtonID
        {
            get { return _buttonID; }
            set
            {
                ButtonState button;
                if (!ACDomain.ButtonSet.TryGetButton(value, out button))
                {
                    throw new ValidationException("意外的按钮标识" + value);
                }
                _buttonID = value;
            }
        }

        public Guid? FunctionID
        {
            get { return _functionID; }
            private set
            {
                if (value == Guid.Empty)
                {
                    value = null;
                }
                if (value.HasValue)
                {
                    FunctionState function;
                    if (!ACDomain.FunctionSet.TryGetFunction(value.Value, out function))
                    {
                        throw new ValidationException("意外的功能标识" + value);
                    }
                    _functionID = value;
                }
            }
        }

        public int IsEnabled { get; private set; }

        public DateTime? CreateOn { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public ButtonState Button
        {
            get
            {
                ButtonState button;
                if (!ACDomain.ButtonSet.TryGetButton(this.ButtonID, out button))
                {
                    throw new CoreException("意外的按钮" + this.ButtonID);
                }
                return button;
            }
        }

        public UIViewState UIView
        {
            get
            {
                UIViewState view;
                if (!ACDomain.UIViewSet.TryGetUIView(this.UIViewID, out view))
                {
                    throw new CoreException("意外的界面视图按钮界面视图标识" + this.UIViewID);
                }
                return view;
            }
        }

        protected override bool DoEquals(UIViewButtonState other)
        {
            return Id == other.Id &&
                UIViewID == other.UIViewID &&
                FunctionID == other.FunctionID &&
                ButtonID == other.ButtonID &&
                IsEnabled == other.IsEnabled;
        }
    }
}

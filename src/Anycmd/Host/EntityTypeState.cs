﻿
namespace Anycmd.Host
{
    using Anycmd.AC.Infra;
    using Exceptions;
    using Host.AC.Infra;
    using Model;
    using System;
    using Util;

    /// <summary>
    /// 表示实体类型业务实体。
    /// </summary>
    public sealed class EntityTypeState : StateObject<EntityTypeState>, IEntityType, IStateObject
    {
        public static readonly EntityTypeState Empty = new EntityTypeState
        {
            Codespace = string.Empty,
            Code = string.Empty,
            IsOrganizational = false,
            CreateOn = SystemTime.MinDate,
            DatabaseID = Guid.Empty,
            DeveloperID = Guid.Empty,
            EditHeight = 0,
            EditWidth = 0,
            Id = Guid.Empty,
            Name = string.Empty,
            SchemaName = string.Empty,
            SortCode = 0,
            TableName = string.Empty
        };

        private EntityTypeMap _map;

        private EntityTypeState() { }

        public static EntityTypeState Create(IACDomain host, EntityTypeBase entityType, EntityTypeMap map)
        {
            if (entityType == null)
            {
                throw new ArgumentNullException("entityType");
            }
            if (!host.Rdbs.ContainsDb(entityType.DatabaseID))
            {
                throw new CoreException("意外的数据库" + entityType.DatabaseID);
            }

            return new EntityTypeState
            {
                ACDomain = host,
                Map = map,
                Codespace = entityType.Codespace,
                Code = entityType.Code,
                IsOrganizational = entityType.IsOrganizational,
                CreateOn = entityType.CreateOn,
                DatabaseID = entityType.DatabaseID,
                DeveloperID = entityType.DeveloperID,
                EditHeight = entityType.EditHeight,
                EditWidth = entityType.EditWidth,
                Id = entityType.Id,
                Name = entityType.Name,
                SchemaName = entityType.SchemaName,
                SortCode = entityType.SortCode,
                TableName = entityType.TableName
            };
        }

        public IACDomain ACDomain { get; private set; }

        public EntityTypeMap Map
        {
            get
            {
                if (_map == null)
                {
                    return EntityTypeMap.Empty;
                }
                return _map;
            }
            private set { _map = value; }
        }

        public string Codespace { get; private set; }

        public string Code { get; private set; }

        public string Name { get; private set; }

        public bool IsOrganizational { get; private set; }

        public Guid DatabaseID { get; private set; }

        public Guid DeveloperID { get; private set; }

        public string SchemaName { get; private set; }

        public string TableName { get; private set; }

        public int SortCode { get; private set; }

        public int EditWidth { get; private set; }

        public int EditHeight { get; private set; }

        public DateTime? CreateOn { get; private set; }

        protected override bool DoEquals(EntityTypeState other)
        {
            return Id == other.Id &&
                Codespace == other.Codespace &&
                Code == other.Code &&
                Name == other.Name &&
                IsOrganizational == other.IsOrganizational &&
                DatabaseID == other.DatabaseID &&
                SchemaName == other.SchemaName &&
                TableName == other.TableName &&
                SortCode == other.SortCode &&
                DeveloperID == other.DeveloperID &&
                EditHeight == other.EditHeight &&
                EditWidth == other.EditWidth;
        }
    }
}

﻿
namespace Anycmd.Host
{
    using Anycmd.AC;
    using Model;
    using System;

    /// <summary>
    /// 表示静态职责分离角色集业务实体。
    /// </summary>
    public class SSDSetState : StateObject<SSDSetState>, ISSDSet, IStateObject
    {
        private SSDSetState() { }

        public static SSDSetState Create(SSDSetBase ssdSet)
        {
            return new SSDSetState
            {
                Id = ssdSet.Id,
                Name = ssdSet.Name,
                IsEnabled = ssdSet.IsEnabled,
                SSDCard = ssdSet.SSDCard,
                Description = ssdSet.Description,
                CreateOn = ssdSet.CreateOn
            };
        }

        public string Name { get; private set; }

        public int SSDCard { get; private set; }

        public int IsEnabled { get; private set; }

        public string Description { get; private set; }

        public DateTime? CreateOn { get; private set; }

        protected override bool DoEquals(SSDSetState other)
        {
            return Id == other.Id &&
                Name == other.Name &&
                SSDCard == other.SSDCard &&
                IsEnabled == other.IsEnabled &&
                Description == other.Description;
        }
    }
}

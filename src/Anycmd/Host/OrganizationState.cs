﻿
namespace Anycmd.Host
{
    using Anycmd.AC.Infra;
    using Model;
    using System;
    using Util;

    /// <summary>
    /// 表示组织结构业务实体。
    /// </summary>
    public sealed class OrganizationState : StateObject<OrganizationState>, IOrganization, IStateObject
    {
        public static readonly OrganizationState VirtualRoot = new OrganizationState
        {
            CategoryCode = string.Empty,
            Code = string.Empty,
            CreateOn = SystemTime.MinDate,
            Description = string.Empty,
            Id = Guid.Empty,
            IsEnabled = 1,
            ModifiedOn = null,
            Name = "虚拟根",
            ShortName = "根",
            ParentCode = null,
            SortCode = 0,
            ContractorID = null
        };

        public static readonly OrganizationState Empty = new OrganizationState
        {
            CategoryCode = string.Empty,
            Code = string.Empty,
            CreateOn = SystemTime.MinDate,
            Description = string.Empty,
            Id = Guid.NewGuid(),
            IsEnabled = 1,
            ModifiedOn = null,
            Name = "无",
            ShortName = "无",
            ParentCode = null,
            ContractorID = null,
            SortCode = 0
        };

        private OrganizationState() { }

        public static OrganizationState Create(IACDomain host, OrganizationBase organization)
        {
            if (organization == null)
            {
                throw new ArgumentNullException("organization");
            }
            return new OrganizationState
            {
                ACDomain = host,
                CategoryCode = organization.CategoryCode,
                Code = organization.Code,
                CreateOn = organization.CreateOn,
                Description = organization.Description,
                Id = organization.Id,
                IsEnabled = organization.IsEnabled,
                ModifiedOn = organization.ModifiedOn,
                Name = organization.Name,
                ShortName = organization.ShortName,
                ParentCode = organization.ParentCode,
                SortCode = organization.SortCode,
                ContractorID = organization.ContractorID
            };
        }

        public IACDomain ACDomain { get; private set; }

        public string Code { get; private set; }

        public string Name { get; private set; }

        public string ShortName { get; private set; }

        public string ParentCode { get; private set; }

        public string CategoryCode { get; private set; }

        public Guid? ContractorID { get; private set; }

        public DateTime? CreateOn { get; private set; }

        public DateTime? ModifiedOn { get; private set; }

        public string Description { get; private set; }

        public int IsEnabled { get; private set; }

        public int SortCode { get; private set; }

        /// <summary>
        /// 返回Empty，不会返回null。
        /// 虚拟根的父级是Empty。Empty没有父级
        /// </summary>
        public OrganizationState Parent
        {
            get
            {
                if (this.Equals(OrganizationState.Empty))
                {
                    throw new InvalidOperationException("不能访问Null组织结构的父级");
                }
                if (this.Equals(OrganizationState.VirtualRoot))
                {
                    return OrganizationState.Empty;
                }
                if (string.IsNullOrEmpty(this.ParentCode))
                {
                    return OrganizationState.VirtualRoot;
                }
                OrganizationState _parent;
                if (!ACDomain.OrganizationSet.TryGetOrganization(this.ParentCode, out _parent))
                {
                    return OrganizationState.Empty;
                }

                return _parent;
            }
        }

        protected override bool DoEquals(OrganizationState other)
        {
            return Id == other.Id &&
                Code == other.Code &&
                Name == other.Name &&
                ShortName == other.ShortName &&
                ParentCode == other.ParentCode &&
                CategoryCode == other.CategoryCode &&
                IsEnabled == other.IsEnabled &&
                ContractorID == other.ContractorID &&
                SortCode == other.SortCode &&
                Description == other.Description;
        }
    }
}

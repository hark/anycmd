﻿
namespace Anycmd.Host
{
    using Anycmd.AC;
    using Model;
    using System;

    /// <summary>
    /// 表示东塔职责分离角色业务实体。
    /// </summary>
    public sealed class DSDRoleState : StateObject<DSDRoleState>, IDSDRole, IStateObject
    {
        private DSDRoleState() { }

        public static DSDRoleState Create(DSDRoleBase dsdRole)
        {
            return new DSDRoleState
            {
                Id = dsdRole.Id,
                RoleID = dsdRole.RoleID,
                DSDSetID = dsdRole.DSDSetID,
                CreateOn = dsdRole.CreateOn
            };
        }

        public Guid DSDSetID { get; private set; }

        public Guid RoleID { get; private set; }

        public DateTime? CreateOn { get; private set; }

        protected override bool DoEquals(DSDRoleState other)
        {
            return Id == other.Id &&
                DSDSetID == other.DSDSetID &&
                RoleID == other.RoleID;
        }
    }
}

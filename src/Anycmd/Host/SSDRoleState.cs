﻿
namespace Anycmd.Host
{
    using Anycmd.AC;
    using Model;
    using System;

    /// <summary>
    /// 表示静态职责分离角色业务实体。
    /// </summary>
    public sealed class SSDRoleState : StateObject<SSDRoleState>, ISSDRole, IStateObject
    {
        private SSDRoleState() { }

        public static SSDRoleState Create(SSDRoleBase ssdRole)
        {
            return new SSDRoleState
            {
                Id = ssdRole.Id,
                RoleID = ssdRole.RoleID,
                SSDSetID = ssdRole.SSDSetID,
                CreateOn = ssdRole.CreateOn
            };
        }

        public Guid SSDSetID { get; private set; }

        public Guid RoleID { get; private set; }

        public DateTime? CreateOn { get; private set; }

        protected override bool DoEquals(SSDRoleState other)
        {
            return Id == other.Id &&
                SSDSetID == other.SSDSetID &&
                RoleID == other.RoleID;
        }
    }
}

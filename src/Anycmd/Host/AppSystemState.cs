﻿
namespace Anycmd.Host
{
    using Anycmd.AC.Infra;
    using Exceptions;
    using Model;
    using System;
    using Util;

    /// <summary>
    /// 表示应用系统业务实体类型。
    /// </summary>
    public sealed class AppSystemState : StateObject<AppSystemState>, IAppSystem, IStateObject
    {
        public static readonly AppSystemState Empty = new AppSystemState
        {
            Code = string.Empty,
            CreateOn = SystemTime.MinDate,
            Icon = string.Empty,
            Id = Guid.Empty,
            IsEnabled = 0,
            Name = string.Empty,
            PrincipalID = Guid.Empty,
            SortCode = 0,
            SSOAuthAddress = string.Empty
        };

        private AppSystemState() { }

        public static AppSystemState Create(IACDomain host, AppSystemBase appSystem)
        {
            if (appSystem == null)
            {
                throw new ArgumentNullException("appSystem");
            }
            AccountState principal;
            if (!host.SysUsers.TryGetDevAccount(appSystem.PrincipalID, out principal))
            {
                throw new CoreException("意外的应用系统负责人标识" + appSystem.PrincipalID);
            }
            return new AppSystemState
            {
                Id = appSystem.Id,
                Code = appSystem.Code,
                Name = appSystem.Name,
                SortCode = appSystem.SortCode,
                PrincipalID = appSystem.PrincipalID,
                IsEnabled = appSystem.IsEnabled,
                SSOAuthAddress = appSystem.SSOAuthAddress,
                Icon = appSystem.Icon,
                CreateOn = appSystem.CreateOn,
            };
        }

        public string Code { get; private set; }

        public string Name { get; private set; }

        public int SortCode { get; set; }

        public Guid PrincipalID { get; set; }

        public int IsEnabled { get; private set; }

        public string SSOAuthAddress { get; private set; }

        public string Icon { get; set; }

        public DateTime? CreateOn { get; private set; }

        protected override bool DoEquals(AppSystemState other)
        {
            return
                Id == other.Id &&
                Code == other.Code &&
                Name == other.Name &&
                SortCode == other.SortCode &&
                IsEnabled == other.IsEnabled &&
                SSOAuthAddress == other.SSOAuthAddress &&
                Icon == other.Icon;
        }
    }
}

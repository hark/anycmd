﻿
namespace Anycmd.Host
{
    using Anycmd.AC.Infra;
    using Model;
    using System;

    /// <summary>
    /// 表示按钮业务实体。
    /// </summary>
    public sealed class ButtonState : StateObject<ButtonState>, IButton, IStateObject
    {
        private ButtonState() { }

        public static ButtonState Create(ButtonBase button)
        {
            if (button == null)
            {
                throw new ArgumentNullException("button");
            }
            return new ButtonState
            {
                Id = button.Id,
                Name = button.Name,
                CategoryCode = button.CategoryCode,
                Code = button.Code,
                Icon = button.Icon,
                SortCode = button.SortCode,
                IsEnabled = button.IsEnabled,
                CreateOn = button.CreateOn
            };
        }

        public string Name { get; private set; }

        public string Code { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public string CategoryCode { get; set; }

        public string Icon { get; private set; }

        public int SortCode { get; private set; }

        public int IsEnabled { get; private set; }

        public DateTime? CreateOn { get; private set; }

        protected override bool DoEquals(ButtonState other)
        {
            return Id == other.Id &&
                Name == other.Name &&
                Code == other.Code &&
                CategoryCode == other.CategoryCode &&
                Icon == other.Icon &&
                SortCode == other.SortCode &&
                IsEnabled == other.IsEnabled;
        }
    }
}

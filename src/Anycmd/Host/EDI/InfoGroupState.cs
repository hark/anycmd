﻿
namespace Anycmd.Host.EDI
{
    using Anycmd.EDI;
    using Model;
    using System;

    public sealed class InfoGroupState : StateObject<InfoGroupState>, IInfoGroup, IStateObject
    {
        private InfoGroupState() { }

        public static InfoGroupState Create(IInfoGroup infoGroup)
        {
            if (infoGroup == null)
            {
                throw new ArgumentNullException("infoGroup");
            }
            return new InfoGroupState
            {
                Code = infoGroup.Code,
                Description = infoGroup.Description,
                Id = infoGroup.Id,
                Name = infoGroup.Name,
                OntologyID = infoGroup.OntologyID,
                SortCode = infoGroup.SortCode
            };
        }

        public string Code { get; private set; }

        public string Name { get; private set; }

        public Guid OntologyID { get; private set; }

        public int SortCode { get; private set; }

        public string Description { get; private set; }

        protected override bool DoEquals(InfoGroupState other)
        {
            return
                Id == other.Id &&
                Code == other.Code &&
                Name == other.Name &&
                OntologyID == other.OntologyID &&
                SortCode == other.SortCode &&
                Description == other.Description;
        }
    }
}

﻿
namespace Anycmd.Host.EDI.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class AddProcessCommand : AddEntityCommand<IProcessCreateIO>, ISysCommand
    {
        public AddProcessCommand(IProcessCreateIO input)
            : base(input)
        {

        }
    }
}

﻿
namespace Anycmd.Host.EDI.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class UpdateProcessCommand : UpdateEntityCommand<IProcessUpdateIO>, ISysCommand
    {
        public UpdateProcessCommand(IProcessUpdateIO input)
            : base(input)
        {

        }
    }
}

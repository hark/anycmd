﻿
namespace Anycmd.Host.EDI.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class AddNodeOntologyOrganizationCommand : AddEntityCommand<INodeOntologyOrganizationCreateIO>, ISysCommand
    {
        public AddNodeOntologyOrganizationCommand(INodeOntologyOrganizationCreateIO input)
            : base(input)
        {

        }
    }
}

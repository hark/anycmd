﻿
namespace Anycmd.Host.EDI.Messages
{
    using Anycmd.EDI;
    using Events;
    using InOuts;

    /// <summary>
    /// 
    /// </summary>
    public class InfoDicAddedEvent : DomainEvent
    {
        #region Ctor
        public InfoDicAddedEvent(InfoDicBase source, IInfoDicCreateIO output)
            : base(source)
        {
            if (output == null)
            {
                throw new System.ArgumentNullException("output");
            }
            this.Output = output;
        }
        #endregion

        public IInfoDicCreateIO Output { get; private set; }
    }
}

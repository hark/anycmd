﻿
namespace Anycmd.Host.EDI.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class UpdateInfoDicCommand : UpdateEntityCommand<IInfoDicUpdateIO>, ISysCommand
    {
        public UpdateInfoDicCommand(IInfoDicUpdateIO input)
            : base(input)
        {

        }
    }
}

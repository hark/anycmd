﻿
namespace Anycmd.Host.EDI.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class AddNodeElementActionCommand : AddEntityCommand<INodeElementActionCreateIO>, ISysCommand
    {
        public AddNodeElementActionCommand(INodeElementActionCreateIO input)
            : base(input)
        {

        }
    }
}

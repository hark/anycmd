﻿
namespace Anycmd.Host.EDI.Messages
{
    using Anycmd.EDI;
    using Events;

    public class OrganizationActionAddedEvent : DomainEvent {
        #region Ctor
        public OrganizationActionAddedEvent(OrganizationAction source) : base(source) { }
        #endregion
    }
}

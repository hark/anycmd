﻿
namespace Anycmd.Host.EDI.Messages
{
    using Anycmd.EDI;
    using Events;
    using InOuts;

    /// <summary>
    /// 
    /// </summary>
    public class InfoDicUpdatedEvent : DomainEvent
    {
        #region Ctor
        public InfoDicUpdatedEvent(InfoDicBase source, IInfoDicUpdateIO output)
            : base(source)
        {
            if (output == null)
            {
                throw new System.ArgumentNullException("output");
            }
            this.Output = output;
        }
        #endregion

        public IInfoDicUpdateIO Output { get; private set; }
    }
}

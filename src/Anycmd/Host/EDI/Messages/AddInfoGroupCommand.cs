﻿
namespace Anycmd.Host.EDI.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class AddInfoGroupCommand : AddEntityCommand<IInfoGroupCreateIO>, ISysCommand
    {
        public AddInfoGroupCommand(IInfoGroupCreateIO input)
            : base(input)
        {

        }
    }
}

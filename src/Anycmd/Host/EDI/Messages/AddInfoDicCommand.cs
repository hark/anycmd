﻿
namespace Anycmd.Host.EDI.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class AddInfoDicCommand : AddEntityCommand<IInfoDicCreateIO>, ISysCommand
    {
        public AddInfoDicCommand(IInfoDicCreateIO input)
            : base(input)
        {

        }
    }
}

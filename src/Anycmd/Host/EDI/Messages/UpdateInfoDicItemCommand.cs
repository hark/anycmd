﻿
namespace Anycmd.Host.EDI.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class UpdateInfoDicItemCommand : UpdateEntityCommand<IInfoDicItemUpdateIO>, ISysCommand
    {
        public UpdateInfoDicItemCommand(IInfoDicItemUpdateIO input)
            : base(input)
        {

        }
    }
}

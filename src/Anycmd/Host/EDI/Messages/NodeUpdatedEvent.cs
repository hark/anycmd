﻿
namespace Anycmd.Host.EDI.Messages
{
    using Anycmd.EDI;
    using Anycmd.Events;
    using InOuts;

    /// <summary>
    /// 
    /// </summary>
    public class NodeUpdatedEvent : DomainEvent
    {
        #region Ctor
        public NodeUpdatedEvent(NodeBase source, INodeUpdateIO output)
            : base(source)
        {
            if (output == null)
            {
                throw new System.ArgumentNullException("output");
            }
            this.Output = output;
        }
        #endregion

        public INodeUpdateIO Output { get; private set; }
    }
}

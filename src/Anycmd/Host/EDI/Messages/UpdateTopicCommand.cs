﻿
namespace Anycmd.Host.EDI.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class UpdateTopicCommand : UpdateEntityCommand<ITopicUpdateIO>, ISysCommand
    {
        public UpdateTopicCommand(ITopicUpdateIO input)
            : base(input)
        {

        }
    }
}

﻿
namespace Anycmd.Host.EDI.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class UpdateInfoGroupCommand : UpdateEntityCommand<IInfoGroupUpdateIO>, ISysCommand
    {
        public UpdateInfoGroupCommand(IInfoGroupUpdateIO input)
            : base(input)
        {

        }
    }
}

﻿
namespace Anycmd.Host.EDI.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class AddOrganizationActionCommand: AddEntityCommand<IOrganizationActionCreateIO>, ISysCommand
    {
        public AddOrganizationActionCommand(IOrganizationActionCreateIO input)
            : base(input)
        {

        }
    }
}

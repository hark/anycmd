﻿
namespace Anycmd.Host.EDI.Messages
{
    using Anycmd.EDI;
    using Events;
    using InOuts;

    /// <summary>
    /// 
    /// </summary>
    public class NodeElementCareAddedEvent : DomainEvent {
        #region Ctor
        public NodeElementCareAddedEvent(NodeElementCareBase source, INodeElementCareCreateIO output)
            : base(source)
        {
            if (output == null)
            {
                throw new System.ArgumentNullException("output");
            }
            this.Output = output;
        }
        #endregion

        public INodeElementCareCreateIO Output { get; private set; }
    }
}

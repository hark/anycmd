﻿
namespace Anycmd.Host.EDI.Messages
{
    using Commands;
    using Model;
    using InOuts;


    public class AddActionCommand : AddEntityCommand<IActionCreateIO>, ISysCommand
    {
        public AddActionCommand(IActionCreateIO input)
            : base(input)
        {

        }
    }
}

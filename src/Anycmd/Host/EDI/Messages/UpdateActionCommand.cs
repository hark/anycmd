﻿
namespace Anycmd.Host.EDI.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class UpdateActionCommand : UpdateEntityCommand<IActionUpdateIO>, ISysCommand
    {
        public UpdateActionCommand(IActionUpdateIO input)
            : base(input)
        {

        }
    }
}

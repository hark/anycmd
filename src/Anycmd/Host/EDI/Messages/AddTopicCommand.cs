﻿
namespace Anycmd.Host.EDI.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class AddTopicCommand : AddEntityCommand<ITopicCreateIO>, ISysCommand
    {
        public AddTopicCommand(ITopicCreateIO input)
            : base(input)
        {

        }
    }
}

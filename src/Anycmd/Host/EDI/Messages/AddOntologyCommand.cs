﻿
namespace Anycmd.Host.EDI.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class AddOntologyCommand : AddEntityCommand<IOntologyCreateIO>, ISysCommand
    {
        public AddOntologyCommand(IOntologyCreateIO input)
            : base(input)
        {

        }
    }
}

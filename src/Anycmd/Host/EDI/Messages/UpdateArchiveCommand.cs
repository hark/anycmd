﻿
namespace Anycmd.Host.EDI.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class UpdateArchiveCommand : UpdateEntityCommand<IArchiveUpdateIO>, ISysCommand
    {
        public UpdateArchiveCommand(IArchiveUpdateIO input)
            : base(input)
        {

        }
    }
}

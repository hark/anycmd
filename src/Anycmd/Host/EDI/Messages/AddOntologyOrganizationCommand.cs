﻿
namespace Anycmd.Host.EDI.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class AddOntologyOrganizationCommand: AddEntityCommand<IOntologyOrganizationCreateIO>, ISysCommand
    {
        public AddOntologyOrganizationCommand(IOntologyOrganizationCreateIO input)
            : base(input)
        {

        }
    }
}

﻿
namespace Anycmd.Host.EDI.Messages
{
    using Commands;
    using System;

    public class RemoveNodeOntologyOrganizationCommand : Command, ISysCommand
    {
        public RemoveNodeOntologyOrganizationCommand(Guid nodeID, Guid ontologyID, Guid organizationID)
        {
            this.NodeID = nodeID;
            this.OntologyID = ontologyID;
            this.OrganizationID = organizationID;
        }
        
        public Guid NodeID { get; private set; }
        public Guid OntologyID { get; private set; }
        public Guid OrganizationID { get; private set; }
    }
}

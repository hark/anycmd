﻿
namespace Anycmd.Host.EDI.Messages
{
    using Anycmd.EDI;
    using Events;

    public class ProcessAddedEvent: DomainEvent {
        #region Ctor
        public ProcessAddedEvent(ProcessBase source)
            : base(source) {
        }
        #endregion
    }
}

﻿
namespace Anycmd.Host.EDI.Messages
{
    using Anycmd.EDI;
    using Events;
    using InOuts;

    public class OntologyOrganizationAddedEvent : DomainEvent
    {
        #region Ctor
        public OntologyOrganizationAddedEvent(OntologyOrganizationBase source, IOntologyOrganizationCreateIO output)
            : base(source)
        {
            if (output == null)
            {
                throw new System.ArgumentNullException("output");
            }
            this.Output = output;
        }
        #endregion

        public IOntologyOrganizationCreateIO Output { get; private set; }
    }
}

﻿
namespace Anycmd.Host.EDI.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class AddBatchCommand : AddEntityCommand<IBatchCreateIO>, ISysCommand
    {
        public AddBatchCommand(IBatchCreateIO input)
            : base(input)
        {

        }
    }
}

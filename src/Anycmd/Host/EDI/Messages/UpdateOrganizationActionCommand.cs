﻿
namespace Anycmd.Host.EDI.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class UpdateOrganizationActionCommand : UpdateEntityCommand<IOrganizationActionUpdateIO>, ISysCommand
    {
        public UpdateOrganizationActionCommand(IOrganizationActionUpdateIO input)
            : base(input)
        {

        }
    }
}

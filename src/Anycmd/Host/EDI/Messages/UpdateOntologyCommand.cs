﻿
namespace Anycmd.Host.EDI.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class UpdateOntologyCommand: UpdateEntityCommand<IOntologyUpdateIO>, ISysCommand
    {
        public UpdateOntologyCommand(IOntologyUpdateIO input)
            : base(input)
        {

        }
    }
}

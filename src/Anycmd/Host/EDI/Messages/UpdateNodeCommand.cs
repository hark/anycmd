﻿
namespace Anycmd.Host.EDI.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class UpdateNodeCommand : UpdateEntityCommand<INodeUpdateIO>, ISysCommand
    {
        public UpdateNodeCommand(INodeUpdateIO input)
            : base(input)
        {

        }
    }
}

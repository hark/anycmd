﻿
namespace Anycmd.Host.EDI.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class AddArchiveCommand : AddEntityCommand<IArchiveCreateIO>, ISysCommand
    {
        public AddArchiveCommand(IArchiveCreateIO input)
            : base(input)
        {

        }
    }
}

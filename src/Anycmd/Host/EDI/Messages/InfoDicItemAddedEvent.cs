﻿
namespace Anycmd.Host.EDI.Messages
{
    using Anycmd.EDI;
    using Events;
    using InOuts;

    /// <summary>
    /// 
    /// </summary>
    public class InfoDicItemAddedEvent : DomainEvent
    {
        #region Ctor
        public InfoDicItemAddedEvent(InfoDicItemBase source, IInfoDicItemCreateIO output)
            : base(source)
        {
            if (output == null)
            {
                throw new System.ArgumentNullException("output");
            }
            this.Output = output;
        }
        #endregion

        public IInfoDicItemCreateIO Output { get; private set; }
    }
}

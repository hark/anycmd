﻿
namespace Anycmd.Host.EDI.Messages
{
    using Anycmd.EDI;
    using Events;
    using InOuts;

    /// <summary>
    /// 
    /// </summary>
    public class NodeOntologyCareAddedEvent : DomainEvent
    {
        #region Ctor
        public NodeOntologyCareAddedEvent(NodeOntologyCareBase source, INodeOntologyCareCreateIO output)
            : base(source)
        {
            if (output == null)
            {
                throw new System.ArgumentNullException("output");
            }
            this.Output = output;
        }
        #endregion

        public INodeOntologyCareCreateIO Output { get; private set; }
    }
}

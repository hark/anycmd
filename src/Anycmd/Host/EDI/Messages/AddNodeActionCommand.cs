﻿
namespace Anycmd.Host.EDI.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class AddNodeActionCommand : AddEntityCommand<INodeActionCreateIO>, ISysCommand
    {
        public AddNodeActionCommand(INodeActionCreateIO input)
            : base(input)
        {

        }
    }
}

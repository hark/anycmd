﻿
namespace Anycmd.Host.EDI.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class AddNodeOntologyCareCommand: AddEntityCommand<INodeOntologyCareCreateIO>, ISysCommand
    {
        public AddNodeOntologyCareCommand(INodeOntologyCareCreateIO input)
            : base(input)
        {

        }
    }
}

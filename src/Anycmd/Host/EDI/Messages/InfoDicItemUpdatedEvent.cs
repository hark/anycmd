﻿
namespace Anycmd.Host.EDI.Messages
{
    using Anycmd.EDI;
    using Events;
    using InOuts;

    /// <summary>
    /// 
    /// </summary>
    public class InfoDicItemUpdatedEvent : DomainEvent
    {
        #region Ctor
        public InfoDicItemUpdatedEvent(InfoDicItemBase source, IInfoDicItemUpdateIO output)
            : base(source)
        {
            if (output == null)
            {
                throw new System.ArgumentNullException("output");
            }
            this.Output = output;
        }
        #endregion

        public IInfoDicItemUpdateIO Output { get; private set; }
    }
}

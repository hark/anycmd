﻿
namespace Anycmd.Host.EDI.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class UpdateElementCommand : UpdateEntityCommand<IElementUpdateIO>, ISysCommand
    {
        public UpdateElementCommand(IElementUpdateIO input)
            : base(input)
        {

        }
    }
}

﻿
namespace Anycmd.Host.EDI.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class AddNodeCommand : AddEntityCommand<INodeCreateIO>, ISysCommand
    {
        public AddNodeCommand(INodeCreateIO input)
            : base(input)
        {

        }
    }
}

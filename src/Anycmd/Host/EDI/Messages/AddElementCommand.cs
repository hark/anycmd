﻿
namespace Anycmd.Host.EDI.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class AddElementCommand : AddEntityCommand<IElementCreateIO>, ISysCommand
    {
        public AddElementCommand(IElementCreateIO input)
            : base(input)
        {

        }
    }
}

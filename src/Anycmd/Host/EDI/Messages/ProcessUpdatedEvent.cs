﻿
namespace Anycmd.Host.EDI.Messages
{
    using Anycmd.EDI;
    using Events;

    public class ProcessUpdatedEvent : DomainEvent {
        #region Ctor
        public ProcessUpdatedEvent(ProcessBase source)
            : base(source) {
        }
        #endregion
    }
}

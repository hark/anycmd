﻿
namespace Anycmd.Host.EDI.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class AddInfoDicItemCommand : AddEntityCommand<IInfoDicItemCreateIO>, ISysCommand
    {
        public AddInfoDicItemCommand(IInfoDicItemCreateIO input)
            : base(input)
        {

        }
    }
}

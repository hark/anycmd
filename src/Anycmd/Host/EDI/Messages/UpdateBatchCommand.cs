﻿
namespace Anycmd.Host.EDI.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class UpdateBatchCommand : UpdateEntityCommand<IBatchUpdateIO>, ISysCommand
    {
        public UpdateBatchCommand(IBatchUpdateIO input)
            : base(input)
        {

        }
    }
}

﻿
namespace Anycmd.Host.EDI.Messages
{
    using Anycmd.EDI;
    using Events;

    public class NodeOrganizationActionAddedEvent : DomainEvent {
        #region Ctor
        public NodeOrganizationActionAddedEvent(NodeOrganizationAction source) : base(source) { }
        #endregion
    }
}

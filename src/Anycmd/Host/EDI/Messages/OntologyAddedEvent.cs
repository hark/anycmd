﻿
namespace Anycmd.Host.EDI.Messages
{
    using Anycmd.EDI;
    using Events;
    using InOuts;

    /// <summary>
    /// 
    /// </summary>
    public class OntologyAddedEvent : DomainEvent
    {
        #region Ctor
        public OntologyAddedEvent(OntologyBase source, IOntologyCreateIO output)
            : base(source)
        {
            if (output == null)
            {
                throw new System.ArgumentNullException("output");
            }
            this.Output = output;
        }
        #endregion

        public IOntologyCreateIO Output { get; private set; }
    }
}

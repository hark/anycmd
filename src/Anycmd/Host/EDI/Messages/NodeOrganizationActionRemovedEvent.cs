﻿
namespace Anycmd.Host.EDI.Messages
{
    using Anycmd.EDI;
    using Events;

    public class NodeOrganizationActionRemovedEvent : DomainEvent {
        #region Ctor
        public NodeOrganizationActionRemovedEvent(NodeOrganizationAction source) : base(source) { }
        #endregion
    }
}

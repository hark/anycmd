﻿
namespace Anycmd.Host.EDI.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class AddNodeElementCareCommand : AddEntityCommand<INodeElementCareCreateIO>, ISysCommand
    {
        public AddNodeElementCareCommand(INodeElementCareCreateIO input)
            : base(input)
        {

        }
    }
}

﻿
namespace Anycmd.Host.EDI
{
    using Anycmd.EDI;
    using Model;
    using System;

    public sealed class NodeOntologyCareState : StateObject<NodeOntologyCareState>, INodeOntologyCare, IStateObject
    {
        private NodeOntologyCareState() { }

        public static NodeOntologyCareState Create(INodeOntologyCare nodeOntologyCare)
        {
            if (nodeOntologyCare == null)
            {
                throw new ArgumentNullException("nodeOntologyCare");
            }
            return new NodeOntologyCareState
            {
                CreateOn = nodeOntologyCare.CreateOn,
                Id = nodeOntologyCare.Id,
                NodeID = nodeOntologyCare.NodeID,
                OntologyID = nodeOntologyCare.OntologyID
            };
        }

        public Guid NodeID { get; private set; }

        public Guid OntologyID { get; private set; }

        public DateTime? CreateOn { get; private set; }

        protected override bool DoEquals(NodeOntologyCareState other)
        {
            return
                Id == other.Id &&
                NodeID == other.NodeID &&
                OntologyID == other.OntologyID;
        }
    }
}

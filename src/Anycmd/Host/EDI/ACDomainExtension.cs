﻿
namespace Anycmd.Host.EDI
{
    using Messages;
    using System;
    using InOuts;

    public static class ACDomainExtension
    {
        #region EDI
        public static void AddArchive(this IACDomain host, IArchiveCreateIO input)
        {
            host.Handle(new AddArchiveCommand(input));
        }
        public static void UpdateArchive(this IACDomain host, IArchiveUpdateIO input)
        {
            host.Handle(new UpdateArchiveCommand(input));
        }
        public static void RemoveArchive(this IACDomain host, Guid archiveID)
        {
            host.Handle(new RemoveArchiveCommand(archiveID));
        }

        public static void AddBatch(this IACDomain host, IBatchCreateIO input)
        {
            host.Handle(new AddBatchCommand(input));
        }
        public static void UpdateBatch(this IACDomain host, IBatchUpdateIO input)
        {
            host.Handle(new UpdateBatchCommand(input));
        }
        public static void RemoveBatch(this IACDomain host, Guid batchID)
        {
            host.Handle(new RemoveBatchCommand(batchID));
        }

        public static void AddElement(this IACDomain host, IElementCreateIO input)
        {
            host.Handle(new AddElementCommand(input));
        }
        public static void UpdateElement(this IACDomain host, IElementUpdateIO input)
        {
            host.Handle(new UpdateElementCommand(input));
        }
        public static void RemoveElement(this IACDomain host, Guid elementID)
        {
            host.Handle(new RemoveElementCommand(elementID));
        }

        public static void AddInfoDic(this IACDomain host, IInfoDicCreateIO input)
        {
            host.Handle(new AddInfoDicCommand(input));
        }
        public static void UpdateInfoDic(this IACDomain host, IInfoDicUpdateIO input)
        {
            host.Handle(new UpdateInfoDicCommand(input));
        }
        public static void RemoveInfoDic(this IACDomain host, Guid infoDicID)
        {
            host.Handle(new RemoveInfoDicCommand(infoDicID));
        }

        public static void AddInfoDicItem(this IACDomain host, IInfoDicItemCreateIO input)
        {
            host.Handle(new AddInfoDicItemCommand(input));
        }
        public static void UpdateInfoDicItem(this IACDomain host, IInfoDicItemUpdateIO input)
        {
            host.Handle(new UpdateInfoDicItemCommand(input));
        }
        public static void RemoveInfoDicItem(this IACDomain host, Guid infoDicItemID)
        {
            host.Handle(new RemoveInfoDicItemCommand(infoDicItemID));
        }

        public static void AddNode(this IACDomain host, INodeCreateIO input)
        {
            host.Handle(new AddNodeCommand(input));
        }
        public static void UpdateNode(this IACDomain host, INodeUpdateIO input)
        {
            host.Handle(new UpdateNodeCommand(input));
        }
        public static void RemoveNode(this IACDomain host, Guid nodeID)
        {
            host.Handle(new RemoveNodeCommand(nodeID));
        }

        public static void RemoveNodeOntologyCare(this IACDomain host, Guid nodeOntologyCareID)
        {
            host.Handle(new RemoveNodeOntologyCareCommand(nodeOntologyCareID));
        }

        public static void AddNodeOntologyCare(this IACDomain host, INodeOntologyCareCreateIO input)
        {
            host.Handle(new AddNodeOntologyCareCommand(input));
        }

        public static void RemoveNodeElementCare(this IACDomain host, Guid nodeElementCareID)
        {
            host.Handle(new RemoveNodeElementCareCommand(nodeElementCareID));
        }

        public static void AddNodeElementCare(this IACDomain host, INodeElementCareCreateIO input)
        {
            host.Handle(new AddNodeElementCareCommand(input));
        }

        public static void AddOntology(this IACDomain host, IOntologyCreateIO input)
        {
            host.Handle(new AddOntologyCommand(input));
        }
        public static void UpdateOntology(this IACDomain host, IOntologyUpdateIO input)
        {
            host.Handle(new UpdateOntologyCommand(input));
        }
        public static void RemoveOntology(this IACDomain host, Guid ontologyID)
        {
            host.Handle(new RemoveOntologyCommand(ontologyID));
        }

        public static void AddOntologyOrganization(this IACDomain host, IOntologyOrganizationCreateIO input)
        {
            host.Handle(new AddOntologyOrganizationCommand(input));
        }

        public static void RemoveOntologyOrganization(this IACDomain host, Guid ontologyID, Guid organizationID)
        {
            host.Handle(new RemoveOntologyOrganizationCommand(ontologyID, organizationID));
        }

        public static void AddInfoGroup(this IACDomain host, IInfoGroupCreateIO input)
        {
            host.Handle(new AddInfoGroupCommand(input));
        }

        public static void UpdateInfoGroup(this IACDomain host, IInfoGroupUpdateIO input)
        {
            host.Handle(new UpdateInfoGroupCommand(input));
        }

        public static void RemoveInfoGroup(this IACDomain host, Guid infoGroupID)
        {
            host.Handle(new RemoveInfoGroupCommand(infoGroupID));
        }

        public static void AddAction(this IACDomain host, IActionCreateIO input)
        {
            host.Handle(new AddActionCommand(input));
        }

        public static void UpdateAction(this IACDomain host, IActionUpdateIO input)
        {
            host.Handle(new UpdateActionCommand(input));
        }

        public static void RemoveAction(this IACDomain host, Guid actionID)
        {
            host.Handle(new RemoveActionCommand(actionID));
        }

        public static void AddTopic(this IACDomain host, ITopicCreateIO input)
        {
            host.Handle(new AddTopicCommand(input));
        }

        public static void UpdateTopic(this IACDomain host, ITopicUpdateIO input)
        {
            host.Handle(new UpdateTopicCommand(input));
        }

        public static void RemoveTopic(this IACDomain host, Guid topicID)
        {
            host.Handle(new RemoveTopicCommand(topicID));
        }

        public static void AddProcess(this IACDomain host, IProcessCreateIO input)
        {
            host.Handle(new AddProcessCommand(input));
        }

        public static void UpdateProcess(this IACDomain host, IProcessUpdateIO input)
        {
            host.Handle(new UpdateProcessCommand(input));
        }
        #endregion
    }
}

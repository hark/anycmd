﻿
namespace Anycmd.Host.EDI
{
    using Handlers;
    using Hecp;
    using MemorySets.Impl;

    public class DefaultNodeHost : NodeHost
    {
        public DefaultNodeHost(IACDomain host)
        {
            this.StateCodes = new StateCodes();
            this.HecpHandler = new HecpHandler();
            this.MessageProducer = new DefaultMessageProducer();
            this.Ontologies = new OntologySet(host);
            this.Processs = new ProcesseSet(host);
            this.Nodes = new NodeSet(host);
            this.InfoDics = new InfoDicSet(host);
            this.InfoStringConverters = new InfoStringConverterSet(host);
            this.InfoRules = new InfoRuleSet(host);
            this.MessageProviders = new MessageProviderSet(host);
            this.EntityProviders = new EntityProviderSet(host);
            this.Transfers = new MessageTransferSet(host);
        }
    }
}

﻿using System;

namespace Anycmd.Host.EDI.InOuts
{
    using Model;

    public interface IActionUpdateIO : IEntityUpdateInput
    {
        string Description { get; }
        string IsAllowed { get; }
        string IsAudit { get; }
        bool IsPersist { get; }
        string Name { get; }
        int SortCode { get; }
        string Verb { get; }
    }
}

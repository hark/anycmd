﻿
namespace Anycmd.Host.EDI.InOuts
{
    using Model;
    using System;

    public interface INodeOntologyOrganizationCreateIO : IEntityCreateInput
    {
        Guid NodeID { get; }
        Guid OntologyID { get; }
        Guid OrganizationID { get; }
    }
}

﻿using System;

namespace Anycmd.Host.EDI.InOuts
{
    using Model;

    public interface IBatchCreateIO : IEntityCreateInput
    {
        string Description { get; }
        bool? IncludeDescendants { get; }
        Guid NodeID { get; }
        Guid OntologyID { get; }
        string OrganizationCode { get; }
        string Title { get; }
        string Type { get; }
    }
}

﻿using System;

namespace Anycmd.Host.EDI.InOuts
{
    using Model;

    public interface INodeElementCareCreateIO : IEntityCreateInput
    {
        Guid ElementID { get; }
        Guid NodeID { get; }
        bool IsInfoIDItem { get; }
    }
}

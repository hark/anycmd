﻿using Anycmd.Model;
using System;

namespace Anycmd.Host.EDI.InOuts
{
    public interface IOntologyOrganizationCreateIO : IEntityCreateInput
    {
        /// <summary>
        /// 
        /// </summary>
        Guid OntologyID { get; }
        /// <summary>
        /// 
        /// </summary>
        Guid OrganizationID { get; }
    }
}

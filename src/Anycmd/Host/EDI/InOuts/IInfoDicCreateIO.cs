﻿
namespace Anycmd.Host.EDI.InOuts
{
    using Model;

    public interface IInfoDicCreateIO : IEntityCreateInput
    {
        string Code { get; }
        string Description { get; }
        int IsEnabled { get; }
        string Name { get; }
        int SortCode { get; }
    }
}

﻿using System;

namespace Anycmd.Host.EDI.InOuts
{
    using Model;

    public interface IInfoGroupUpdateIO : IEntityUpdateInput
    {
        string Code { get; }
        string Description { get; }
        string Name { get; }
        int SortCode { get; }
    }
}

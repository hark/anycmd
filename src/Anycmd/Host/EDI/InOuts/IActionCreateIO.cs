﻿
namespace Anycmd.Host.EDI.InOuts
{
    using Model;
    using System;

    public interface IActionCreateIO : IEntityCreateInput
    {
        string Description { get; }
        string IsAllowed { get; }
        string IsAudit { get; }
        bool IsPersist { get; }
        string Name { get; }
        Guid OntologyID { get; }
        int SortCode { get; }
        string Verb { get; }
    }
}

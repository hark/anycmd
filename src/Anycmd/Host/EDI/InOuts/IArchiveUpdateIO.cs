﻿using System;

namespace Anycmd.Host.EDI.InOuts
{
    using Model;

    public interface IArchiveUpdateIO : IEntityUpdateInput
    {
        string Description { get; }
        string Title { get; }
    }
}

﻿using System;

namespace Anycmd.Host.EDI.InOuts
{
    using Model;

    public interface IArchiveCreateIO : IEntityCreateInput
    {
        string Description { get; }
        Guid OntologyID { get; }
        string RdbmsType { get; }
        string Title { get; }
    }
}

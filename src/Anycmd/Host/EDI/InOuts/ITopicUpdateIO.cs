﻿using System;

namespace Anycmd.Host.EDI.InOuts
{
    using Model;

    public interface ITopicUpdateIO : IEntityUpdateInput
    {
        string Code { get; }
        string Description { get; }
        string Name { get; }
    }
}

﻿using Anycmd.Model;

namespace Anycmd.Host.EDI.InOuts
{
    public interface INodeActionUpdateIO : IEntityUpdateInput
    {
    }
}

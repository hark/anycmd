﻿using System;

namespace Anycmd.Host.EDI.InOuts
{
    using Model;

    public interface IInfoGroupCreateIO : IEntityCreateInput
    {
        string Code { get; }
        string Description { get; }
        string Name { get; }
        Guid OntologyID { get; }
        int SortCode { get; }
    }
}

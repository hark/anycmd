﻿using System;

namespace Anycmd.Host.EDI.InOuts
{
    using Model;

    public interface ITopicCreateIO : IEntityCreateInput
    {
        string Code { get; }
        string Description { get; }
        string Name { get; }
        bool IsAllowed { get; }
        Guid OntologyID { get; }
    }
}

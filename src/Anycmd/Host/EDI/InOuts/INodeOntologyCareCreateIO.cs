﻿using System;

namespace Anycmd.Host.EDI.InOuts
{
    using Model;

    public interface INodeOntologyCareCreateIO : IEntityCreateInput
    {
        Guid NodeID { get; }
        Guid OntologyID { get; }
    }
}

﻿using System;

namespace Anycmd.Host.EDI.InOuts
{
    using Model;

    public interface IProcessCreateIO : IEntityCreateInput
    {
        string Type { get; }

        string Name { get; }

        int NetPort { get; }

        int IsEnabled { get; }

        Guid OntologyID { get; }

        string OrganizationCode { get; }

        string Description { get; }
    }
}

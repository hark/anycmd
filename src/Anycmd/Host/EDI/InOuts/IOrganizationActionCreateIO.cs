﻿using Anycmd.Model;

namespace Anycmd.Host.EDI.InOuts
{
    public interface IOrganizationActionCreateIO : IEntityCreateInput
    {
    }
}

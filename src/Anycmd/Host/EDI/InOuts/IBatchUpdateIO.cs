﻿using System;

namespace Anycmd.Host.EDI.InOuts
{
    using Model;

    public interface IBatchUpdateIO : IEntityUpdateInput
    {
        string Description { get; set; }
        string Title { get; set; }
    }
}

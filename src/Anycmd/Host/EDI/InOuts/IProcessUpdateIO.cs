﻿using System;

namespace Anycmd.Host.EDI.InOuts
{
    using Model;

    public interface IProcessUpdateIO : IEntityUpdateInput
    {
        string Name { get; }

        int IsEnabled { get; }
    }
}

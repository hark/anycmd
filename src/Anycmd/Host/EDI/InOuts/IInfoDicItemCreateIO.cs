﻿using System;

namespace Anycmd.Host.EDI.InOuts
{
    using Model;

    public interface IInfoDicItemCreateIO : IEntityCreateInput
    {
        string Code { get; }
        string Description { get; }
        Guid InfoDicID { get; }
        int IsEnabled { get; }
        string Level { get; }
        string Name { get; }
        int SortCode { get; }
    }
}

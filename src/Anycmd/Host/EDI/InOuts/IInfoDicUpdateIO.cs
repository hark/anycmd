﻿using System;

namespace Anycmd.Host.EDI.InOuts
{
    using Model;

    public interface IInfoDicUpdateIO : IEntityUpdateInput
    {
        string Code { get; }
        string Description { get; }
        int IsEnabled { get; }
        string Name { get; }
        int SortCode { get; }
    }
}

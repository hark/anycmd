﻿using System;

namespace Anycmd.Host.EDI
{
    using Anycmd.EDI;
    using Exceptions;
    using Model;

    public sealed class TopicState : StateObject<TopicState>, ITopic, IStateObject
    {
        private Guid _ontologyID;
        private readonly IACDomain host;

        private TopicState(IACDomain host)
        {
            this.host = host;
        }

        public static TopicState Create(IACDomain host, ITopic topic)
        {
            if (topic == null)
            {
                throw new ArgumentNullException("topic");
            }
            return new TopicState(host)
            {
                Code = topic.Code,
                CreateOn = topic.CreateOn,
                Description = topic.Description,
                Id = topic.Id,
                IsAllowed = topic.IsAllowed,
                Name = topic.Name,
                OntologyID = topic.OntologyID
            };
        }

        public Guid OntologyID
        {
            get { return _ontologyID; }
            private set
            {
                OntologyDescriptor ontology;
                if (!host.NodeHost.Ontologies.TryGetOntology(value, out ontology))
                {
                    throw new ValidationException("意外的本体标识" + value);
                }
                _ontologyID = value;
            }
        }

        public string Code { get; private set; }

        public string Name { get; private set; }

        public bool IsAllowed { get; private set; }

        public string Description { get; private set; }

        public DateTime? CreateOn { get; private set; }

        protected override bool DoEquals(TopicState other)
        {
            return
                Id == other.Id &&
                OntologyID == other.OntologyID &&
                Code == other.Code &&
                Name == other.Name &&
                IsAllowed == other.IsAllowed &&
                Description == other.Description;
        }
    }
}

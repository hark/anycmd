﻿
namespace Anycmd.Host.EDI
{
    using Anycmd.EDI;
    using Model;
    using System;

    public sealed class OntologyState : StateObject<OntologyState>, IOntology, IStateObject
    {
        private OntologyState() { }

        public static OntologyState Create(IOntology ontology)
        {
            if (ontology == null)
            {
                throw new ArgumentNullException("ontology");
            }
            return new OntologyState
            {
                CanAction = ontology.CanAction,
                CanCommand = ontology.CanCommand,
                CanEvent = ontology.CanEvent,
                Code = ontology.Code,
                CreateOn = ontology.CreateOn,
                DispatcherLoadCount = ontology.DispatcherLoadCount,
                DispatcherSleepTimeSpan = ontology.DispatcherSleepTimeSpan,
                EntityDatabaseID = ontology.EntityDatabaseID,
                EntityProviderID = ontology.EntityProviderID,
                EntitySchemaName = ontology.EntitySchemaName,
                EntityTableName = ontology.EntityTableName,
                ExecutorLoadCount = ontology.ExecutorLoadCount,
                ExecutorSleepTimeSpan = ontology.ExecutorSleepTimeSpan,
                Icon = ontology.Icon,
                Id = ontology.Id,
                IsEnabled = ontology.IsEnabled,
                IsLogicalDeletionEntity = ontology.IsLogicalDeletionEntity,
                IsOrganizationalEntity = ontology.IsOrganizationalEntity,
                IsSystem = ontology.IsSystem,
                MessageDatabaseID = ontology.MessageDatabaseID,
                MessageProviderID = ontology.MessageProviderID,
                MessageSchemaName = ontology.MessageSchemaName,
                Name = ontology.Name,
                ReceivedMessageBufferSize = ontology.ReceivedMessageBufferSize,
                ServiceIsAlive = ontology.ServiceIsAlive,
                SortCode = ontology.SortCode,
                Triggers = ontology.Triggers
            };
        }

        public string Code { get; private set; }

        public string Name { get; private set; }

        public string Triggers { get; private set; }

        public string Icon { get; private set; }

        public bool ServiceIsAlive { get; private set; }

        public Guid MessageProviderID { get; private set; }

        public Guid EntityProviderID { get; private set; }

        public Guid EntityDatabaseID { get; private set; }

        public bool IsSystem { get; private set; }

        public bool IsOrganizationalEntity { get; private set; }

        public bool IsLogicalDeletionEntity { get; private set; }

        public Guid MessageDatabaseID { get; private set; }

        public int ReceivedMessageBufferSize { get; private set; }

        public string EntitySchemaName { get; private set; }

        public string MessageSchemaName { get; private set; }

        public string EntityTableName { get; private set; }

        public int ExecutorLoadCount { get; private set; }

        public int ExecutorSleepTimeSpan { get; private set; }

        public int DispatcherLoadCount { get; private set; }

        public int DispatcherSleepTimeSpan { get; private set; }

        public int IsEnabled { get; private set; }

        public bool CanAction { get; private set; }

        public bool CanCommand { get; private set; }

        public bool CanEvent { get; private set; }

        public int SortCode { get; private set; }

        public DateTime? CreateOn { get; private set; }

        protected override bool DoEquals(OntologyState other)
        {
            return Id == other.Id &&
                Code == other.Code &&
                Name == other.Name &&
                Triggers == other.Triggers &&
                Icon == other.Icon &&
                ServiceIsAlive == other.ServiceIsAlive &&
                MessageProviderID == other.MessageProviderID &&
                EntityProviderID == other.EntityProviderID &&
                EntityDatabaseID == other.EntityDatabaseID &&
                IsSystem == other.IsSystem &&
                IsOrganizationalEntity == other.IsOrganizationalEntity &&
                IsLogicalDeletionEntity == other.IsLogicalDeletionEntity &&
                MessageDatabaseID == other.MessageDatabaseID &&
                ReceivedMessageBufferSize == other.ReceivedMessageBufferSize &&
                EntitySchemaName == other.EntitySchemaName &&
                MessageSchemaName == other.MessageSchemaName &&
                EntityTableName == other.EntityTableName &&
                ExecutorLoadCount == other.ExecutorLoadCount &&
                ExecutorSleepTimeSpan == other.ExecutorSleepTimeSpan &&
                DispatcherLoadCount == other.DispatcherLoadCount &&
                DispatcherSleepTimeSpan == other.DispatcherSleepTimeSpan &&
                IsEnabled == other.IsEnabled &&
                CanAction == other.CanAction &&
                CanCommand == other.CanCommand &&
                CanEvent == other.CanEvent &&
                SortCode == other.SortCode;
        }
    }
}

﻿
namespace Anycmd.Host.EDI
{
    using Anycmd.EDI;
    using Model;
    using System;

    public sealed class InfoDicItemState : StateObject<InfoDicItemState>, IInfoDicItem, IStateObject
    {
        private InfoDicItemState() { }

        public static InfoDicItemState Create(IInfoDicItem infoDicItem)
        {
            if (infoDicItem == null)
            {
                throw new ArgumentNullException("infoDicItem");
            }
            return new InfoDicItemState
            {
                Code = infoDicItem.Code,
                CreateOn = infoDicItem.CreateOn,
                Description = infoDicItem.Description,
                Id = infoDicItem.Id,
                InfoDicID = infoDicItem.InfoDicID,
                IsEnabled = infoDicItem.IsEnabled,
                Level = infoDicItem.Level,
                ModifiedOn = infoDicItem.ModifiedOn,
                Name = infoDicItem.Name,
                SortCode = infoDicItem.SortCode
            };
        }

        public string Level { get; private set; }

        public string Code { get; private set; }

        public int IsEnabled { get; private set; }

        public Guid InfoDicID { get; private set; }

        public string Name { get; private set; }

        public int SortCode { get; private set; }

        public DateTime? CreateOn { get; private set; }

        public DateTime? ModifiedOn { get; private set; }

        public string Description { get; private set; }

        protected override bool DoEquals(InfoDicItemState other)
        {
            return
                Id == other.Id &&
                Level == other.Level &&
                Code == other.Code &&
                IsEnabled == other.IsEnabled &&
                InfoDicID == other.InfoDicID &&
                SortCode == other.SortCode &&
                Name == other.Name;
        }
    }
}

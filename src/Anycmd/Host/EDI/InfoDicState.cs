﻿
namespace Anycmd.Host.EDI
{
    using Anycmd.EDI;
    using Model;
    using System;

    public sealed class InfoDicState : StateObject<InfoDicState>, IInfoDic, IStateObject
    {
        private InfoDicState() { }

        public static InfoDicState Create(IACDomain host, IInfoDic infoDic)
        {
            if (infoDic == null)
            {
                throw new ArgumentNullException("infoDic");
            }
            return new InfoDicState
            {
                Host = host,
                Code = infoDic.Code,
                CreateOn = infoDic.CreateOn,
                Id = infoDic.Id,
                IsEnabled = infoDic.IsEnabled,
                Name = infoDic.Name,
                SortCode = infoDic.SortCode
            };
        }

        public IACDomain Host { get; private set; }

        public string Code { get; private set; }

        public int IsEnabled { get; private set; }

        public int SortCode { get; private set; }

        public string Name { get; private set; }

        public DateTime? CreateOn { get; private set; }

        protected override bool DoEquals(InfoDicState other)
        {
            return
                Id == other.Id &&
                Code == other.Code &&
                IsEnabled == other.IsEnabled &&
                SortCode == other.SortCode &&
                Name == other.Name;
        }
    }
}

﻿using System;

namespace Anycmd.Host.EDI
{
    using Info;
    using Model;

    public sealed class InfoRuleState : StateObject<InfoRuleState>, IStateObject
    {
        private InfoRuleState() { }

        public static InfoRuleState Create(InfoRuleEntityBase entity, IInfoRule infoRule)
        {
            return new InfoRuleState
            {
                Id = entity.Id,
                CreateOn = entity.CreateOn,
                IsEnabled = entity.IsEnabled,
                InfoRule = infoRule
            };
        }

        public int IsEnabled { get; private set; }

        public DateTime? CreateOn { get; private set; }

        public IInfoRule InfoRule { get; private set; }

        protected override bool DoEquals(InfoRuleState other)
        {
            return Id == other.Id &&
                IsEnabled == other.IsEnabled;
        }
    }
}

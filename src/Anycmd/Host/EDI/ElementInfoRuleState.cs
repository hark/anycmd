﻿
namespace Anycmd.Host.EDI
{
    using Anycmd.EDI;
    using Model;
    using System;

    public sealed class ElementInfoRuleState : StateObject<ElementInfoRuleState>, IElementInfoRule, IStateObject
    {
        private readonly IACDomain host;

        private ElementInfoRuleState(IACDomain host)
        {
            this.host = host;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="elementInfoRule"></param>
        /// <returns></returns>
        public static ElementInfoRuleState Create(IACDomain host, IElementInfoRule elementInfoRule)
        {
            InfoRuleState infoRule;
            if (!host.NodeHost.InfoRules.TryGetInfoRule(elementInfoRule.InfoRuleID, out infoRule))
            {
                throw new InvalidProgramException("请检测InfoRule的存在性");
            }
            return new ElementInfoRuleState(host)
            {
                CreateOn = elementInfoRule.CreateOn,
                ElementID = elementInfoRule.ElementID,
                Id = elementInfoRule.Id,
                InfoRuleID = elementInfoRule.InfoRuleID,
                IsEnabled = elementInfoRule.IsEnabled,
                SortCode = elementInfoRule.SortCode
            };
        }

        public Guid ElementID { get; private set; }

        public Guid InfoRuleID { get; private set; }

        public int IsEnabled { get; private set; }

        public int SortCode { get; private set; }

        public DateTime CreateOn { get; private set; }

        protected override bool DoEquals(ElementInfoRuleState other)
        {
            return
                Id == other.Id &&
                ElementID == other.ElementID &&
                InfoRuleID == other.InfoRuleID &&
                IsEnabled == other.IsEnabled &&
                SortCode == other.SortCode;
        }
    }
}

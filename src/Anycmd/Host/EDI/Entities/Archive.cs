
namespace Anycmd.Host.EDI.Entities
{
    using Anycmd.EDI;
    using InOuts;
    using Model;
    using System;

    /// <summary>
    /// 表示归档记录数据访问实体。
    /// </summary>
    public class Archive : ArchiveBase, IAggregateRoot
    {
        public Archive() { }

        public static Archive Create(IArchiveCreateIO input)
        {
            return new Archive
            {
                Id = input.Id.Value,
                ArchiveOn = DateTime.Now,
                DataSource = string.Empty,
                Description = input.Description,
                FilePath = string.Empty,
                Password = string.Empty,
                Title = input.Title,
                RdbmsType = input.RdbmsType,
                OntologyID = input.OntologyID,
                UserID = string.Empty
            };
        }

        public void Update(IArchiveUpdateIO input)
        {
            this.Description = input.Description;
            this.Title = input.Title;
        }
    }
}

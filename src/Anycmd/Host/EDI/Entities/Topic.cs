﻿
namespace Anycmd.Host.EDI.Entities
{
    using Anycmd.EDI;
    using Model;
    using InOuts;

    /// <summary>
    /// 表示话题数据访问实体。
    /// </summary>
    public class Topic : TopicBase, IAggregateRoot
    {
        public Topic() { }

        public static Topic Create(ITopicCreateIO input)
        {
            return new Topic
            {
                Code = input.Code,
                Id = input.Id.Value,
                Description = input.Description,
                IsAllowed = input.IsAllowed,
                Name = input.Name,
                OntologyID = input.OntologyID
            };
        }

        public void Update(ITopicUpdateIO input)
        {
            this.Code = input.Code;
            this.Name = input.Name;
            this.Description = input.Description;
        }
    }
}

﻿
namespace Anycmd.Host.EDI.Entities
{
    using Anycmd.EDI;
    using Model;
    using InOuts;

    /// <summary>
    /// 表示进程数据访问实体
    /// </summary>
    public class Process : ProcessBase, IAggregateRoot
    {
        public Process() { }

        public static Process Create(IProcessCreateIO input)
        {
            return new Process
            {
                Type = input.Type,
                Id = input.Id.Value,
                IsEnabled = input.IsEnabled,
                Description = input.Description,
                Name = input.Name,
                NetPort = input.NetPort,
                OntologyID = input.OntologyID,
                OrganizationCode = input.OrganizationCode
            };
        }

        public void Update(IProcessUpdateIO input)
        {
            this.Name = input.Name;
            this.IsEnabled = input.IsEnabled;
        }
    }
}

﻿
namespace Anycmd.Host.EDI.MemorySets
{
    using System.Collections.Generic;

    public interface IStateCodes : IEnumerable<StateCode>
    {
    }
}

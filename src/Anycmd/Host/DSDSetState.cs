﻿
namespace Anycmd.Host
{
    using Anycmd.AC;
    using Model;
    using System;

    /// <summary>
    /// 表示动态职责分离角色集业务实体。
    /// </summary>
    public sealed class DSDSetState : StateObject<DSDSetState>, IDSDSet, IStateObject
    {
        private DSDSetState() { }

        public static DSDSetState Create(DSDSetBase dsdSet)
        {
            return new DSDSetState
            {
                Id = dsdSet.Id,
                Name = dsdSet.Name,
                IsEnabled = dsdSet.IsEnabled,
                DSDCard = dsdSet.DSDCard,
                Description = dsdSet.Description,
                CreateOn = dsdSet.CreateOn
            };
        }

        public string Name { get; private set; }

        public int IsEnabled { get; private set; }

        public int DSDCard { get; private set; }

        public string Description { get; private set; }

        public DateTime? CreateOn { get; private set; }

        protected override bool DoEquals(DSDSetState other)
        {
            return Id == other.Id &&
                Name == other.Name &&
                DSDCard == other.DSDCard &&
                IsEnabled == other.IsEnabled &&
                Description == other.Description;
        }
    }
}

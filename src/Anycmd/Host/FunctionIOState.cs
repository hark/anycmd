﻿
namespace Anycmd.Host
{
    using Anycmd.AC.Infra;
    using Exceptions;
    using Model;
    using System;
    using Util;

    /// <summary>
    /// 表示过程的输入输出参数业务实体。
    /// </summary>
    public sealed class FunctionIOState : StateObject<FunctionIOState>, IStateObject
    {
        public static FunctionIOState Create(FunctionIOBase entity)
        {
            IODirection direction;
            if (!entity.Direction.TryParse(out direction))
            {
                throw new CoreException("意外的输入输出方向" + entity.Direction);
            }
            return new FunctionIOState
            {
                Id = entity.Id,
                Direction = direction,
                Code = entity.Code,
                Name = entity.Name,
                FunctionID = entity.FunctionID
            };
        }

        public Guid FunctionID { get; private set; }

        public IODirection Direction { get; private set; }

        public string Code { get; private set; }

        public string Name { get; private set; }

        protected override bool DoEquals(FunctionIOState other)
        {
            return FunctionID == other.FunctionID &&
                Direction == other.Direction &&
                Code == other.Code &&
                Name == other.Name;
        }
    }
}

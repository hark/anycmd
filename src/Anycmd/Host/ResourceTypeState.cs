﻿
namespace Anycmd.Host
{
    using Anycmd.AC.Infra;
    using Model;
    using System;
    using Util;

    /// <summary>
    /// 表示系统资源类型业务实体。
    /// </summary>
    public sealed class ResourceTypeState : StateObject<ResourceTypeState>, IResourceType, IStateObject
    {
        public static readonly ResourceTypeState Empty = new ResourceTypeState
        {
            AppSystemID = Guid.Empty,
            AllowDelete = 0,
            AllowEdit = 0,
            Code = string.Empty,
            CreateOn = SystemTime.MinDate,
            Icon = string.Empty,
            Id = Guid.Empty,
            Name = string.Empty,
            SortCode = 0
        };

        private ResourceTypeState() { }

        public static ResourceTypeState Create(ResourceTypeBase resource)
        {
            if (resource == null)
            {
                throw new ArgumentNullException("resource");
            }
            return new ResourceTypeState
            {
                Id = resource.Id,
                AppSystemID = resource.AppSystemID,
                Name = resource.Name,
                Code = resource.Code,
                Icon = resource.Icon,
                SortCode = resource.SortCode,
                CreateOn = resource.CreateOn,
                AllowDelete = resource.AllowDelete,
                AllowEdit = resource.AllowEdit
            };
        }

        public Guid AppSystemID { get; private set; }

        public string Name { get; private set; }

        public string Code { get; private set; }

        public string Icon { get; private set; }

        public int SortCode { get; private set; }

        public DateTime? CreateOn { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public int AllowEdit { get; private set; }
        /// <summary>
        /// 
        /// </summary>
        public int AllowDelete { get; private set; }

        protected override bool DoEquals(ResourceTypeState other)
        {
            return Id == other.Id &&
                AppSystemID == other.AppSystemID &&
                Name == other.Name &&
                Code == other.Code &&
                Icon == other.Icon &&
                SortCode == other.SortCode;
        }
    }
}

﻿
namespace Anycmd.Host.AC
{
    using Anycmd.AC;
    using Model;
    using InOuts;

    /// <summary>
    /// 表示i动态职责分离角色数据访问实体。
    /// </summary>
    public class DSDRole : DSDRoleBase, IAggregateRoot
    {
        public static DSDRole Create(IDSDRoleCreateIO input)
        {
            return new DSDRole
            {
                Id = input.Id.Value,
                RoleID = input.RoleID,
                DSDSetID = input.DSDSetID
            };
        }
    }
}

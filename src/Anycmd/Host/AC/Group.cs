﻿
namespace Anycmd.Host.AC
{
    using Anycmd.AC;
    using Model;
    using InOuts;

    /// <summary>
    /// 表示组数据访问实体。
    /// </summary>
    public class Group : GroupBase, IAggregateRoot, IGroup
    {
        public Group()
        {
            IsEnabled = 1;
        }

        public static Group Create(IGroupCreateIO input)
        {
            return new Group
            {
                Id = input.Id.Value,
                CategoryCode = input.CategoryCode,
                Description = input.Description,
                IsEnabled = input.IsEnabled,
                Name = input.Name,
                ShortName = input.ShortName,
                SortCode = input.SortCode,
                TypeCode = input.TypeCode
            };
        }

        public void Update(IGroupUpdateIO input)
        {
            this.CategoryCode = input.CategoryCode;
            this.Description = input.Description;
            this.IsEnabled = input.IsEnabled;
            this.Name = input.Name;
            this.ShortName = input.ShortName;
            this.SortCode = input.SortCode;
        }

        public static Group Create(IPositionCreateIO input)
        {
            return new Group
            {
                Id = input.Id.Value,
                CategoryCode = input.CategoryCode,
                Description = input.Description,
                IsEnabled = input.IsEnabled,
                Name = input.Name,
                ShortName = input.ShortName,
                SortCode = input.SortCode,
                TypeCode = "AC",
                OrganizationCode = input.OrganizationCode
            };
        }

        public void Update(IPositionUpdateIO input)
        {
            this.CategoryCode = input.CategoryCode;
            this.Description = input.Description;
            this.IsEnabled = input.IsEnabled;
            this.Name = input.Name;
            this.ShortName = input.ShortName;
            this.SortCode = input.SortCode;
        }
    }
}

﻿
namespace Anycmd.Host.AC.Infra
{
    using Anycmd.AC.Infra;
    using Model;
    using InOuts;

    /// <summary>
    /// 表示系统界面视图按钮数据访问实体，设计用于支持自动化界面等。将界面视图和按钮的关系视作实体。
    /// <remarks>
    /// 1该模型是程序开发模型，被程序员使用，用户不关心本概念;
    /// 2在数据库中没有任何表需要引用UIViewButton表所以UIViewButton无需标记删除
    /// </remarks>
    /// </summary>
    public class UIViewButton : UIViewButtonBase, IAggregateRoot
    {
        #region Ctor
        public UIViewButton()
        {
            IsEnabled = 1;
        }
        #endregion

        public static UIViewButton Create(IUIViewButtonCreateIO input)
        {
            return new UIViewButton
                {
                    Id = input.Id.Value,
                    ButtonID = input.ButtonID,
                    UIViewID = input.UIViewID,
                    FunctionID = input.FunctionID,
                    IsEnabled = input.IsEnabled
                };
        }

        public void Update(IUIViewButtonUpdateIO input)
        {
            this.IsEnabled = input.IsEnabled;
            this.FunctionID = input.FunctionID;
        }
    }
}

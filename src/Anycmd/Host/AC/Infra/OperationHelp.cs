﻿
namespace Anycmd.Host.AC.Infra
{
    using Anycmd.AC.Infra;
    using Model;

    /// <summary>
    /// 表示系统操作帮助数据访问实体。
    /// </summary>
    public class OperationHelp : OperationHelpBase, IAggregateRoot
    {
        public OperationHelp() { }
    }
}

﻿
namespace Anycmd.Host.AC.Infra
{
    using Anycmd.AC.Infra;
    using Model;
    using InOuts;

    /// <summary>
    /// 表示系统字典数据访问实体。
    /// </summary>
    public class Dic : DicBase, IAggregateRoot
    {
        public Dic()
        {
        }

        public static Dic Create(IDicCreateIO input)
        {
            return new Dic
                {
                    Id = input.Id.Value,
                    Code = input.Code,
                    Name = input.Name,
                    Description = input.Description,
                    SortCode = input.SortCode,
                    IsEnabled = input.IsEnabled
                };
        }

        public void Update(IDicUpdateIO input)
        {
            this.Code = input.Code;
            this.Description = input.Description;
            this.IsEnabled = input.IsEnabled;
            this.SortCode = input.SortCode;
            this.Name = input.Name;
        }
    }
}

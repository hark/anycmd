﻿
namespace Anycmd.Host.AC.Infra
{
    using Anycmd.AC.Infra;
    using Model;
    using InOuts;

    /// <summary>
    /// 表示系统界面视图数据访问实体。
    /// </summary>
    public class UIView : UIViewBase, IAggregateRoot
    {
        public UIView() { }

        public static UIView Create(IUIViewCreateIO input)
        {
            return new UIView
            {
                Id = input.Id.Value,
                Icon = input.Icon,
                Tooltip = input.Tooltip
            };
        }

        public void Update(IUIViewUpdateIO input)
        {
            this.Tooltip = input.Tooltip;
            this.Icon = input.Icon;
        }
    }
}

﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Anycmd.AC.Infra;
    using Model;
    using InOuts;

    /// <summary>
    /// 
    /// </summary>
    public class ButtonAddedEvent : EntityAddedEvent<IButtonCreateIO>
    {
        public ButtonAddedEvent(ButtonBase source, IButtonCreateIO input)
            : base(source, input)
        {
        }
    }
}
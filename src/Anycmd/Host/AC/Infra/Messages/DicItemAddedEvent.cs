﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Anycmd.AC.Infra;
    using Model;
    using InOuts;

    /// <summary>
    /// 
    /// </summary>
    public class DicItemAddedEvent : EntityAddedEvent<IDicItemCreateIO>
    {
        #region Ctor
        public DicItemAddedEvent(DicItemBase source, IDicItemCreateIO input)
            : base(source, input)
        {
        }
        #endregion
    }
}
﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Commands;
    using Model;
    using InOuts;


    public class AddDicCommand : AddEntityCommand<IDicCreateIO>, ISysCommand
    {
        public AddDicCommand(IDicCreateIO input)
            : base(input)
        {

        }
    }
}

﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using AC.InOuts;
    using Anycmd.AC.Infra;
    using Anycmd.Events;
    using System;

    /// <summary>
    /// 
    /// </summary>
    public class AppSystemAddedEvent : DomainEvent
    {
        public AppSystemAddedEvent(AppSystemBase source, IAppSystemCreateIO input)
            : base(source)
        {
            if (input == null)
            {
                throw new ArgumentNullException("input");
            }
            this.Input = input;
        }

        public IAppSystemCreateIO Input { get; private set; }
    }
}
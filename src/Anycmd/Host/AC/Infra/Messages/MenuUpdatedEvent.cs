﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using AC.InOuts;
    using Anycmd.AC.Infra;
    using Anycmd.Events;

    public class MenuUpdatedEvent : DomainEvent
    {
        #region Ctor
        public MenuUpdatedEvent(MenuBase source, IMenuUpdateIO input)
            : base(source)
        {
            if (input == null)
            {
                throw new System.ArgumentNullException("input");
            }
            this.Input = input;
        }
        #endregion

        public IMenuUpdateIO Input { get; private set; }
    }
}
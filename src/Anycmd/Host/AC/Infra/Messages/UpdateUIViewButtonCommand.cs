﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Commands;
    using Model;
    using InOuts;


    public class UpdateUIViewButtonCommand : UpdateEntityCommand<IUIViewButtonUpdateIO>, ISysCommand
    {
        public UpdateUIViewButtonCommand(IUIViewButtonUpdateIO input)
            : base(input)
        {

        }
    }
}

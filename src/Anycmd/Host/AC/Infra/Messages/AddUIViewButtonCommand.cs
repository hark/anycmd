﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Commands;
    using Model;
    using InOuts;


    public class AddUIViewButtonCommand : AddEntityCommand<IUIViewButtonCreateIO>, ISysCommand
    {
        public AddUIViewButtonCommand(IUIViewButtonCreateIO input)
            : base(input)
        {

        }
    }
}

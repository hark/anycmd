﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Commands;
    using Model;
    using InOuts;


    public class UpdateEntityTypeCommand : UpdateEntityCommand<IEntityTypeUpdateIO>, ISysCommand
    {
        public UpdateEntityTypeCommand(IEntityTypeUpdateIO input)
            : base(input)
        {

        }
    }
}

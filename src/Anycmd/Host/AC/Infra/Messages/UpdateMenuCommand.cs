﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class UpdateMenuCommand : UpdateEntityCommand<IMenuUpdateIO>, ISysCommand
    {
        public UpdateMenuCommand(IMenuUpdateIO input)
            : base(input)
        {

        }
    }
}

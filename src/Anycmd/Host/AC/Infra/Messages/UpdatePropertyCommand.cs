﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class UpdatePropertyCommand : UpdateEntityCommand<IPropertyUpdateIO>, ISysCommand
    {
        public UpdatePropertyCommand(IPropertyUpdateIO input)
            : base(input)
        {

        }
    }
}

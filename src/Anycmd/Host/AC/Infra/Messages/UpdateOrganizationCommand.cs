﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class UpdateOrganizationCommand : UpdateEntityCommand<IOrganizationUpdateIO>, ISysCommand
    {
        public UpdateOrganizationCommand(IOrganizationUpdateIO input)
            : base(input)
        {

        }
    }
}

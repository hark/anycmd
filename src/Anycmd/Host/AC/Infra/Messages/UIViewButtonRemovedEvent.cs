﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Anycmd.AC.Infra;
    using Anycmd.Events;

    /// <summary>
    /// 
    /// </summary>
    public class UIViewButtonRemovedEvent : DomainEvent
    {
        #region Ctor
        public UIViewButtonRemovedEvent(UIViewButtonBase source)
            : base(source)
        {
        }
        #endregion
    }
}

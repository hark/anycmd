﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using AC.InOuts;
    using Anycmd.AC.Infra;
    using Anycmd.Events;

    /// <summary>
    /// 
    /// </summary>
    public class OrganizationUpdatedEvent : DomainEvent
    {
        #region Ctor
        public OrganizationUpdatedEvent(OrganizationBase source, IOrganizationUpdateIO input)
            : base(source)
        {
            if (input == null)
            {
                throw new System.ArgumentNullException("input");
            }
            this.Input = input;
        }
        #endregion

        public IOrganizationUpdateIO Input { get; private set; }
    }
}

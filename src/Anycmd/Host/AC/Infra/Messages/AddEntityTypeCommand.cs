﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Commands;
    using Model;
    using InOuts;


    public class AddEntityTypeCommand : AddEntityCommand<IEntityTypeCreateIO>, ISysCommand
    {
        public AddEntityTypeCommand(IEntityTypeCreateIO input)
            : base(input)
        {

        }
    }
}

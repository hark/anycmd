﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class AddAppSystemCommand : AddEntityCommand<IAppSystemCreateIO>, ISysCommand
    {
        public AddAppSystemCommand(IAppSystemCreateIO input)
            : base(input)
        {

        }
    }
}

﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class UpdateResourceCommand : UpdateEntityCommand<IResourceTypeUpdateIO>, ISysCommand
    {
        public UpdateResourceCommand(IResourceTypeUpdateIO input)
            : base(input)
        {

        }
    }
}

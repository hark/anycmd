﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using AC.InOuts;
    using Anycmd.AC.Infra;
    using Anycmd.Events;
    using System;

    /// <summary>
    /// 
    /// </summary>
    public class AppSystemUpdatedEvent : DomainEvent
    {
        public AppSystemUpdatedEvent(AppSystemBase source, IAppSystemUpdateIO input)
            : base(source)
        {
            if (input == null)
            {
                throw new ArgumentNullException("input");
            }
            this.Input = input;
        }

        public IAppSystemUpdateIO Input { get; private set; }
    }
}
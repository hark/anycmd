﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Anycmd.AC.Infra;
    using Model;
    using InOuts;

    /// <summary>
    /// 
    /// </summary>
    public class PropertyAddedEvent : EntityAddedEvent<IPropertyCreateIO>
    {
        #region Ctor
        public PropertyAddedEvent(PropertyBase source, IPropertyCreateIO input)
            : base(source, input)
        {
        }
        #endregion
    }
}

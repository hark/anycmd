﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Commands;
    using Model;
    using InOuts;


    public class UpdateFunctionCommand : UpdateEntityCommand<IFunctionUpdateIO>, ISysCommand
    {
        public UpdateFunctionCommand(IFunctionUpdateIO input)
            : base(input)
        {

        }
    }
}

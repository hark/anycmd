﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Anycmd.AC.Infra;
    using Model;
    using InOuts;

    /// <summary>
    /// 
    /// </summary>
    public class OrganizationAddedEvent : EntityAddedEvent<IOrganizationCreateIO>
    {
        #region Ctor
        public OrganizationAddedEvent(OrganizationBase source, IOrganizationCreateIO input)
            : base(source, input)
        {
        }
        #endregion
    }
}

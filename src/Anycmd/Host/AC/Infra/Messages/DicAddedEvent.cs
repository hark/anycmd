﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Anycmd.AC.Infra;
    using Model;
    using InOuts;

    /// <summary>
    /// 
    /// </summary>
    public class DicAddedEvent : EntityAddedEvent<IDicCreateIO>
    {
        #region Ctor
        public DicAddedEvent(DicBase source, IDicCreateIO input)
            : base(source, input)
        {
        }
        #endregion
    }
}
﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Anycmd.AC.Infra;
    using Model;
    using InOuts;

    /// <summary>
    /// 
    /// </summary>
    public class UIViewAddedEvent : EntityAddedEvent<IUIViewCreateIO>
    {
        #region Ctor
        public UIViewAddedEvent(UIViewBase source, IUIViewCreateIO input)
            : base(source, input)
        {
        }
        #endregion
    }
}

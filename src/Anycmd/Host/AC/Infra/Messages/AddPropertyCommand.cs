﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Commands;
    using Model;
    using InOuts;


    public class AddPropertyCommand : AddEntityCommand<IPropertyCreateIO>, ISysCommand
    {
        public AddPropertyCommand(IPropertyCreateIO input)
            : base(input)
        {

        }
    }
}

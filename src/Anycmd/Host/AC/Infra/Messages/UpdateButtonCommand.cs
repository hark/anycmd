﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Commands;
    using Model;
    using InOuts;


    public class UpdateButtonCommand : UpdateEntityCommand<IButtonUpdateIO>, ISysCommand
    {
        public UpdateButtonCommand(IButtonUpdateIO input)
            : base(input)
        {

        }
    }
}

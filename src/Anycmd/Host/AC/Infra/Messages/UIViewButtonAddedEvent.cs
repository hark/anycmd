﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Anycmd.AC.Infra;
    using Model;
    using InOuts;

    /// <summary>
    /// 
    /// </summary>
    public class UIViewButtonAddedEvent : EntityAddedEvent<IUIViewButtonCreateIO>
    {
        #region Ctor
        public UIViewButtonAddedEvent(UIViewButtonBase source, IUIViewButtonCreateIO input)
            : base(source, input)
        {
        }
        #endregion
    }
}

﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Anycmd.AC.Infra;
    using Anycmd.Events;

    /// <summary>
    /// 
    /// </summary>
    public class UIViewRemovedEvent : DomainEvent
    {
        #region Ctor
        public UIViewRemovedEvent(UIViewBase source)
            : base(source)
        {
        }
        #endregion
    }
}

﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Commands;
    using Model;
    using InOuts;


    public class AddButtonCommand : AddEntityCommand<IButtonCreateIO>, ISysCommand
    {
        public AddButtonCommand(IButtonCreateIO input)
            : base(input)
        {

        }
    }
}

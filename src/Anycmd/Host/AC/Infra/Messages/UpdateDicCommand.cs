﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Commands;
    using Model;
    using InOuts;


    public class UpdateDicCommand : UpdateEntityCommand<IDicUpdateIO>, ISysCommand
    {
        public UpdateDicCommand(IDicUpdateIO input)
            : base(input)
        {

        }
    }
}

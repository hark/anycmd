﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class UpdateAppSystemCommand : UpdateEntityCommand<IAppSystemUpdateIO>, ISysCommand
    {
        public UpdateAppSystemCommand(IAppSystemUpdateIO input)
            : base(input)
        {

        }
    }
}

﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Anycmd.AC.Infra;
    using Model;
    using InOuts;

    public class MenuAddedEvent : EntityAddedEvent<IMenuCreateIO>
    {
        #region Ctor
        public MenuAddedEvent(MenuBase source, IMenuCreateIO input)
            : base(source, input)
        {
        }
        #endregion
    }
}
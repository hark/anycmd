﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Commands;
    using Model;
    using InOuts;


    public class AddOrganizationCommand : AddEntityCommand<IOrganizationCreateIO>, ISysCommand
    {
        public AddOrganizationCommand(IOrganizationCreateIO input)
            : base(input)
        {

        }
    }
}

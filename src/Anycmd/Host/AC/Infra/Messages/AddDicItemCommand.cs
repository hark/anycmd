﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Commands;
    using Model;
    using InOuts;


    public class AddDicItemCommand : AddEntityCommand<IDicItemCreateIO>, ISysCommand
    {
        public AddDicItemCommand(IDicItemCreateIO input)
            : base(input)
        {

        }
    }
}

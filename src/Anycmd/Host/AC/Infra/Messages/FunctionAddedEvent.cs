﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Anycmd.AC.Infra;
    using Model;
    using InOuts;

    /// <summary>
    /// 
    /// </summary>
    public class FunctionAddedEvent : EntityAddedEvent<IFunctionCreateIO>
    {
        #region Ctor
        public FunctionAddedEvent(FunctionBase source, IFunctionCreateIO input)
            : base(source, input)
        {
        }
        #endregion
    }
}

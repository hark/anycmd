﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class UpdateDicItemCommand : UpdateEntityCommand<IDicItemUpdateIO>, ISysCommand
    {
        public UpdateDicItemCommand(IDicItemUpdateIO input)
            : base(input)
        {

        }
    }
}

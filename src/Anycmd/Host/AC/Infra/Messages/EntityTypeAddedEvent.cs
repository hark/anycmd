﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Anycmd.AC.Infra;
    using Model;
    using InOuts;

    /// <summary>
    /// 
    /// </summary>
    public class EntityTypeAddedEvent : EntityAddedEvent<IEntityTypeCreateIO>
    {
        #region Ctor
        public EntityTypeAddedEvent(EntityTypeBase source, IEntityTypeCreateIO input)
            : base(source, input)
        {
        }
        #endregion
    }
}

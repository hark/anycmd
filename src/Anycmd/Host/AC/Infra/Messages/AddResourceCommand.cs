﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class AddResourceCommand : AddEntityCommand<IResourceTypeCreateIO>, ISysCommand
    {
        public AddResourceCommand(IResourceTypeCreateIO input)
            : base(input)
        {

        }
    }
}

﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Commands;
    using Model;
    using InOuts;


    public class AddFunctionCommand : AddEntityCommand<IFunctionCreateIO>, ISysCommand
    {
        public AddFunctionCommand(IFunctionCreateIO input)
            : base(input)
        {

        }
    }
}

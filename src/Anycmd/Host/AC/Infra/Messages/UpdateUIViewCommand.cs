﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class UpdateUIViewCommand : UpdateEntityCommand<IUIViewUpdateIO>, ISysCommand
    {
        public UpdateUIViewCommand(IUIViewUpdateIO input)
            : base(input)
        {

        }
    }
}

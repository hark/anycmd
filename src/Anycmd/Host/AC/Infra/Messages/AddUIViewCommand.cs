﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Commands;
    using Model;
    using InOuts;


    public class AddUIViewCommand : AddEntityCommand<IUIViewCreateIO>, ISysCommand
    {
        public AddUIViewCommand(IUIViewCreateIO input)
            : base(input)
        {

        }
    }
}

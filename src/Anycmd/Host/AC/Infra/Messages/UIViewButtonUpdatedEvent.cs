﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using AC.InOuts;
    using Anycmd.AC.Infra;
    using Anycmd.Events;

    /// <summary>
    /// 
    /// </summary>
    public class UIViewButtonUpdatedEvent : DomainEvent
    {
        #region Ctor
        public UIViewButtonUpdatedEvent(UIViewButtonBase source, IUIViewButtonUpdateIO input)
            : base(source)
        {
            if (input == null)
            {
                throw new System.ArgumentNullException("input");
            }
            this.Input = input;
        }
        #endregion

        public IUIViewButtonUpdateIO Input { get; private set; }
    }
}

﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using Commands;
    using Model;
    using InOuts;


    public class AddMenuCommand : AddEntityCommand<IMenuCreateIO>, ISysCommand
    {
        public AddMenuCommand(IMenuCreateIO input)
            : base(input)
        {

        }
    }
}

﻿
namespace Anycmd.Host.AC.Infra.Messages
{
    using AC.InOuts;
    using Anycmd.AC.Infra;
    using Anycmd.Events;

    /// <summary>
    /// 
    /// </summary>
    public class ResourceTypeAddedEvent : DomainEvent
    {
        #region Ctor
        public ResourceTypeAddedEvent(ResourceTypeBase source, IResourceTypeCreateIO input)
            : base(source)
        {
            if (input == null)
            {
                throw new System.ArgumentNullException("input");
            }
        }
        #endregion

        public IResourceTypeCreateIO input { get; private set; }
    }
}
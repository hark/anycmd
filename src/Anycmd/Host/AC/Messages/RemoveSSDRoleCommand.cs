﻿
namespace Anycmd.Host.AC.Messages
{
    using Commands;
    using Model;
    using System;

    public class RemoveSSDRoleCommand : RemoveEntityCommand, ISysCommand
    {
        public RemoveSSDRoleCommand(Guid ssdRoleID)
            : base(ssdRoleID)
        {

        }
    }
}

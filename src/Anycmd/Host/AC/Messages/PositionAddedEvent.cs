﻿
namespace Anycmd.Host.AC.Messages
{
    using Anycmd.AC;
    using Model;
    using InOuts;

    public class PositionAddedEvent : EntityAddedEvent<IPositionCreateIO>
    {
        #region Ctor
        public PositionAddedEvent(GroupBase source, IPositionCreateIO output)
            : base(source, output)
        {
        }
        #endregion
    }
}

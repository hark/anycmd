﻿
namespace Anycmd.Host.AC.Messages
{
    using AC.InOuts;
    using Anycmd.AC;
    using Events;

    public class GroupUpdatedEvent : DomainEvent
    {
        #region Ctor
        public GroupUpdatedEvent(GroupBase source, IGroupUpdateIO output)
            : base(source)
        {
            if (output == null)
            {
                throw new System.ArgumentNullException("output");
            }
            this.Output = output;
        }
        #endregion

        public IGroupUpdateIO Output { get; private set; }
    }
}
﻿
namespace Anycmd.Host.AC.Messages
{
    using Anycmd.AC;
    using Model;
    using InOuts;

    public class SSDSetAddedEvent: EntityAddedEvent<ISSDSetCreateIO>
    {
        #region Ctor
        public SSDSetAddedEvent(SSDSetBase source, ISSDSetCreateIO output)
            : base(source, output)
        {
        }
        #endregion
    }
}

﻿
namespace Anycmd.Host.AC.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class UpdateGroupCommand : UpdateEntityCommand<IGroupUpdateIO>, ISysCommand
    {
        public UpdateGroupCommand(IGroupUpdateIO input)
            : base(input)
        {

        }
    }
}

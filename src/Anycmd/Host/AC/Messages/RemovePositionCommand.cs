﻿
namespace Anycmd.Host.AC.Messages
{
    using Commands;
    using Model;
    using System;

    public class RemovePositionCommand: RemoveEntityCommand, ISysCommand
    {
        public RemovePositionCommand(Guid groupID)
            : base(groupID)
        {

        }
    }
}

﻿
namespace Anycmd.Host.AC.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class UpdateDSDSetCommand : UpdateEntityCommand<IDSDSetUpdateIO>, ISysCommand
    {
        public UpdateDSDSetCommand(IDSDSetUpdateIO input)
            : base(input)
        {

        }
    }
}

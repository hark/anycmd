﻿
namespace Anycmd.Host.AC.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class AddPrivilegeBigramCommand : AddEntityCommand<IPrivilegeBigramCreateIO>, ISysCommand
    {
        public AddPrivilegeBigramCommand(IPrivilegeBigramCreateIO input)
            : base(input)
        {

        }
    }
}

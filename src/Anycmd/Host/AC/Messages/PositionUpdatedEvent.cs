﻿
namespace Anycmd.Host.AC.Messages
{
    using Anycmd.AC;
    using Events;
    using InOuts;

    public class PositionUpdatedEvent : DomainEvent
    {
        #region Ctor
        public PositionUpdatedEvent(GroupBase source, IPositionUpdateIO output)
            : base(source)
        {
            if (output == null)
            {
                throw new System.ArgumentNullException("output");
            }
            this.Output = output;
        }
        #endregion

        public IPositionUpdateIO Output { get; private set; }
    }
}
﻿
namespace Anycmd.Host.AC.Messages
{
    using AC.InOuts;
    using Anycmd.AC;
    using Events;

    public class RoleUpdatedEvent : DomainEvent
    {
        #region Ctor
        public RoleUpdatedEvent(RoleBase source, IRoleUpdateIO output)
            : base(source)
        {
            if (output == null)
            {
                throw new System.ArgumentNullException("output");
            }
            this.Output = output;
        }
        #endregion

        public IRoleUpdateIO Output { get; private set; }
    }
}
﻿
namespace Anycmd.Host.AC.Messages
{
    using Anycmd.AC;
    using Model;
    using InOuts;

    public class SSDRoleAddedEvent : EntityAddedEvent<ISSDRoleCreateIO>
    {
        #region Ctor
        public SSDRoleAddedEvent(SSDRoleBase source, ISSDRoleCreateIO output)
            : base(source, output)
        {
        }
        #endregion
    }
}

﻿
namespace Anycmd.Host.AC.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class UpdatePrivilegeBigramCommand : UpdateEntityCommand<IPrivilegeBigramUpdateIO>, ISysCommand
    {
        public UpdatePrivilegeBigramCommand(IPrivilegeBigramUpdateIO input)
            : base(input)
        {

        }
    }
}

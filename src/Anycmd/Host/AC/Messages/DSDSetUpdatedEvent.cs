﻿
namespace Anycmd.Host.AC.Messages
{
    using Anycmd.AC;
    using Events;
    using InOuts;

    public class DSDSetUpdatedEvent : DomainEvent
    {
        #region Ctor
        public DSDSetUpdatedEvent(DSDSetBase source, IDSDSetUpdateIO output)
            : base(source)
        {
            if (output == null)
            {
                throw new System.ArgumentNullException("output");
            }
            this.Output = output;
        }
        #endregion

        public IDSDSetUpdateIO Output { get; private set; }
    }
}
﻿
namespace Anycmd.Host.AC.Messages
{
    using Anycmd.AC;
    using Model;
    using InOuts;

    public class RoleAddedEvent : EntityAddedEvent<IRoleCreateIO>
    {
        #region Ctor
        public RoleAddedEvent(RoleBase source, IRoleCreateIO output)
            : base(source, output)
        {
        }
        #endregion
    }
}

﻿
namespace Anycmd.Host.AC.Messages
{
    using Anycmd.AC;
    using Events;

    public class DSDRoleRemovedEvent : DomainEvent
    {
        #region Ctor
        public DSDRoleRemovedEvent(DSDRoleBase source)
            : base(source)
        {
        }
        #endregion
    }
}
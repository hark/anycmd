﻿
namespace Anycmd.Host.AC.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class AddDSDSetCommand : AddEntityCommand<IDSDSetCreateIO>, ISysCommand
    {
        public AddDSDSetCommand(IDSDSetCreateIO input)
            : base(input)
        {

        }
    }
}

﻿
namespace Anycmd.Host.AC.Messages
{
    using Anycmd.AC;
    using Model;
    using InOuts;

    public class DSDRoleAddedEvent : EntityAddedEvent<IDSDRoleCreateIO>
    {
        #region Ctor
        public DSDRoleAddedEvent(DSDRoleBase source, IDSDRoleCreateIO output)
            : base(source, output)
        {
        }
        #endregion
    }
}

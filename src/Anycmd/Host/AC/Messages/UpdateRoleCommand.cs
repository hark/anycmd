﻿
namespace Anycmd.Host.AC.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class UpdateRoleCommand : UpdateEntityCommand<IRoleUpdateIO>, ISysCommand
    {
        public UpdateRoleCommand(IRoleUpdateIO input)
            : base(input)
        {

        }
    }
}

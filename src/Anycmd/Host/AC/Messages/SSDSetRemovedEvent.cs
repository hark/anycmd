﻿
namespace Anycmd.Host.AC.Messages
{
    using Anycmd.AC;
    using Events;

    public class SSDSetRemovedEvent : DomainEvent
    {
        #region Ctor
        public SSDSetRemovedEvent(SSDSetBase source)
            : base(source)
        {
        }
        #endregion
    }
}
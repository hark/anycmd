﻿
namespace Anycmd.Host.AC.Messages
{
    using Anycmd.AC;
    using Model;
    using InOuts;

    public class GroupAddedEvent : EntityAddedEvent<IGroupCreateIO>
    {
        #region Ctor
        public GroupAddedEvent(GroupBase source, IGroupCreateIO output)
            : base(source, output)
        {
        }
        #endregion
    }
}

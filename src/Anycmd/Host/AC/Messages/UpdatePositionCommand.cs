﻿
namespace Anycmd.Host.AC.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class UpdatePositionCommand : UpdateEntityCommand<IPositionUpdateIO>, ISysCommand
    {
        public UpdatePositionCommand(IPositionUpdateIO input)
            : base(input)
        {

        }
    }
}

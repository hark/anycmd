﻿
namespace Anycmd.Host.AC.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class AddPositionCommand : AddEntityCommand<IPositionCreateIO>, ISysCommand
    {
        public AddPositionCommand(IPositionCreateIO input)
            : base(input)
        {
        }
    }
}

﻿
namespace Anycmd.Host.AC.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class AddRoleCommand : AddEntityCommand<IRoleCreateIO>, ISysCommand
    {
        public AddRoleCommand(IRoleCreateIO input)
            : base(input)
        {

        }
    }
}

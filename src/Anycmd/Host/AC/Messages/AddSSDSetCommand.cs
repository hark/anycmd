﻿
namespace Anycmd.Host.AC.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class AddSSDSetCommand : AddEntityCommand<ISSDSetCreateIO>, ISysCommand
    {
        public AddSSDSetCommand(ISSDSetCreateIO input)
            : base(input)
        {

        }
    }
}

﻿
namespace Anycmd.Host.AC.Messages
{
    using AC.InOuts;
    using Anycmd.AC;
    using Events;

    public class PrivilegeBigramAddedEvent : DomainEvent
    {
        #region Ctor
        public PrivilegeBigramAddedEvent(PrivilegeBigramBase source, IPrivilegeBigramCreateIO output)
            : base(source)
        {
            if (output == null)
            {
                throw new System.ArgumentNullException("output");
            }
            this.Output = output;
        }
        #endregion

        public IPrivilegeBigramCreateIO Output { get; private set; }
    }
}

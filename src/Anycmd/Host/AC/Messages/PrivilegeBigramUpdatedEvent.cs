﻿
namespace Anycmd.Host.AC.Messages
{
    using AC.InOuts;
    using Anycmd.AC;
    using Events;

    public class PrivilegeBigramUpdatedEvent : DomainEvent
    {
        #region Ctor
        public PrivilegeBigramUpdatedEvent(PrivilegeBigramBase source, IPrivilegeBigramUpdateIO output)
            : base(source)
        {
            if (output == null)
            {
                throw new System.ArgumentNullException("output");
            }
            this.Output = output;
        }
        #endregion

        public IPrivilegeBigramUpdateIO Output { get; private set; }
    }
}

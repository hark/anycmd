﻿
namespace Anycmd.Host.AC.Messages
{
    using Anycmd.AC;
    using Model;
    using InOuts;

    public class DSDSetAddedEvent : EntityAddedEvent<IDSDSetCreateIO>
    {
        #region Ctor
        public DSDSetAddedEvent(DSDSetBase source, IDSDSetCreateIO output)
            : base(source, output)
        {
        }
        #endregion
    }
}

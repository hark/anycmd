﻿
namespace Anycmd.Host.AC.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class AddGroupCommand : AddEntityCommand<IGroupCreateIO>, ISysCommand
    {
        public AddGroupCommand(IGroupCreateIO input)
            : base(input)
        {

        }
    }
}

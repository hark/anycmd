﻿
namespace Anycmd.Host.AC.Messages
{
    using Anycmd.AC;
    using Events;
    using InOuts;

    public class SSDSetUpdatedEvent: DomainEvent
    {
        #region Ctor
        public SSDSetUpdatedEvent(SSDSetBase source, ISSDSetUpdateIO output)
            : base(source)
        {
            if (output == null)
            {
                throw new System.ArgumentNullException("output");
            }
            this.Output = output;
        }
        #endregion

        public ISSDSetUpdateIO Output { get; private set; }
    }
}
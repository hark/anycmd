﻿
namespace Anycmd.Host.AC.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class AddDSDRoleCommand : AddEntityCommand<IDSDRoleCreateIO>, ISysCommand
    {
        public AddDSDRoleCommand(IDSDRoleCreateIO input)
            : base(input)
        {

        }
    }
}

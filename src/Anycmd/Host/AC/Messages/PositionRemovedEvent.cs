﻿
namespace Anycmd.Host.AC.Messages
{
    using Anycmd.AC;
    using Events;

    public class PositionRemovedEvent : DomainEvent
    {
        #region Ctor
        public PositionRemovedEvent(GroupBase source)
            : base(source)
        {
        }
        #endregion
    }
}
﻿
namespace Anycmd.Host.AC.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class UpdateSSDSetCommand: UpdateEntityCommand<ISSDSetUpdateIO>, ISysCommand
    {
        public UpdateSSDSetCommand(ISSDSetUpdateIO input)
            : base(input)
        {

        }
    }
}

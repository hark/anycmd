﻿
namespace Anycmd.Host.AC.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class AddSSDRoleCommand : AddEntityCommand<ISSDRoleCreateIO>, ISysCommand
    {
        public AddSSDRoleCommand(ISSDRoleCreateIO input)
            : base(input)
        {

        }
    }
}

﻿
namespace Anycmd.Host.AC
{
    using Anycmd.AC;
    using Model;
    using InOuts;

    /// <summary>
    /// 表示动态职责分离角色集数据访问实体。
    /// </summary>
    public class DSDSet : DSDSetBase, IAggregateRoot
    {
        public static DSDSet Create(IDSDSetCreateIO input)
        {
            return new DSDSet
            {
                Id = input.Id.Value,
                Description = input.Description,
                IsEnabled = input.IsEnabled,
                Name = input.Name,
                DSDCard = input.DSDCard
            };
        }

        public void Update(IDSDSetUpdateIO input)
        {
            this.Description = input.Description;
            this.IsEnabled = input.IsEnabled;
            this.Name = input.Name;
            this.DSDCard = input.DSDCard;
        }
    }
}

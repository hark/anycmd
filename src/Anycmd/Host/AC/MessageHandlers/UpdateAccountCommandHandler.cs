﻿
namespace Anycmd.Host.AC.MessageHandlers
{
    using Commands;
    using Exceptions;
    using Identity;
    using Identity.Messages;
    using Repositories;
    using System.Linq;

    public class UpdateAccountCommandHandler : CommandHandler<UpdateAccountCommand>
    {
        private readonly IACDomain host;

        public UpdateAccountCommandHandler(IACDomain host)
        {
            this.host = host;
        }

        public override void Handle(UpdateAccountCommand command)
        {
            var accountRepository = host.GetRequiredService<IRepository<Account>>();
            if (accountRepository.AsQueryable()
                .Where(a => a.Code == command.Output.Code && a.Id != command.Output.Id)
                .Any())
            {
                throw new ValidationException("用户编码重复");
            }
            var entity = accountRepository.GetByKey(command.Output.Id);
            if (entity == null)
            {
                throw new NotExistException();
            }
            if (command.Output.OrganizationCode != entity.OrganizationCode)
            {
                if (string.IsNullOrEmpty(command.Output.OrganizationCode))
                {
                    throw new CoreException("用户必须属于一个组织结构");
                }
                OrganizationState organization;
                if (!host.OrganizationSet.TryGetOrganization(command.Output.OrganizationCode, out organization))
                {
                    throw new CoreException("意外的组织结构码" + command.Output.OrganizationCode);
                }
            }
            entity.Update(command.Output);
            accountRepository.Update(entity);
            accountRepository.Context.Commit();
            AccountState devAccount;
            if (host.SysUsers.TryGetDevAccount(entity.Id, out devAccount))
            {
                host.EventBus.Publish(new DeveloperUpdatedEvent(entity));
            }
            host.EventBus.Publish(new AccountUpdatedEvent(entity));
            host.EventBus.Commit();
        }
    }
}

﻿
namespace Anycmd.Host.AC.MessageHandlers
{
    using Anycmd.Rdb;
    using Bus;
    using Dapper;
    using Exceptions;
    using Identity.Messages;
    using System;
    using System.Data;

    public class AddVisitingLogCommandHandler : IHandler<AddVisitingLogCommand>
    {
        private readonly IACDomain host;

        public AddVisitingLogCommandHandler(IACDomain host)
        {
            this.host = host;
        }

        public void Handle(AddVisitingLogCommand message)
        {
            Guid dbID = new Guid("67E6CBF4-B481-4DDD-9FD9-1F0E06E9E1CB");
            RdbDescriptor db;
            if (!host.Rdbs.TryDb(dbID, out db))
            {
                //throw new CoreException("意外的数据库标识" + dbID);
                return;
            }
            using (var conn = db.GetConnection())
            {
                if (conn.State != ConnectionState.Open)
                {
                    conn.Open();
                }
                conn.Execute(
@"INSERT INTO dbo.VisitingLog
        ( Id ,
          AccountID ,
          LoginName ,
          VisitOn ,
          VisitedOn ,
          IPAddress ,
          StateCode ,
          ReasonPhrase ,
          Description
        )
VALUES  ( @Id ,
          @AccountID ,
          @LoginName ,
          @VisitOn ,
          @VisitedOn ,
          @IPAddress ,
          @StateCode ,
          @ReasonPhrase ,
          @Description
        )", new
                {
                    message.Id,
                    message.AccountID,
                    message.LoginName,
                    message.VisitOn,
                    message.VisitedOn,
                    message.IPAddress,
                    message.StateCode,
                    message.ReasonPhrase,
                    message.Description
                });
            }
        }
    }
}

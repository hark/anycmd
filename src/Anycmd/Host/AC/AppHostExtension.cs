﻿
namespace Anycmd.Host.AC
{
    using Identity.Messages;
    using Infra.Messages;
    using Messages;
    using System;
    using InOuts;

    public static class ACDomainExtension
    {
        public static void AddAppSystem(this IACDomain host, IAppSystemCreateIO input)
        {
            host.Handle(new AddAppSystemCommand(input));
        }

        public static void UpdateAppSystem(this IACDomain host, IAppSystemUpdateIO input)
        {
            host.Handle(new UpdateAppSystemCommand(input));
        }

        public static void RemoveAppSystem(this IACDomain host, Guid appSystemID)
        {
            host.Handle(new RemoveAppSystemCommand(appSystemID));
        }

        public static void AssignPassword(this IACDomain host, IPasswordAssignIO input)
        {
            host.Handle(new AssignPasswordCommand(input));
        }

        public static void ChangePassword(this IACDomain host, IPasswordChangeIO input)
        {
            host.Handle(new ChangePasswordCommand(input));
        }

        public static void AddAccount(this IACDomain host, IAccountCreateIO input)
        {
            host.Handle(new AddAccountCommand(input));
        }

        public static void UpdateAccount(this IACDomain host, IAccountUpdateIO input)
        {
            host.Handle(new UpdateAccountCommand(input));
        }

        public static void RemoveAccount(this IACDomain host, Guid accountID)
        {
            host.Handle(new RemoveAccountCommand(accountID));
        }

        public static void RemovePrivilegeBigram(this IACDomain host, Guid privilegeBigramID)
        {
            host.Handle(new RemovePrivilegeBigramCommand(privilegeBigramID));
        }

        public static void UpdatePrivilegeBigram(this IACDomain host, IPrivilegeBigramUpdateIO input)
        {
            host.Handle(new UpdatePrivilegeBigramCommand(input));
        }

        public static void AddPrivilegeBigram(this IACDomain host, IPrivilegeBigramCreateIO input)
        {
            host.Handle(new AddPrivilegeBigramCommand(input));
        }

        public static void AddButton(this IACDomain host, IButtonCreateIO input)
        {
            host.Handle(new AddButtonCommand(input));
        }

        public static void UpdateButton(this IACDomain host, IButtonUpdateIO input)
        {
            host.Handle(new UpdateButtonCommand(input));
        }

        public static void RemoveButton(this IACDomain host, Guid buttonID)
        {
            host.Handle(new RemoveButtonCommand(buttonID));
        }

        public static void AddDic(this IACDomain host, IDicCreateIO input)
        {
            host.Handle(new AddDicCommand(input));
        }

        public static void UpdateDic(this IACDomain host, IDicUpdateIO input)
        {
            host.Handle(new UpdateDicCommand(input));
        }

        public static void RemoveDic(this IACDomain host, Guid dicID)
        {
            host.Handle(new RemoveDicCommand(dicID));
        }

        public static void AddDicItem(this IACDomain host, IDicItemCreateIO input)
        {
            host.Handle(new AddDicItemCommand(input));
        }

        public static void UpdateDicItem(this IACDomain host, IDicItemUpdateIO input)
        {
            host.Handle(new UpdateDicItemCommand(input));
        }

        public static void RemoveDicItem(this IACDomain host, Guid dicItemID)
        {
            host.Handle(new RemoveDicItemCommand(dicItemID));
        }

        public static void AddEntityType(this IACDomain host, IEntityTypeCreateIO input)
        {
            host.Handle(new AddEntityTypeCommand(input));
        }

        public static void UpdateEntityType(this IACDomain host, IEntityTypeUpdateIO input)
        {
            host.Handle(new UpdateEntityTypeCommand(input));
        }

        public static void RemoveEntityType(this IACDomain host, Guid entityTypeID)
        {
            host.Handle(new RemoveEntityTypeCommand(entityTypeID));
        }

        public static void AddFunction(this IACDomain host, IFunctionCreateIO input)
        {
            host.Handle(new AddFunctionCommand(input));
        }
    }
}

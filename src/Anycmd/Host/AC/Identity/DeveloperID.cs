﻿
namespace Anycmd.Host.AC.Identity
{
    using Anycmd.AC.Identity;
    using Model;

    /// <summary>
    /// 表示开发者数据访问实体。
    /// </summary>
    public class DeveloperID : EntityBase, IDeveloperID, IAggregateRoot
    {
        public DeveloperID() { }
    }
}

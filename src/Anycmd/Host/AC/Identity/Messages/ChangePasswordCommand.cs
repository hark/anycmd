﻿
namespace Anycmd.Host.AC.Identity.Messages
{
    using Commands;
    using System;
    using InOuts;

    public class ChangePasswordCommand : Command, ISysCommand
    {
        public ChangePasswordCommand(IPasswordChangeIO input)
        {
            if (input == null)
            {
                throw new ArgumentNullException("input");
            }
            this.Input = input;
        }

        public IPasswordChangeIO Input { get; private set; }
    }
}

﻿
namespace Anycmd.Host.AC.Identity.Messages
{
    using Commands;
    using System;
    using InOuts;

    public class AssignPasswordCommand : Command, ISysCommand
    {
        public AssignPasswordCommand(IPasswordAssignIO input)
        {
            if (input == null)
            {
                throw new ArgumentNullException("input");
            }
            this.Input = input;
        }

        public IPasswordAssignIO Input { get; private set; }
    }
}

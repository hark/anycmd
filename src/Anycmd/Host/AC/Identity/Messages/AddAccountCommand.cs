﻿
namespace Anycmd.Host.AC.Identity.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class AddAccountCommand : AddEntityCommand<IAccountCreateIO>, ISysCommand
    {
        public AddAccountCommand(IAccountCreateIO input)
            : base(input)
        {

        }
    }
}

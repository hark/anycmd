﻿
namespace Anycmd.Host.AC.Identity.Messages
{
    using Commands;
    using Model;
    using InOuts;

    public class UpdateAccountCommand : UpdateEntityCommand<IAccountUpdateIO>, ISysCommand
    {
        public UpdateAccountCommand(IAccountUpdateIO input)
            : base(input)
        {

        }
    }
}

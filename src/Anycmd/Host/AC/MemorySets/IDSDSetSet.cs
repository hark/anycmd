﻿
namespace Anycmd.Host.AC.MemorySets
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// 表示该接口的实现类是动态职责分离角色集。
    /// </summary>
    public interface IDSDSetSet : IEnumerable<DSDSetState>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="dsdSetID"></param>
        /// <param name="dsdSet"></param>
        /// <returns></returns>
        bool TryGetDSDSet(Guid dsdSetID, out DSDSetState dsdSet);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="dsdSet"></param>
        /// <returns></returns>
        IReadOnlyCollection<DSDRoleState> GetDSDRoles(DSDSetState dsdSet);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IReadOnlyCollection<DSDRoleState> GetDSDRoles();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="roles"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        bool CheckRoles(IEnumerable<RoleState> roles, out string msg);
    }
}

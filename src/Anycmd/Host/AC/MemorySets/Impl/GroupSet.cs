﻿
namespace Anycmd.Host.AC.MemorySets.Impl
{
    using AC;
    using Anycmd.AC;
    using Anycmd.AC.Infra;
    using Bus;
    using Exceptions;
    using Extensions;
    using Host;
    using Host.Impl;
    using Infra.Messages;
    using Messages;
    using Repositories;
    using System;
    using System.Collections;
    using System.Collections.Generic;
    using System.Linq;
    using InOuts;

    public sealed class GroupSet : IGroupSet
    {
        public static readonly IGroupSet Empty = new GroupSet(EmptyACDomain.SingleInstance);

        private readonly Dictionary<Guid, GroupState> _groupDic = new Dictionary<Guid, GroupState>();
        private bool _initialized = false;

        private readonly Guid _id = Guid.NewGuid();
        private readonly IACDomain host;
        public Guid Id
        {
            get { return _id; }
        }

        public GroupSet(IACDomain host)
        {
            if (host == null)
            {
                throw new ArgumentNullException("host");
            }
            this.host = host;
            new MessageHandler(this).Register();
        }

        public bool TryGetGroup(Guid groupID, out GroupState group)
        {
            if (!_initialized)
            {
                Init();
            }
            return _groupDic.TryGetValue(groupID, out group);
        }

        public IEnumerator<GroupState> GetEnumerator()
        {
            if (!_initialized)
            {
                Init();
            }
            return _groupDic.Values.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            if (!_initialized)
            {
                Init();
            }
            return _groupDic.Values.GetEnumerator();
        }

        private void Init()
        {
            if (!_initialized)
            {
                lock (this)
                {
                    if (!_initialized)
                    {
                        _groupDic.Clear();
                        var groups = host.GetRequiredService<IOriginalHostStateReader>().GetAllGroups();
                        foreach (var group in groups)
                        {
                            if (!(group is GroupBase))
                            {
                                throw new CoreException(group.GetType().Name + "必须继承" + typeof(GroupBase).Name);
                            }
                            if (!_groupDic.ContainsKey(group.Id))
                            {
                                _groupDic.Add(group.Id, GroupState.Create(group));
                            }
                        }
                        _initialized = true;
                    }
                }
            }
        }

        #region MessageHandler
        private class MessageHandler :
            IHandler<AddGroupCommand>,
            IHandler<AddPositionCommand>,
            IHandler<GroupAddedEvent>,
            IHandler<PositionAddedEvent>,
            IHandler<UpdateGroupCommand>,
            IHandler<UpdatePositionCommand>,
            IHandler<GroupUpdatedEvent>,
            IHandler<PositionUpdatedEvent>,
            IHandler<RemoveGroupCommand>, 
            IHandler<RemovePositionCommand>, 
            IHandler<GroupRemovedEvent>,
            IHandler<PositionRemovedEvent>,
            IHandler<OrganizationRemovedEvent>
        {
            private readonly GroupSet set;

            public MessageHandler(GroupSet set)
            {
                this.set = set;
            }

            public void Register()
            {
                var messageDispatcher = set.host.MessageDispatcher;
                if (messageDispatcher == null)
                {
                    throw new ArgumentNullException("messageDispatcher has not be set of host:{0}".Fmt(set.host.Name));
                }
                messageDispatcher.Register((IHandler<AddGroupCommand>)this);
                messageDispatcher.Register((IHandler<GroupAddedEvent>)this);
                messageDispatcher.Register((IHandler<AddPositionCommand>)this);
                messageDispatcher.Register((IHandler<PositionAddedEvent>)this);
                messageDispatcher.Register((IHandler<UpdateGroupCommand>)this);
                messageDispatcher.Register((IHandler<GroupUpdatedEvent>)this);
                messageDispatcher.Register((IHandler<UpdatePositionCommand>)this);
                messageDispatcher.Register((IHandler<PositionUpdatedEvent>)this);
                messageDispatcher.Register((IHandler<RemoveGroupCommand>)this);
                messageDispatcher.Register((IHandler<GroupRemovedEvent>)this);
                messageDispatcher.Register((IHandler<RemovePositionCommand>)this);
                messageDispatcher.Register((IHandler<PositionRemovedEvent>)this);
            }

            public void Handle(OrganizationRemovedEvent message)
            {
                var organizationCode = (message.Source as OrganizationBase).Code;
                var groupIDs = new HashSet<Guid>();
                foreach (var item in set._groupDic.Values)
                {
                    if (!string.IsNullOrEmpty(item.OrganizationCode) && item.OrganizationCode.Equals(organizationCode, StringComparison.OrdinalIgnoreCase))
                    {
                        groupIDs.Add(item.Id);
                    }
                }
                foreach (var groupID in groupIDs)
                {
                    set.host.Handle(new RemovePositionCommand(groupID));
                }
            }

            public void Handle(AddGroupCommand message)
            {
                this.Handle(message.Input, true);
            }

            public void Handle(GroupAddedEvent message)
            {
                if (message.GetType() == typeof(PrivateGroupAddedEvent))
                {
                    return;
                }
                this.Handle(message.Output, false);
            }

            public void Handle(AddPositionCommand message)
            {
                this.Handle(message.Input, true);
            }

            public void Handle(PositionAddedEvent message)
            {
                if (message.GetType() == typeof(PrivatePositionAddedEvent))
                {
                    return;
                }
                this.Handle(message.Output, false);
            }

            private void Handle(IGroupCreateIO input, bool isCommand)
            {
                var host = set.host;
                var _groupDic = set._groupDic;
                var groupRepository = host.GetRequiredService<IRepository<Group>>();
                if (!input.Id.HasValue)
                {
                    throw new ValidationException("标识是必须的");
                }

                var entity = Group.Create(input);

                lock (this)
                {
                    GroupState group;
                    if (host.GroupSet.TryGetGroup(entity.Id, out group))
                    {
                        throw new CoreException("意外的重复标识");
                    }
                    if (host.GroupSet.Any(a => a.Name.Equals(input.Name, StringComparison.OrdinalIgnoreCase)))
                    {
                        throw new ValidationException("重复的工作组名");
                    }
                    if (!_groupDic.ContainsKey(entity.Id))
                    {
                        _groupDic.Add(entity.Id, GroupState.Create(entity));
                    }
                    if (isCommand)
                    {
                        try
                        {
                            groupRepository.Add(entity);
                            groupRepository.Context.Commit();
                        }
                        catch
                        {
                            if (_groupDic.ContainsKey(entity.Id))
                            {
                                _groupDic.Remove(entity.Id);
                            }
                            groupRepository.Context.Rollback();
                            throw;
                        }
                    }
                }
                if (isCommand)
                {
                    host.MessageDispatcher.DispatchMessage(new PrivateGroupAddedEvent(entity, input));
                }
            }

            private void Handle(IPositionCreateIO input, bool isCommand)
            {
                var host = set.host;
                var _groupDic = set._groupDic;
                var groupRepository = host.GetRequiredService<IRepository<Group>>();
                if (!input.Id.HasValue)
                {
                    throw new ValidationException("标识是必须的");
                }
                if (string.IsNullOrEmpty(input.OrganizationCode))
                {
                    throw new ValidationException("组织结构码不能为空");
                }
                OrganizationState org;
                if (!host.OrganizationSet.TryGetOrganization(input.OrganizationCode, out org))
                {
                    throw new ValidationException("非法的组织结构码" + input.OrganizationCode);
                }

                var entity = Group.Create(input);

                lock (this)
                {
                    GroupState group;
                    if (host.GroupSet.TryGetGroup(entity.Id, out group))
                    {
                        throw new CoreException("意外的重复标识");
                    }
                    if (host.GroupSet.Any(a => input.OrganizationCode.Equals(a.OrganizationCode, StringComparison.OrdinalIgnoreCase) && a.Name.Equals(input.Name, StringComparison.OrdinalIgnoreCase)))
                    {
                        throw new ValidationException("重复的岗位名");
                    }
                    if (!_groupDic.ContainsKey(entity.Id))
                    {
                        _groupDic.Add(entity.Id, GroupState.Create(entity));
                    }
                    if (isCommand)
                    {
                        try
                        {
                            groupRepository.Add(entity);
                            groupRepository.Context.Commit();
                        }
                        catch
                        {
                            if (_groupDic.ContainsKey(entity.Id))
                            {
                                _groupDic.Remove(entity.Id);
                            }
                            groupRepository.Context.Rollback();
                            throw;
                        }
                    }
                }
                if (isCommand)
                {
                    host.MessageDispatcher.DispatchMessage(new PrivatePositionAddedEvent(entity, input));
                }
            }

            private class PrivateGroupAddedEvent : GroupAddedEvent
            {
                public PrivateGroupAddedEvent(GroupBase source, IGroupCreateIO input)
                    : base(source, input)
                {

                }
            }
            private class PrivatePositionAddedEvent : PositionAddedEvent
            {
                public PrivatePositionAddedEvent(GroupBase source, IPositionCreateIO input)
                    : base(source, input)
                {

                }
            }

            public void Handle(UpdateGroupCommand message)
            {
                this.Handle(message.Output, true);
            }

            public void Handle(GroupUpdatedEvent message)
            {
                if (message.GetType() == typeof(PrivateGroupUpdatedEvent))
                {
                    return;
                }
                this.Handle(message.Output, false);
            }

            public void Handle(UpdatePositionCommand message)
            {
                this.Handle(message.Output, true);
            }

            public void Handle(PositionUpdatedEvent message)
            {
                if (message.GetType() == typeof(PrivatePositionUpdatedEvent))
                {
                    return;
                }
                this.Handle(message.Output, false);
            }

            private void Handle(IGroupUpdateIO input, bool isCommand)
            {
                var host = set.host;
                var _groupDic = set._groupDic;
                var groupRepository = host.GetRequiredService<IRepository<Group>>();
                GroupState bkState;
                if (!host.GroupSet.TryGetGroup(input.Id, out bkState))
                {
                    throw new NotExistException();
                }
                Group entity;
                bool stateChanged = false;
                lock (bkState)
                {
                    GroupState oldState;
                    if (!host.GroupSet.TryGetGroup(input.Id, out oldState))
                    {
                        throw new NotExistException();
                    }
                    if (host.GroupSet.Any(a => a.Name.Equals(input.Name, StringComparison.OrdinalIgnoreCase) && a.Id != input.Id))
                    {
                        throw new ValidationException("重复的工作组名");
                    }
                    entity = groupRepository.GetByKey(input.Id);
                    if (entity == null)
                    {
                        throw new NotExistException();
                    }

                    entity.Update(input);

                    var newState = GroupState.Create(entity);
                    stateChanged = newState != bkState;
                    if (stateChanged)
                    {
                        Update(newState);
                    }
                    if (isCommand)
                    {
                        try
                        {
                            groupRepository.Update(entity);
                            groupRepository.Context.Commit();
                        }
                        catch
                        {
                            if (stateChanged)
                            {
                                Update(bkState);
                            }
                            groupRepository.Context.Rollback();
                            throw;
                        }
                    }
                }
                if (isCommand && stateChanged)
                {
                    host.MessageDispatcher.DispatchMessage(new PrivateGroupUpdatedEvent(entity, input));
                }
            }

            private void Handle(IPositionUpdateIO input, bool isCommand)
            {
                var host = set.host;
                var _groupDic = set._groupDic;
                var groupRepository = host.GetRequiredService<IRepository<Group>>();
                GroupState bkState;
                if (!host.GroupSet.TryGetGroup(input.Id, out bkState))
                {
                    throw new NotExistException();
                }
                if (string.IsNullOrEmpty(bkState.OrganizationCode))
                {
                    throw new CoreException("组织结构码为空");
                }
                Group entity;
                bool stateChanged = false;
                lock (bkState)
                {
                    GroupState oldState;
                    if (!host.GroupSet.TryGetGroup(input.Id, out oldState))
                    {
                        throw new NotExistException();
                    }
                    if (host.GroupSet.Any(a => bkState.OrganizationCode.Equals(a.OrganizationCode, StringComparison.OrdinalIgnoreCase) && a.Name.Equals(input.Name, StringComparison.OrdinalIgnoreCase) && a.Id != input.Id))
                    {
                        throw new ValidationException("重复的岗位名");
                    }
                    entity = groupRepository.GetByKey(input.Id);
                    if (entity == null)
                    {
                        throw new NotExistException();
                    }

                    entity.Update(input);

                    var newState = GroupState.Create(entity);
                    stateChanged = newState != bkState;
                    if (stateChanged)
                    {
                        Update(newState);
                    }
                    if (isCommand)
                    {
                        try
                        {
                            groupRepository.Update(entity);
                            groupRepository.Context.Commit();
                        }
                        catch
                        {
                            if (stateChanged)
                            {
                                Update(bkState);
                            }
                            groupRepository.Context.Rollback();
                            throw;
                        }
                    }
                }
                if (isCommand && stateChanged)
                {
                    host.MessageDispatcher.DispatchMessage(new PrivatePositionUpdatedEvent(entity, input));
                }
            }

            private void Update(GroupState state)
            {
                var host = set.host;
                var _groupDic = set._groupDic;
                _groupDic[state.Id] = state;
            }

            private class PrivateGroupUpdatedEvent : GroupUpdatedEvent
            {
                public PrivateGroupUpdatedEvent(GroupBase source, IGroupUpdateIO input)
                    : base(source, input)
                {

                }
            }

            private class PrivatePositionUpdatedEvent : PositionUpdatedEvent
            {
                public PrivatePositionUpdatedEvent(GroupBase source, IPositionUpdateIO input)
                    : base(source, input)
                {

                }
            }

            public void Handle(RemoveGroupCommand message)
            {
                this.Handle(message.EntityID, true);
            }

            public void Handle(GroupRemovedEvent message)
            {
                if (message.GetType() == typeof(PrivateGroupRemovedEvent))
                {
                    return;
                }
                this.Handle(message.Source.Id, false);
            }

            public void Handle(RemovePositionCommand message)
            {
                this.Handle(message.EntityID, true);
            }

            public void Handle(PositionRemovedEvent message)
            {
                if (message.GetType() == typeof(PrivatePositionRemovedEvent))
                {
                    return;
                }
                this.Handle(message.Source.Id, false);
            }

            private void Handle(Guid groupID, bool isCommand)
            {
                var host = set.host;
                var _groupDic = set._groupDic;
                var groupRepository = host.GetRequiredService<IRepository<Group>>();
                GroupState bkState;
                if (!host.GroupSet.TryGetGroup(groupID, out bkState))
                {
                    return;
                }
                Group entity;
                lock (bkState)
                {
                    GroupState state;
                    if (!host.GroupSet.TryGetGroup(groupID, out state))
                    {
                        return;
                    }
                    entity = groupRepository.GetByKey(groupID);
                    if (entity == null)
                    {
                        return;
                    }
                    if (_groupDic.ContainsKey(bkState.Id))
                    {
                        if (isCommand)
                        {
                            host.MessageDispatcher.DispatchMessage(new GroupRemovingEvent(entity));
                        }
                        _groupDic.Remove(bkState.Id);
                    }
                    if (isCommand)
                    {
                        try
                        {
                            groupRepository.Remove(entity);
                            groupRepository.Context.Commit();
                        }
                        catch
                        {
                            if (!_groupDic.ContainsKey(entity.Id))
                            {
                                _groupDic.Add(bkState.Id, bkState);
                            }
                            groupRepository.Context.Rollback();
                            throw;
                        }
                    }
                }
                if (isCommand)
                {
                    host.MessageDispatcher.DispatchMessage(new PrivateGroupRemovedEvent(entity));
                    if (!string.IsNullOrEmpty(bkState.OrganizationCode))
                    {
                        host.MessageDispatcher.DispatchMessage(new PrivatePositionRemovedEvent(entity));
                    }
                }
            }

            private class PrivateGroupRemovedEvent : GroupRemovedEvent
            {
                public PrivateGroupRemovedEvent(GroupBase source)
                    : base(source)
                {

                }
            }

            private class PrivatePositionRemovedEvent : PositionRemovedEvent
            {
                public PrivatePositionRemovedEvent(GroupBase source)
                    : base(source)
                {

                }
            }
        }
        #endregion
    }
}

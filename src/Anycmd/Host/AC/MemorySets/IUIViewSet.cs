﻿
namespace Anycmd.Host.AC.MemorySets
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// 表示该接口的实现类是系统界面视图集。
    /// </summary>
    public interface IUIViewSet : IEnumerable<UIViewState>
    {
        /// <summary>
        /// 
        /// </summary>
        Guid Id { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="function"></param>
        /// <param name="view"></param>
        /// <returns></returns>
        /// <exception cref="CoreException">当索引的界面视图不存在时引发</exception>
        bool TryGetUIView(FunctionState function, out UIViewState view);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="viewID"></param>
        /// <param name="view"></param>
        /// <returns></returns>
        bool TryGetUIView(Guid viewID, out UIViewState view);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IEnumerable<UIViewButtonState> GetUIViewButtons();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="view"></param>
        /// <returns></returns>
        IReadOnlyList<UIViewButtonState> GetUIViewButtons(UIViewState view);
    }
}

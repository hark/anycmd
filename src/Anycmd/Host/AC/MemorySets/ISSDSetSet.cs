﻿
namespace Anycmd.Host.AC.MemorySets
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// 表示该接口的实现类是静态职责分离角色集。
    /// </summary>
    public interface ISSDSetSet : IEnumerable<SSDSetState>
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="ssdSetID"></param>
        /// <param name="ssdSet"></param>
        /// <returns></returns>
        bool TryGetSSDSet(Guid ssdSetID, out SSDSetState ssdSet);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="ssdSet"></param>
        /// <returns></returns>
        IReadOnlyCollection<SSDRoleState> GetSSDRoles(SSDSetState ssdSet);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IReadOnlyCollection<SSDRoleState> GetSSDRoles();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="roles"></param>
        /// <param name="msg"></param>
        /// <returns></returns>
        bool CheckRoles(IEnumerable<RoleState> roles, out string msg);
    }
}

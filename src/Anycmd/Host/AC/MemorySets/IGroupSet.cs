﻿
namespace Anycmd.Host.AC.MemorySets
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// 表示该接口的实现类是组集。
    /// </summary>
    public interface IGroupSet : IEnumerable<GroupState>
    {
        /// <summary>
        /// 
        /// </summary>
        Guid Id { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="groupID"></param>
        /// <param name="group"></param>
        /// <returns></returns>
        bool TryGetGroup(Guid groupID, out GroupState group);
    }
}

﻿
namespace Anycmd.Host.AC.MemorySets
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// 表示该接口的实现类是系统功能集。
    /// </summary>
    public interface IFunctionSet : IEnumerable<FunctionState>
    {
        /// <summary>
        /// 
        /// </summary>
        Guid Id { get; }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="appSystem"></param>
        /// <param name="resource"></param>
        /// <param name="functionCode"></param>
        /// <returns></returns>
        bool TryGetFunction(ResourceTypeState resource, string functionCode, out FunctionState function);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="functionID"></param>
        /// <param name="function"></param>
        /// <returns></returns>
        bool TryGetFunction(Guid functionID, out FunctionState function);
    }
}

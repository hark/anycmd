﻿
namespace Anycmd.Host.AC
{
    using Anycmd.AC;
    using Model;
    using InOuts;

    /// <summary>
    /// 表示静态职责分离角色集数据访问实体。
    /// </summary>
    public class SSDSet : SSDSetBase, IAggregateRoot
    {
        public static SSDSet Create(ISSDSetCreateIO input)
        {
            return new SSDSet
            {
                Id = input.Id.Value,
                Description = input.Description,
                IsEnabled = input.IsEnabled,
                Name = input.Name,
                SSDCard = input.SSDCard
            };
        }

        public void Update(ISSDSetUpdateIO input)
        {
            this.Description = input.Description;
            this.IsEnabled = input.IsEnabled;
            this.Name = input.Name;
            this.SSDCard = input.SSDCard;
        }
    }
}

﻿
namespace Anycmd.Host.AC
{
    using Anycmd.AC;
    using Model;
    using InOuts;

    /// <summary>
    /// 表示权限二元组数据访问实体。
    /// </summary>
    public class PrivilegeBigram : PrivilegeBigramBase, IAggregateRoot
    {
        public PrivilegeBigram() { }

        public static PrivilegeBigram Create(IPrivilegeBigramCreateIO input)
        {
            return new PrivilegeBigram
            {
                Id = input.Id.Value,
                SubjectType = input.SubjectType,
                SubjectInstanceID = input.SubjectInstanceID,
                ObjectType = input.ObjectType,
                ObjectInstanceID = input.ObjectInstanceID,
                PrivilegeConstraint = input.PrivilegeConstraint,
                PrivilegeOrientation = input.PrivilegeOrientation
            };
        }

        public void Update(IPrivilegeBigramUpdateIO input)
        {
            this.PrivilegeConstraint = input.PrivilegeConstraint;
        }
    }
}

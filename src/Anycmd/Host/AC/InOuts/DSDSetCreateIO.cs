﻿
namespace Anycmd.Host.AC.InOuts
{
    using Model;

    /// <summary>
    /// 创建动态职责分离角色集时的输入或输出参数类型。
    /// </summary>
    public class DSDSetCreateIO : EntityCreateInput, IInputModel, IDSDSetCreateIO
    {
        public string Name { get; set; }

        public int DSDCard { get; set; }

        public int IsEnabled { get; set; }

        public string Description { get; set; }
    }
}

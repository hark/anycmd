﻿
namespace Anycmd.Host.AC.InOuts
{
    using Model;
    using System;

    /// <summary>
    /// 表示该接口的实现类是创建系统实体类型时的输入或输出参数类型。
    /// </summary>
    public interface IEntityTypeCreateIO : IEntityCreateInput
    {
        string Code { get; }
        bool IsOrganizational { get; }
        string Codespace { get; }
        Guid DatabaseID { get; }
        string Description { get; }
        Guid DeveloperID { get; }
        int EditHeight { get; }
        int EditWidth { get; }
        string Name { get; }
        string SchemaName { get; }
        int SortCode { get; }
        string TableName { get; }
    }
}

﻿
namespace Anycmd.Host.AC.InOuts
{
    using Model;
    using System;

    /// <summary>
    /// 创建动态职责分离角色时的输入或输出参数类型。
    /// </summary>
    public class DSDRoleCreateIO : EntityCreateInput, IInputModel, IDSDRoleCreateIO
    {
        public Guid DSDSetID { get; set; }

        public Guid RoleID { get; set; }
    }
}

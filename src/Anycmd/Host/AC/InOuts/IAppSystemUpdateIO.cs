﻿
namespace Anycmd.Host.AC.InOuts
{
    using Model;
    using System;

    /// <summary>
    /// 表示该接口的实现类是更新应用系统时的输入或输出参数类型。
    /// </summary>
    public interface IAppSystemUpdateIO : IEntityUpdateInput, IManagedPropertyValues
    {
        string Code { get; }
        string Description { get; }
        string Icon { get; }
        string ImageUrl { get; }
        int IsEnabled { get; }
        string Name { get; }
        Guid PrincipalID { get; }
        int SortCode { get; }
        string SSOAuthAddress { get; }
    }
}

﻿
namespace Anycmd.Host.AC.InOuts
{
    using Model;
    using System;

    /// <summary>
    /// 表示该接口的实现类是更新系统实体类型时的输入或输出参数类型。
    /// </summary>
    public interface IEntityTypeUpdateIO : IEntityUpdateInput
    {
        string Code { get; }
        string Codespace { get; }
        bool IsOrganizational { get; }
        Guid DatabaseID { get; }
        string Description { get; }
        Guid DeveloperID { get; }
        int EditHeight { get; }
        int EditWidth { get; }
        string Name { get; }
        string SchemaName { get; }
        int SortCode { get; }
        string TableName { get; }
    }
}

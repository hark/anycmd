﻿
namespace Anycmd.Host.AC.InOuts
{
    using Model;
    using System;

    /// <summary>
    /// 更新权限二元组时的输入或输出参数类型。
    /// </summary>
    public class PrivilegeBigramUpdateIO : IInputModel, IPrivilegeBigramUpdateIO
    {
        public Guid Id { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public string PrivilegeConstraint { get; set; }
    }
}

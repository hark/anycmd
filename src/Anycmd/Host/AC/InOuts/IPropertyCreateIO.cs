﻿
namespace Anycmd.Host.AC.InOuts
{
    using Model;
    using System;

    /// <summary>
    /// 表示该接口的实现类是创建实体属性时的输入或输出参数类型。
    /// </summary>
    public interface IPropertyCreateIO : IEntityCreateInput
    {
        /// <summary>
        /// 
        /// </summary>
        Guid? ForeignPropertyID { get; }
        string Code { get; }
        string Description { get; }
        Guid? DicID { get; }
        Guid EntityTypeID { get; }
        string GroupCode { get; }
        string GuideWords { get; }
        string Icon { get; }
        string InputType { get; }
        bool IsDetailsShow { get; }
        bool IsDeveloperOnly { get; }
        bool IsInput { get; }
        bool IsTotalLine { get; }
        int? MaxLength { get; }
        string Name { get; }
        string Tooltip { get; }
        int SortCode { get; }
    }
}

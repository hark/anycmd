﻿
namespace Anycmd.Host.AC.InOuts
{
    using Model;

    /// <summary>
    /// 表示该接口的实现类是创建界面视图时的输入或输出参数类型。
    /// </summary>
    public interface IUIViewCreateIO : IEntityCreateInput
    {
        string Icon { get; }
        string Tooltip { get; }
    }
}

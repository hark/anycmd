﻿
namespace Anycmd.Host.AC.InOuts
{
    using Model;
    using System;

    /// <summary>
    /// 表示该接口的实现类是更新系统字典时的输入或输出参数类型。
    /// </summary>
    public interface IDicUpdateIO : IEntityUpdateInput
    {
        string Code { get; }
        string Description { get; }
        int IsEnabled { get; }
        int SortCode { get; }
        string Name { get; }
    }
}

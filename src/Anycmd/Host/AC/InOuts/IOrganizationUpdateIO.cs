﻿
namespace Anycmd.Host.AC.InOuts
{
    using Model;
    using System;

    /// <summary>
    /// 表示该接口的实现类是更新组织结构时的输入或输出参数类型。
    /// </summary>
    public interface IOrganizationUpdateIO : IEntityUpdateInput
    {
        string Address { get; }
        string CategoryCode { get; }
        string Code { get; }
        string Description { get; }
        string Fax { get; }
        string Icon { get; }
        string InnerPhone { get; }
        int IsEnabled { get; }
        string Name { get; }
        string Postalcode { get; }
        string OuterPhone { get; }
        string ParentCode { get; }
        string ShortName { get; }
        int SortCode { get; }
        string WebPage { get; }
        Guid? ContractorID { get; }
    }
}

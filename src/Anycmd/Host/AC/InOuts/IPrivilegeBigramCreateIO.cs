﻿
namespace Anycmd.Host.AC.InOuts
{
    using Model;
    using System;

    /// <summary>
    /// 表示该接口的实现类是创建权限二元组时的输入或输出参数类型。
    /// </summary>
    public interface IPrivilegeBigramCreateIO : IEntityCreateInput
    {
        string SubjectType { get; }
        Guid SubjectInstanceID { get; }
        string ObjectType { get; }
        Guid ObjectInstanceID { get; }
        string PrivilegeConstraint { get; }
        int PrivilegeOrientation { get; }
    }
}

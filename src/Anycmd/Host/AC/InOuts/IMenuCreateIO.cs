﻿
namespace Anycmd.Host.AC.InOuts
{
    using Model;
    using System;

    /// <summary>
    /// 表示该接口的实现类是创建系统菜单时的输入或输出参数类型。
    /// </summary>
    public interface IMenuCreateIO : IEntityCreateInput
    {
        Guid AppSystemID { get; }
        string Description { get; }
        string Icon { get; }
        string Name { get; }
        Guid? ParentID { get; }
        int SortCode { get; }
        string Url { get; }
    }
}

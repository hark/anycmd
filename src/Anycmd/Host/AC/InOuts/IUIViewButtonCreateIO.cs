﻿
namespace Anycmd.Host.AC.InOuts
{
    using Model;
    using System;

    /// <summary>
    /// 表示该接口的实现类是创建界面视图按钮时的输入或输出参数类型。
    /// </summary>
    public interface IUIViewButtonCreateIO : IEntityCreateInput
    {
        Guid ButtonID { get; }
        Guid? FunctionID { get; }
        int IsEnabled { get; }
        Guid UIViewID { get; }
    }
}

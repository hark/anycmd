﻿
namespace Anycmd.Host.AC.InOuts
{
    using Model;
    using System;

    /// <summary>
    /// 表示该接口的实现类是创建系统功能时的输入或输出参数类型。
    /// </summary>
    public interface IFunctionCreateIO : IEntityCreateInput
    {
        string Code { get; }
        bool IsManaged { get; }
        int IsEnabled { get; }
        string Description { get; }
        Guid DeveloperID { get; }
        Guid ResourceTypeID { get; }
        int SortCode { get; }
    }
}

﻿
namespace Anycmd.Host.AC.InOuts
{
    using Model;

    /// <summary>
    /// 创建静态职责分离角色集时的输入或输出参数类型。
    /// </summary>
    public class SSDSetCreateIO : EntityCreateInput, IInputModel, ISSDSetCreateIO
    {
        public string Name { get; set; }

        public int SSDCard { get; set; }

        public int IsEnabled { get; set; }

        public string Description { get; set; }
    }
}

﻿
namespace Anycmd.Host.AC.InOuts
{
    using Model;

    /// <summary>
    /// 表示该接口的实现类是创建系统按钮时的输入或输出参数类型。
    /// </summary>
    public interface IButtonCreateIO : IEntityCreateInput
    {
        string Code { get; }
        string CategoryCode { get; }
        string Description { get; }
        string Icon { get; }
        int IsEnabled { get; }
        string Name { get; }
        int SortCode { get; }
    }
}

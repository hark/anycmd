﻿
namespace Anycmd.Host.AC.InOuts
{
    using Model;
    using System;

    /// <summary>
    /// 创建静态职责分离角色时的输入或输出参数类型。
    /// </summary>
    public class SSDRoleCreateIO : EntityCreateInput, IInputModel, ISSDRoleCreateIO
    {
        public Guid SSDSetID { get; set; }

        public Guid RoleID { get; set; }
    }
}

﻿
namespace Anycmd.Host.AC.InOuts
{
    using Model;
    using System;

    /// <summary>
    /// 表示该接口的实现类是更新权限二元组时的输入或输出参数类型。
    /// </summary>
    public interface IPrivilegeBigramUpdateIO : IEntityUpdateInput
    {
        string PrivilegeConstraint { get; }
    }
}

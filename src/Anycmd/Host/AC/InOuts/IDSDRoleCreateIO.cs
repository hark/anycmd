﻿
namespace Anycmd.Host.AC.InOuts
{
    using Model;
    using System;

    /// <summary>
    /// 表示该接口的实现类是创建动态职责分离角色时的输入或输出参数类型。
    /// </summary>
    public interface IDSDRoleCreateIO : IEntityCreateInput
    {
        Guid DSDSetID { get; }
        Guid RoleID { get; }
    }
}

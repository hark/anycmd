﻿
namespace Anycmd.Host.AC.InOuts
{
    using Model;
    using System;

    /// <summary>
    /// 表示该接口的实现类是更新界面视图按钮时的输入或输出参数类型。
    /// </summary>
    public interface IUIViewButtonUpdateIO : IEntityUpdateInput
    {
        Guid? FunctionID { get; }
        int IsEnabled { get; }
    }
}

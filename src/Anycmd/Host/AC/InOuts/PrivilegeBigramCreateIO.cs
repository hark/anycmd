﻿
namespace Anycmd.Host.AC.InOuts
{
    using Model;
    using System;

    /// <summary>
    /// 创建权限二元组时的输入或输出参数类型。
    /// </summary>
    public class PrivilegeBigramCreateIO : EntityCreateInput, IInputModel, IPrivilegeBigramCreateIO
    {
        public string SubjectType { get; set; }

        public Guid SubjectInstanceID { get; set; }

        public string ObjectType { get; set; }

        public Guid ObjectInstanceID { get; set; }

        public string PrivilegeConstraint { get; set; }

        public int PrivilegeOrientation { get; set; }
    }
}

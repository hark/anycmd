﻿
namespace Anycmd.Host.AC
{
    using Anycmd.AC;
    using Model;
    using InOuts;

    /// <summary>
    /// 表示静态职责分离角色数据访问实体。
    /// </summary>
    public class SSDRole : SSDRoleBase, IAggregateRoot
    {
        public static SSDRole Create(ISSDRoleCreateIO input)
        {
            return new SSDRole
            {
                Id = input.Id.Value,
                SSDSetID = input.SSDSetID,
                RoleID = input.RoleID
            };
        }
    }
}

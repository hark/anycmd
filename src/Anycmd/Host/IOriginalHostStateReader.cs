﻿
namespace Anycmd.Host
{
    using AC;
    using AC.Identity;
    using AC.Infra;
    using Anycmd.Rdb;
    using System.Collections.Generic;

    /// <summary>
    /// 表示该接口的实现类是用以读取系统原始状态的状态读取器。
    /// </summary>
    public interface IOriginalHostStateReader
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IList<RDatabase> GetAllRDatabases();

        /// <summary>
        /// 
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        IList<DbTableColumn> GetTableColumns(RdbDescriptor db);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        IList<DbTable> GetDbTables(RdbDescriptor db);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        IList<DbViewColumn> GetViewColumns(RdbDescriptor db);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="db"></param>
        /// <returns></returns>
        IList<DbView> GetDbViews(RdbDescriptor db);

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IList<Organization> GetOrganizations();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IList<AppSystem> GetAllAppSystems();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IList<Button> GetAllButtons();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IList<Dic> GetAllDics();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IList<DicItem> GetAllDicItems();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IList<EntityType> GetAllEntityTypes();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IList<Property> GetAllProperties();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IList<Function> GetAllFunctions();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IList<Group> GetAllGroups();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IList<Menu> GetAllMenus();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IList<UIView> GetAllUIViews();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IList<UIViewButton> GetAllUIViewButtons();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IList<PrivilegeBigram> GetPrivilegeBigrams();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IList<ResourceType> GetAllResources();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IList<Role> GetAllRoles();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IList<SSDSet> GetAllSSDSets();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IList<DSDSet> GetAllDSDSets();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IList<SSDRole> GetAllSSDRoles();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IList<DSDRole> GetAllDSDRoles();

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        IList<Account> GetAllDevAccounts();
    }
}

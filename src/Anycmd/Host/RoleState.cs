﻿
namespace Anycmd.Host
{
    using Anycmd.AC;
    using Model;
    using System;

    /// <summary>
    /// 表示角色业务实体。
    /// </summary>
    public sealed class RoleState : StateObject<RoleState>, IRole, IStateObject
    {
        private RoleState() { }

        public static RoleState Create(RoleBase role)
        {
            if (role == null)
            {
                throw new ArgumentNullException("role");
            }
            return new RoleState
            {
                Id = role.Id,
                Name = role.Name,
                CategoryCode = role.CategoryCode,
                CreateOn = role.CreateOn,
                IsEnabled = role.IsEnabled,
                Icon = role.Icon,
                SortCode = role.SortCode
            };
        }

        public string Name { get; private set; }
        public string CategoryCode { get; private set; }
        public DateTime? CreateOn { get; private set; }
        public int IsEnabled { get; private set; }
        public string Icon { get; private set; }
        public int SortCode { get; private set; }

        protected override bool DoEquals(RoleState other)
        {
            return Id == other.Id &&
                Name == other.Name &&
                CategoryCode == other.CategoryCode &&
                IsEnabled == other.IsEnabled &&
                Icon == other.Icon &&
                SortCode == other.SortCode;
        }
    }
}

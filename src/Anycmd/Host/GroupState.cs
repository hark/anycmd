﻿
namespace Anycmd.Host
{
    using Anycmd.AC;
    using Model;
    using System;

    /// <summary>
    /// 表示组业务实体。
    /// </summary>
    public sealed class GroupState : StateObject<GroupState>, IGroup, IStateObject
    {
        private GroupState() { }

        public static GroupState Create(GroupBase group)
        {
            if (group == null)
            {
                throw new ArgumentNullException("group");
            }
            return new GroupState
            {
                Id = group.Id,
                Name = group.Name,
                OrganizationCode = group.OrganizationCode,
                CategoryCode = group.CategoryCode,
                SortCode = group.SortCode,
                IsEnabled = group.IsEnabled,
                CreateOn = group.CreateOn
            };
        }

        public string Name { get; private set; }

        public string OrganizationCode { get; private set; }

        public string CategoryCode { get; private set; }

        public int SortCode { get; private set; }

        public int IsEnabled { get; private set; }

        public DateTime? CreateOn { get; private set; }

        protected override bool DoEquals(GroupState other)
        {
            return Id == other.Id &&
                Name == other.Name &&
                OrganizationCode == other.OrganizationCode &&
                CategoryCode == other.CategoryCode &&
                SortCode == other.SortCode &&
                IsEnabled == other.IsEnabled;
        }
    }
}

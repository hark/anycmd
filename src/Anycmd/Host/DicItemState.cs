﻿
namespace Anycmd.Host
{
    using Anycmd.AC.Infra;
    using Exceptions;
    using Model;
    using System;
    using Util;

    /// <summary>
    /// 表示系统字典项业务实体。
    /// </summary>
    public sealed class DicItemState : StateObject<DicItemState>, IDicItem, IStateObject
    {
        public static readonly DicItemState Empty = new DicItemState
        {
            CreateOn = SystemTime.MinDate,
            IsEnabled = 0,
            SortCode = 0,
            Code = string.Empty,
            DicID = Guid.Empty,
            Id = Guid.Empty,
            Name = string.Empty
        };

        private DicItemState() { }

        public static DicItemState Create(IACDomain host, DicItemBase dicItem)
        {
            if (dicItem == null)
            {
                throw new ArgumentNullException("dicItem");
            }
            if (!host.DicSet.ContainsDic(dicItem.DicID))
            {
                throw new CoreException("意外的字典" + dicItem.DicID);
            }
            return new DicItemState
            {
                Id = dicItem.Id,
                Code = dicItem.Code,
                Name = dicItem.Name,
                DicID = dicItem.DicID,
                SortCode = dicItem.SortCode,
                IsEnabled = dicItem.IsEnabled,
                CreateOn = dicItem.CreateOn
            };
        }

        public string Code { get; private set; }

        public string Name { get; private set; }

        public Guid DicID { get; private set; }

        public int SortCode { get; private set; }

        public int IsEnabled { get; private set; }

        public DateTime? CreateOn { get; private set; }

        protected override bool DoEquals(DicItemState other)
        {
            return Id == other.Id &&
                Code == other.Code &&
                Name == other.Name &&
                DicID == other.DicID &&
                SortCode == other.SortCode &&
                IsEnabled == other.IsEnabled;
        }
    }
}

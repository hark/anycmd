﻿
namespace Anycmd.Rdb
{
    using System.Collections.Generic;

    /// <summary>
    /// 表示该接口的实现类是关系数据库视图集。
    /// </summary>
    public interface IDbViews
    {
        IReadOnlyDictionary<string, DbView> this[RdbDescriptor database] { get; }
        bool TryGetDbView(RdbDescriptor db, string dbViewID, out DbView dbView);
    }
}

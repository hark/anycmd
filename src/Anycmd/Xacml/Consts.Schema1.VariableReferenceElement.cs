﻿
namespace Anycmd.Xacml
{
    public partial class Consts
    {
        public partial class Schema1
        {
            /// <summary>The name of the element/attribute in the XSD schema.</summary>
            public static class VariableReferenceElement
            {
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string VariableReference = "VariableReference";

                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string VariableId = "VariableId";
            }
        }
    }
}
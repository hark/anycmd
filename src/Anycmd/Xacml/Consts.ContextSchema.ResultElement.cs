﻿
namespace Anycmd.Xacml
{
    public partial class Consts
    {
        public partial class ContextSchema
        {
            /// <summary>The name of the element/attribute in the XSD schema.</summary>
            public static class ResultElement
            {
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string Result = "Result";
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string Decision = "Decision";
            }
        }
    }
}
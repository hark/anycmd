
namespace Anycmd.Xacml
{
	/// <summary>
	/// The Xacml schema that defines the element.
	/// </summary>
	public enum XacmlSchema
	{
		/// <summary>
		/// The element is defined in the Policy schema.
		/// </summary>
		Policy,

		/// <summary>
		/// The element is defined in the Context schema.
		/// </summary>
		Context
	}
}

﻿
namespace Anycmd.Xacml
{
    public partial class Consts
    {
        public partial class Schema1
        {
            /// <summary>The name of the element/attribute in the XSD schema.</summary>
            public static class Namespaces
            {
                /// <summary>The standard namespace preffix</summary>
                public const string XMLNS = "xmlns";
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string XPath10 = "http://www.w3.org/TR/1999/Rec-xpath-19991116";
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string Policy = "urn:oasis:names:tc:xacml:1.0:policy";
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string Context = "urn:oasis:names:tc:xacml:1.0:context";
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string XmlSignature = "http://www.w3.org/2000/09/xmldsig#";
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string Xsi = "http://www.w3.org/2001/XMLSchema-instance";
            }
        }
    }
}
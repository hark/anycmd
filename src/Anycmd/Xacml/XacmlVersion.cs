
namespace Anycmd.Xacml
{
	/// <summary>
	/// Enumeration describing the schema used to validate a document.
	/// </summary>
	public enum XacmlVersion
	{
		/// <summary>
		/// Version 1.0 document.
		/// </summary>
		Version10, 

		/// <summary>
		/// Version 1.1 document.
		/// </summary>
		Version11, 

		/// <summary>
		/// Version 2.0 document.
		/// </summary>
		Version20
	}
}

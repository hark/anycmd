﻿
namespace Anycmd.Xacml
{
    public partial class Consts
    {
        public partial class ContextSchema
        {
            /// <summary>The name of the element/attribute in the XSD schema.</summary>
            public static class ResourceElement
            {
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string ResourceContent = "ResourceContent";
                /// <summary>The name of the element/attribute in the XSD schema.</summary>
                public const string ResourceId = "urn:oasis:names:tc:xacml:1.0:resource:resource-id";
            }
        }
    }
}
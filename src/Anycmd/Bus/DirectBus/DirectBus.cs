﻿
namespace Anycmd.Bus.DirectBus
{
    using Model;
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// 表示当总线提交（Commit）时消息将被立即分发的消息总线。
    /// </summary>
    public abstract class DirectBus : DisposableObject, IBus
    {
        #region Private Fields
        private volatile bool committed = true;
        private readonly IMessageDispatcher dispatcher;
        private readonly Queue<dynamic> messageQueue = new Queue<dynamic>();
        private readonly object queueLock = new object();
        private dynamic[] backupMessageArray;
        #endregion

        #region Ctor
        /// <summary>
        /// 初始化一个 <c>DirectBus</c> 类型的对象。
        /// </summary>
        /// <param name="dispatcher">总线中所使用的消息分发器对象 <see cref="Anycmd.Bus.IMessageDispatcher"/>。</param>
        public DirectBus(IMessageDispatcher dispatcher)
        {
            if (dispatcher == null)
            {
                throw new ArgumentNullException("dispatcher");
            }
            this.dispatcher = dispatcher;
        }
        #endregion

        #region Protected Methods
        /// <summary>
        /// 释放当前对象。
        /// </summary>
        /// <param name="disposing">表示当前对象是否应该显式的释放。true表示应显式释放。</param>
        protected override void Dispose(bool disposing)
        {
        }
        #endregion

        #region IBus Members
        /// <summary>
        /// 将给定的消息发布到消息总线。
        /// </summary>
        /// <typeparam name="TMessage">将被发布的消息的类型。</typeparam>
        /// <param name="message">将被发布的消息。</param>
        public void Publish<TMessage>(TMessage message) where TMessage : IMessage
        {
            lock (queueLock)
            {
                messageQueue.Enqueue(message);
                committed = false;
            }
        }

        /// <summary>
        /// 将给定的一系列消息发布到消息总线。
        /// </summary>
        /// <typeparam name="TMessage">将被发布的消息的类型。</typeparam>
        /// <param name="messages">将被发布的一个消息系列。</param>
        public void Publish<TMessage>(IEnumerable<TMessage> messages) where TMessage : IMessage
        {
            lock (queueLock)
            {
                foreach (var message in messages)
                {
                    messageQueue.Enqueue(message);
                }
                committed = false;
            }
        }

        /// <summary>
        /// 清空已发布的尚未提交（Commit）的命令。
        /// </summary>
        public void Clear()
        {
            lock (queueLock)
            {
                this.messageQueue.Clear();
            }
        }
        #endregion

        #region IUnitOfWork Members
        /// <summary>
        /// 获得一个<see cref="System.Boolean"/>值，该值表示当前的Unit Of Work是否支持Microsoft分布式事务处理机制。
        /// </summary>
        public bool DistributedTransactionSupported
        {
            get { return false; }
        }

        /// <summary>
        /// 获得一个<see cref="System.Boolean"/>值，该值表述了当前的Unit Of Work事务是否已被提交。
        /// </summary>
        public bool Committed
        {
            get { return this.committed; }
        }

        /// <summary>
        /// 提交当前的Unit Of Work事务。
        /// </summary>
        public void Commit()
        {
            lock (queueLock)
            {
                backupMessageArray = new dynamic[messageQueue.Count];
                messageQueue.CopyTo(backupMessageArray, 0);
                while (messageQueue.Count > 0)
                {
                    dispatcher.DispatchMessage(messageQueue.Dequeue());
                }
                committed = true;
            }
        }

        /// <summary>
        /// 回滚当前的Unit Of Work事务。
        /// </summary>
        public void Rollback()
        {
            lock (queueLock)
            {
                if (backupMessageArray != null && backupMessageArray.Length > 0)
                {
                    messageQueue.Clear();
                    foreach (var msg in backupMessageArray)
                    {
                        messageQueue.Enqueue(msg);
                    }
                }
                committed = false;
            }
        }
        #endregion
    }
}

﻿
namespace Anycmd
{
    using System;
    using Util;

    public class Int32IDAttribute : Attribute, IDAttribute
    {
        public Int32 Value { get; private set; }

        public Int32IDAttribute(Int32 value)
        {
            this.Value = value;
        }
    }
}

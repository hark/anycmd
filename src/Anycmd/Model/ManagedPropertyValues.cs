﻿
namespace Anycmd.Model
{
    using Exceptions;
    using Host.EDI;
    using Host.Info;
    using System.Collections.Generic;
    using System.Reflection;

    public abstract class ManagedPropertyValues : IManagedPropertyValues
    {
        public IEnumerable<InfoItem> GetValues(OntologyDescriptor ontology)
        {
            var properties = this.GetType().GetProperties(BindingFlags.Public & BindingFlags.SetProperty);
            foreach (var property in properties)
            {
                yield return GetProperty(ontology, property.Name, property.GetValue(this).ToString());
            }
        }

        private static InfoItem GetProperty(OntologyDescriptor ontology, string propertyCode, string value)
        {
            if (propertyCode.Equals(ontology.Ontology.Code + "ID", System.StringComparison.OrdinalIgnoreCase))
            {
                propertyCode = "Id";
            }
            ElementDescriptor element;
            if (!ontology.Elements.TryGetValue(propertyCode, out element))
            {
                throw new CoreException("意外的" + ontology.Ontology.Name + "实体属性编码" + propertyCode);
            }
            return InfoItem.Create(element, value);
        }
    }
}

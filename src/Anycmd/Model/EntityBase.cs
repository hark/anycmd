﻿
namespace Anycmd.Model
{
    using Exceptions;
    using Extensions;
    using System;

    /// <summary>
    /// 实体模型基类型
    /// <remarks>
    /// 在EntityObject的基础上增加了CreateOn、CreateUserID、
    /// CreateBy、ModifiedOn、ModifiedUserID、ModifiedBy六个属性。
    /// 注意：
    /// 1， CreateOn和InsertedOn是不同的。CreateOn不是存储层的概念而是业务层的概念，
    /// 存储层可以有InsertedOn这样的概念，但是存储层的InsertedOn概念是无需让应用层知道的。
    /// 2， CreateUserID和InsertedUserID是不同的。CreateUserID不是存储层的概念而是业务层的概念，
    /// 存储层可以有InsertedUserID这样的概念，但是存储层的InsertedUserID概念是无需让应用层知道的。
    /// 存储层的InsertedUserID对应的是“sa”这样的数据库用户名。
    /// 3， ModifiedOn和UpdatedOn是不同的。ModifiedOn不是存储层的概念而是业务层的概念，
    /// 存储层可以有UpdatedOn这样的概念，但是存储层的UpdatedOn概念是无需让应用层知道的。
    /// 4， ModifiedUserID和UpdatedUserID是不同的。ModifiedUserID不是存储层的概念而是业务层的概念，
    /// 存储层可以有UpdatedUserID这样的概念，但是存储层的UpdatedUserID概念是无需让应用层知道的。
    /// 存储层的UpdatedUserID对应的是“sa”这样的数据库用户名。
    /// </remarks>
    /// </summary>
    public abstract class EntityBase : EntityObject, IEntityBase
    {
        private DateTime? createOn;
        private DateTime? modifiedOn;
        private Guid? createUserID;
        private Guid? modifiedUserID;
        private string createBy;
        private string modifiedBy;

        /// <summary>
        /// 实体创生时间。
        /// <remarks>
        /// 注意：CreateOn和InsertedOn是不同的。CreateOn不是存储层的概念而是业务层的概念，
        /// 存储层可以有InsertedOn这样的概念，但是存储层的InsertedOn概念是无需让应用层知道的。
        /// </remarks>
        /// </summary>
        public DateTime? CreateOn
        {
            get { return createOn; }
            protected set
            {
                if (createOn.HasValue && createOn != value)
                {
                    throw new ValidationException("创建时间不能更改");
                }
                createOn = value;
            }
        }

        /// <summary>
        /// 创建人表示。
        /// <remarks>
        /// 注意：CreateUserID和InsertedUserID是不同的。CreateUserID不是存储层的概念而是业务层的概念，
        /// 存储层可以有InsertedUserID这样的概念，但是存储层的InsertedUserID概念是无需让应用层知道的。
        /// 存储层的InsertedUserID对应的是“sa”这样的数据库用户名。
        /// </remarks>
        /// </summary>
        public Guid? CreateUserID
        {
            get { return createUserID; }
            protected set
            {
                if (createUserID.HasValue && createUserID != value)
                {
                    throw new ValidationException("创建人不能更改");
                }
                createUserID = value;
            }
        }

        /// <summary>
        /// 创建人名称。
        /// </summary>
        public string CreateBy
        {
            get { return createBy; }
            protected set { createBy = value; }
        }

        /// <summary>
        /// 实体最后修改时间。
        /// <remarks>
        /// 注意：ModifiedOn和UpdatedOn是不同的。ModifiedOn不是存储层的概念而是业务层的概念，
        /// 存储层可以有UpdatedOn这样的概念，但是存储层的UpdatedOn概念是无需让应用层知道的。
        /// </remarks>
        /// </summary>
        public DateTime? ModifiedOn
        {
            get { return modifiedOn; }
            protected set
            {
                if (value != null)
                {
                    if (value.Value.IsNotValid())
                    {
                        throw new ValidationException("ModifiedOn值不合法" + value);
                    }
                }
                modifiedOn = value;
            }
        }

        /// <summary>
        /// 实体最后修改人标识。
        /// <remarks>
        /// 注意：ModifiedUserID和UpdatedUserID是不同的。ModifiedUserID不是存储层的概念而是业务层的概念，
        /// 存储层可以有UpdatedUserID这样的概念，但是存储层的UpdatedUserID概念是无需让应用层知道的。
        /// 存储层的UpdatedUserID对应的是“sa”这样的数据库用户名。
        /// </remarks>
        /// </summary>
        public Guid? ModifiedUserID
        {
            get { return modifiedUserID; }
            protected set
            {
                if (value != null)
                {
                    if (modifiedUserID != null && value.Value == Guid.Empty)
                    {
                        throw new ValidationException("最后修改人标识错误" + value);
                    }
                }
                modifiedUserID = value;
            }
        }

        /// <summary>
        /// 实体最后修改人名称。
        /// </summary>
        public string ModifiedBy
        {
            get { return modifiedBy; }
            protected set { modifiedBy = value; }
        }

        public Guid rowguid { get; protected set; }

        #region 显示实现
        /// <summary>
        /// 创建时间
        /// </summary>
        DateTime? IEntityBase.CreateOn
        {
            get { return createOn; }
            set
            {
                this.CreateOn = value;
            }
        }

        /// <summary>
        /// 创建人标识
        /// </summary>
        Guid? IEntityBase.CreateUserID
        {
            get { return createUserID; }
            set
            {
                this.CreateUserID = value;
            }
        }

        /// <summary>
        /// 创建人[姓名|登录名]
        /// </summary>
        string IEntityBase.CreateBy
        {
            get { return createBy; }
            set { this.CreateBy = value; }
        }

        /// <summary>
        /// 最后修改时间
        /// </summary>
        DateTime? IEntityBase.ModifiedOn
        {
            get { return modifiedOn; }
            set
            {
                this.ModifiedOn = value;
            }
        }

        /// <summary>
        /// 最后修改人标识
        /// </summary>
        Guid? IEntityBase.ModifiedUserID
        {
            get { return modifiedUserID; }
            set
            {
                this.ModifiedUserID = value;
            }
        }

        /// <summary>
        /// 最后修改人[姓名|登录名]
        /// </summary>
        string IEntityBase.ModifiedBy
        {
            get { return modifiedBy; }
            set { this.ModifiedBy = value; }
        }
        #endregion
    }
}

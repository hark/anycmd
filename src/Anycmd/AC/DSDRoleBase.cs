﻿
namespace Anycmd.AC
{
    using Exceptions;
    using Model;
    using System;

    /// <summary>
    /// 动态职责分离角色基类。
    /// </summary>
    public abstract class DSDRoleBase : EntityBase, IDSDRole
    {
        private Guid dsdSetID;
        private Guid roleID;

        public Guid DSDSetID
        {
            get
            {
                return dsdSetID;
            }
            set
            {
                if (value == Guid.Empty)
                {
                    throw new ValidationException("必须关联集合");
                }
                if (value != dsdSetID)
                {
                    dsdSetID = value;
                }
            }
        }

        public Guid RoleID
        {
            get { return roleID; }
            set
            {
                if (value == Guid.Empty)
                {
                    throw new ValidationException("必须指定角色");
                }
                if (value != roleID)
                {
                    roleID = value;
                }
            }
        }
    }
}

﻿
namespace Anycmd.AC.Identity
{
    using Model;

    /// <summary>
    /// 表示该接口的实现类是开发者
    /// </summary>
    public interface IDeveloperID : IEntity
    {
    }
}

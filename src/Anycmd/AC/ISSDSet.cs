﻿
namespace Anycmd.AC
{
    using System;

    /// <summary>
    /// 表示该接口的实现类是静态责任分离角色集
    /// </summary>
    public interface ISSDSet
    {
        Guid Id { get; }
        string Name { get; }
        int SSDCard { get; }
        /// <summary>
        /// 是否启用
        /// </summary>
        int IsEnabled { get; }

        string Description { get; }

        DateTime? CreateOn { get; }
    }
}

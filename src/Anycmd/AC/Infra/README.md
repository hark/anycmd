﻿Anycmd.AC.Infra命名空间下定义的是AC领域基础设施实体的静态数据结构。
Anycmd的访问控制安全框架，它的所有设施都是为访问控制服务的，虽然某些设施可以供其它业务使用但不推荐这样用，
应用系统使用Anycmd的时候目的是使用它来完成访问控制而不是完成其它业务逻辑。应用系统中可以有自己的与Anycmd的类似的设施，应用系统应该使用自己的设施。
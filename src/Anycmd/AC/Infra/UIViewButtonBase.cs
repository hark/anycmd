﻿
namespace Anycmd.AC.Infra
{
    using Exceptions;
    using Model;
    using System;

    /// <summary>
    /// 界面视图按钮基类。<see cref="IUIViewButton"/>
    /// </summary>
    public abstract class UIViewButtonBase : EntityBase, IUIViewButton
    {
        private Guid _uiViewID;
        private Guid _buttonID;

        /// <summary>
        /// 是否启用
        /// </summary>
        public virtual int IsEnabled { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual Guid? FunctionID { get; set; }

        /// <summary>
        /// 说明
        /// </summary>
        public virtual string Description { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual Guid UIViewID
        {
            get { return _uiViewID; }
            set
            {
                if (value != _uiViewID)
                {
                    if (_uiViewID != Guid.Empty)
                    {
                        throw new CoreException("不能更改所属界面视图");
                    }
                    _uiViewID = value;
                }
            }
        }
        /// <summary>
        /// 
        /// </summary>
        public virtual Guid ButtonID
        {
            get { return _buttonID; }
            set
            {
                if (value != _buttonID)
                {
                    if (_buttonID != Guid.Empty)
                    {
                        throw new CoreException("不能更改关联按钮");
                    }
                    _buttonID = value;
                }
            }
        }
    }
}

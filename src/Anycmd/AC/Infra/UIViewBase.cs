﻿
namespace Anycmd.AC.Infra
{
    using Model;

    /// <summary>
    /// 界面视图基类<see cref="IUIView"/>
    /// </summary>
    public abstract class UIViewBase : EntityBase, IUIView
    {

        /// <summary>
        /// 
        /// </summary>
        public virtual string Icon { get; set; }
        /// <summary>
        /// 
        /// </summary>
        public virtual string Tooltip { get; set; }
    }
}

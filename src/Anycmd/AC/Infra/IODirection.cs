﻿
namespace Anycmd.AC.Infra
{
    /// <summary>
    /// 表示IO方向的值。
    /// </summary>
    public enum IODirection
    {
        Input,
        Output
    }
}

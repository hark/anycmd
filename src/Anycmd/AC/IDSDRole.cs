﻿
namespace Anycmd.AC
{
    using System;

    /// <summary>
    /// 表示动态职责分离角色。
    /// </summary>
    public interface IDSDRole
    {
        /// <summary>
        /// 查看动态职责分离角色标识。
        /// </summary>
        Guid Id { get; }

        /// <summary>
        /// 查看动态职责分离角色集标识。
        /// </summary>
        Guid DSDSetID { get; }

        /// <summary>
        /// 查看角色标识。
        /// </summary>
        Guid RoleID { get; }
    }
}

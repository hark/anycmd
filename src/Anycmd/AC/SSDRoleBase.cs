﻿
namespace Anycmd.AC
{
    using Exceptions;
    using Model;
    using System;

    /// <summary>
    /// 静态职责分离角色基类。
    /// </summary>
    public abstract class SSDRoleBase : EntityBase, ISSDRole
    {
        private Guid ssdSetID;
        private Guid roleID;

        public Guid SSDSetID
        {
            get
            {
                return ssdSetID;
            }
            set
            {
                if (value == Guid.Empty)
                {
                    throw new ValidationException("必须关联集合");
                }
                if (value != ssdSetID)
                {
                    ssdSetID = value;
                }
            }
        }

        public Guid RoleID
        {
            get { return roleID; }
            set
            {
                if (value == Guid.Empty)
                {
                    throw new ValidationException("必须指定角色");
                }
                if (value != roleID)
                {
                    roleID = value;
                }
            }
        }
    }
}
